#! /usr/bin/env python3

# Copyright Caterpillar Inc.
# 
# Author:  Carson Dew (7 May 2014)

import socket
import time
import threading
import binascii
import sys

class THDMessage():
    '''Message class to conform to THD standard-defined messages'''
    thd_request = 0x01
    thd_response = 0x02
    
    def __init__(self,mac,msgtype=thd_request):
        self.msgbyte = bytearray([msgtype])
        self.macbytes = bytearray(binascii.a2b_hex(bytes(mac,'ascii')))
        self.packet = self.msgbyte + self.macbytes

class THDHost():
    '''Holds information about a THD host'''
    def __init__(self, mac, ip_addr):
        self.mac = mac
        self.ip_addr = ip_addr
        self.last_notify_time = None
    
    def UpdateLastRespTime(self, last_time):
        '''Update the time of last response for a THD host'''
        self.last_notify_time = last_time

# Subclassing not really beneficial right now
class THDLocalhost(THDHost):
    '''Holds information about the local THD host and can participate in THD protocol.'''
    def __init__(self, mac, ip_addr, port=31580, bcast_addr='165.26.79.255', resp_per_s=.5):
        
        super(THDLocalhost, self).__init__(mac, ip_addr)
        self.port = port
        self.bcast_addr = bcast_addr
        self.thd_network = THDNetwork()
        self.last_send_resp_time = 0
        self.resp_per_s = resp_per_s

        try:
            #self.thdsocketlock = threading.Lock()
            self.thdsocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.thdsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.thdsocket.bind(('',self.port))
            self.thdsocket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            self.thdsocket.settimeout(.1)

            self.recv_thread = threading.Thread(target=self.THDRecvWorker)
            self.recv_thread.daemon = True
            self.recv_thread_alive = True
            self.recv_thread.start()
        except Exception as instance:
            ''' Clean up THDNetwork to avoid leaving threads running '''
            self.thd_network.close()
            raise instance
        
    def SendRequest(self):
        '''Send a single THD broadcast request to query the network'''
        msg=THDMessage(self.mac)
        #with self.thdsocketlock:
        self.thdsocket.sendto(msg.packet, (self.bcast_addr, self.port))
    
    def SendResponse(self):
        '''Send a single THD response to the network'''
        now = time.time()
        if (now - self.last_send_resp_time) > self.resp_per_s:
            msg=THDMessage(self.mac,msgtype=THDMessage.thd_response)
            #with self.thdsocketlock:
            self.thdsocket.sendto(msg.packet, (self.bcast_addr, self.port))
            self.last_send_resp_time = time.time()
        
    def ProcessRecv(self,recv_msg):
        '''Process received messages from the socket for THD behavior'''
        message, addr = recv_msg
        if message[0] == THDMessage.thd_response:
            sender_mac = binascii.b2a_hex(message[1:7]).upper().decode('utf-8')
            newhost = THDHost(sender_mac, addr[0])
            now = time.time()
            newhost.UpdateLastRespTime(now)
            self.thd_network.ProcessHost(newhost)
        elif message[0] == THDMessage.thd_request:
            self.SendResponse()
    
    def THDRecvWorker(self):
        '''Worker function for a socket receiver thread'''
        while self.recv_thread_alive:
            #with self.thdsocketlock:
            try:
                self.ProcessRecv(self.thdsocket.recvfrom(7))
            except:
                pass
    
    def THDD(self, request_period_s, duration_s, callback=None):
        '''Run thdd like behavior'''
        if duration_s == 0:
            while True:
                self.SendRequest()
                time.sleep(request_period_s)
                if callback:
                    ret_val = callback(self.thd_network.active_hosts)
                    if (ret_val != None):
                        return
        else:
            for per in range(int(duration_s/request_period_s)):
                self.SendRequest()
                time.sleep(request_period_s)
                if callback:
                    ret_val = callback(self.thd_network.active_hosts)
                    if (ret_val != None):
                        return
        
    def close(self):
        '''Cleanup receiver thread and close socket'''
        try:
            self.recv_thread_alive = False
            self.recv_thread.join(1)
            self.thdsocket.close()
        finally:
            self.thd_network.close()
        
class THDNetwork():
    '''Maintains a current active THD network'''
    def __init__(self, stale_time=3):
        
        self.stale_time = stale_time
        self.active_hosts = {}
        
        self.active_host_lock = threading.Lock()
        self.stale_thread = threading.Thread(target=self.StaleWorker)
        self.stale_thread.daemon = True
        self.stale_thread_alive = True
        self.stale_thread.start()
        
    def StaleWorker(self):
        '''Worker function for stale maintenance thread'''
        while self.stale_thread_alive:
            self.CleanStaleHosts()
            time.sleep(.1) # TODO: do I need this?
    
    def CleanStaleHosts(self):
        '''Parse active_hosts for staleness'''
        with self.active_host_lock:
            garbage_hosts = []
            for entry_mac, host in self.active_hosts.items():
                entry_last_response = self.active_hosts[entry_mac].last_notify_time
                if (entry_last_response == None) or (int(time.time() - entry_last_response) > self.stale_time):
                    #print('Removing :' + str(entry_mac), str(entry_last_response), file=sys.stderr)
                    garbage_hosts.append(entry_mac)
            
            for h in garbage_hosts:
                del self.active_hosts[h]
    
    def ProcessHost(self, thd_host):
        with self.active_host_lock:
            # Add/Overwrite the host in the dictionary, host should have response time
            self.active_hosts[thd_host.mac] = thd_host
            
    def NetworkPrinter(active_hosts):
        network_copy = active_hosts.copy()
        for host in network_copy.values():
            print('MAC: {0}'.format(host.mac).ljust(20) + 
                  'IP: {0}'.format(host.ip_addr).ljust(20) +
                  'Last Active: {0}s'.format(host.last_notify_time-time.time()))
        
    def close(self):
        '''Cleanup receiver thread and close socket'''
        self.stale_thread_alive = False
        self.stale_thread.join(1)
            

if __name__ == '__main__':
    
    import argparse
    parser = argparse.ArgumentParser(description='Utility to discover THD hosts')
    parser.add_argument('-t', '--search-time', dest='searchTime', required=True, type=int,
                        help='Time in seconds to discover hosts, 0 for infinite')
    parser.add_argument('--host-mac', dest='hostmac', default='AAAAAAAAAAAA',
                        help='MAC address of this host')
    parser.add_argument('--host-ip', dest='hostip', default='165.26.79.1',
                        help='IP address of this host')
    args = parser.parse_args()
    
    try:
        # run thdd like behavior with static information
        myhost = THDLocalhost(args.hostmac, args.hostip)
        import pprint
        myhost.THDD(.5,args.searchTime,callback=THDNetwork.NetworkPrinter)
        
    except (KeyboardInterrupt, SystemExit):
        myhost.close()
    finally:
        myhost.close()
