'''
Author: Joshua
Last Updated: 1/31/2019
Description: A library script containing useful functions to use everywhere
'''
import os, re, sys, time, subprocess
import configparser
'''configparser'''

cfg = configparser.ConfigParser()
absPath = os.path.dirname(os.path.realpath(__file__))
cwd = os.getcwd()
os.chdir(absPath)

cfg.read('../config/atsGrpcconfig.cfg')
svr = cfg['SERVER_INFO']
os.chdir(cwd)

SSH_NEWKEY = '(?i)are you sure you want to continue connecting'
sshport = 22
user = "root"
password = "p30r1a"
debugDump = ""
targetIP = ''

def verifyPyVer():
	if not re.search("3", sys.version):
		print("You need to use Python version 3.x.x")
		os._exit(1)

'''
Clear the targetIP in the cfg file
NOTE: kept here just in case need to differentiate board using skicheck instead of ssl_inp
'''
'''def initGRPC(targetIP):
    atsSetup()
    cfg = configparser.ConfigParser()
    cfg.read('../config/atsGrpcconfig.cfg')
    cfgFile = open('../config/atsGrpcconfig.cfg', 'w+')
    cfg.set('SERVER_INFO', 'target_ip', targetIP)
    skiCheckRes = os.system('python ../tools/skicheck.py -d ' + str(targetIP))
    if skiCheckRes == 0: cfg.set('SSL', 'option', 'yes')
    elif skiCheckRes == 1: cfg.set('SSL', 'option', 'no')
    else:
        cfgFile.close()
        return -1
    cfg.write(cfgFile)
    cfgFile.close()'''

'''
Clear the targetIP in the cfg file
NOTE: named as initGRPc just in case if there are any other init/setup functions
'''
def initGRPC(targetIP, ssl_inp):
    cfg = configparser.ConfigParser()
    cfg.read('../config/atsGrpcconfig.cfg')
    cfgFile = open('../config/atsGrpcconfig.cfg', 'w+')
    if ssl_inp.upper() == 'SSL':
        cfg.set('SSL', 'option', 'yes')
        cfg.set('RMA', 'rma_grpc', '')
    elif ssl_inp.upper() == 'RMA':
        cfg.set('SSL', 'option', 'yes')
        cfg.set('RMA', 'rma_grpc', 'yes')
    else:
        cfg.set('SSL', 'option', 'no')
        cfg.set('RMA', 'rma_grpc', '')
    cfg.write(cfgFile)
    cfgFile.close()
    atsSetup()


'''
Gets the IP address of the 1st instance of the board connected
to the PC
'''
def getTargetIP():
    print("Searching for IP target...")
    import importlib
    p = os.path.abspath('../lib/')
    if p not in sys.path:
        sys.path.append(p)
    import thd35
    importlib.reload(thd35)


    if (thd35.targetIP == "" or thd35.targetIP == svr.get('HOST_IP')):
        print("No target IP found.")
        return ''
    else:
        print("Target IP found: " + thd35.targetIP)
        return thd35.targetIP


'''
Date: 12/19/2018
Desciption: extracts mac address using the latest thd.py located in tools/ folder
'''
def extractMacAddr():
    print("Extracting mac address from thd.py")
    thdCommand = 'python thd.py -t 1'
    result = subprocess.Popen(thdCommand, shell=True, stdout=subprocess.PIPE)
    mac = ''
    raw = str(result.stdout.read())
    formatted = raw.split('\\r\\n')
    try:
        count = 0
        while count < 4:
            if count == 0 and formatted[0][7:19] != 'AAAAAAAAAAAA': break
            if count == 1 and formatted[1][5:17] != 'AAAAAAAAAAAA': break
            if count == 2 and formatted[2][5:17] != 'AAAAAAAAAAAA': break
            if count == 3 and formatted[3][5:17] != 'AAAAAAAAAAAA': break
            count += 1
        if count == 0: mac = formatted[count][7:19]
        else: mac = formatted[count][5:17]
        if mac == 'AAAAAAAAAAAA' or mac == '':
            print(mac)
            print("No mac address found")
            return -1
        else: print("MAC addr found: " + mac)
    except: pass

    return mac

def extractFromXML(xmlFile):
    ECM_pn = ''
    ECM_sn = ''
    os_pn = ''
    app_pn = ''
    app_cn = ''
    # Open XML document using minidom parser
    DOMTree = xml.dom.minidom.parse(xmlFile)
    collection = DOMTree.documentElement

    flashTargetInformationMessage = collection.getElementsByTagName("FlashTargetInformationMessage")

    for infoMsg in flashTargetInformationMessage:
        ECMPartNumber = infoMsg.getElementsByTagName('ECMPartNumber')[0]
        ECM_pn = ECMPartNumber.childNodes[0].data
        print("ECMPartNumber: %s" % ECMPartNumber.childNodes[0].data)

        ECMSerialNumber = infoMsg.getElementsByTagName('ECMSerialNumber')[0]
        ECM_sn = ECMSerialNumber.childNodes[0].data
        print("ECMSerialNumber: %s" % ECMSerialNumber.childNodes[0].data)

        operatingSystem = collection.getElementsByTagName("OperatingSystem")
        for os_info in operatingSystem:
            partNumber = os_info.getElementsByTagName('PartNumber')[0]
            os_pn = partNumber.childNodes[0].data
            print("OS Part Number: %s" % partNumber.childNodes[0].data)

        application = collection.getElementsByTagName("Application")
        for app_info in application:
            partNumber = app_info.getElementsByTagName('PartNumber')[0]
            app_pn = partNumber.childNodes[0].data
            print("App Part Number: %s" % partNumber.childNodes[0].data)
            changeNumber = app_info.getElementsByTagName('ChangeNumber')[0]
            app_cn = changeNumber.childNodes[0].data
            print("App Change Number: %s" % changeNumber.childNodes[0].data)


    #get time and date
    dateAndTime = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
    SN_Date_n_Time = ""
    SN_Date_n_Time += ECM_sn + "_" + dateAndTime
    # write to file
    f = open(SN_Date_n_Time + '.txt', 'w')
    f.write("ECMSerialNumber: " + ECM_pn + "\n")
    f.write("OS Part Number: " + os_pn + "\n")
    f.write("App Part Number: " + app_pn + "\n")
    f.write("App Change Number: " + app_cn + "\n")
    f.close()

'''
verify python version
check from atsGrpcconfig.cfg for:
    - ssl Option
    - print debug dump option
remove path if already existed in for nonSsl and ssl
'''
def atsSetup():
	global debugDump, targetIP
	verifyPyVer()
	import configparser
	cfg = configparser.ConfigParser()
	cfg.read('../config/atsGrpcconfig.cfg')
	sslOption = cfg['SSL']
	sslPath = os.path.abspath('../ssl/')
	nonSslPath = os.path.abspath('../nonSsl/')


	if sslOption.get('option') == 'no': # for unskied boards
		if sslPath in sys.path:
			sys.path.remove(sslPath)
		if nonSslPath not in sys.path: # to avoid adding duplicate paths
			sys.path.append(nonSslPath)
	elif sslOption.get('option') == 'yes': # for skied boards
		if nonSslPath in sys.path:
			sys.path.remove(nonSslPath)
		if sslPath not in sys.path: # to avoid adding duplicate paths
			sys.path.append(sslPath)
	debugDump = cfg['DEBUG_DUMP'].get('option')


def logScriptError(error, currCount):
	try:
		f = open("scriptErrLog_" + str(currCount), "a")
		f.write("\n"  + str(time.asctime(time.localtime(time.time()))) + " Script error: " + str(error) + "at iteration: " + str(currCount))
		f.close()
	except Exception as e:
		print(e)
		print("Error logging error LOL")

def sshLogin(HOST,user,password):
    print("Logging in by ssh to {0}".format(HOST))
    try:
        SSH = pexpect.spawn('ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p %s -l %s %s'%(sshport, user, HOST))
    except:
        rprint("SSH session was refused")
        os._exit(1)
    i = SSH.expect([pexpect.TIMEOUT, SSH_NEWKEY, "#", 'password'])
    if i == 0:
        print ('ERROR! could not login with SSH. Here is what SSH said:')
        print (SSH.before, SSH.after)
        print (str(SSH))
        sys.exit (1)
    elif i == 1:
        SSH.sendline ('yes')
        j = SSH.expect ([pexpect.TIMEOUT, "#", 'password'])
        if j == 0:
            print ('ERROR! could not login with SSH. Here is what SSH said:')
            print (SSH.before, SSH.after)
            print (str(SSH))
            sys.exit (1)
        elif j == 1:
            pass
        elif j == 2:
            SSH.sendline(password)
            SSH.expect (["#"])
    elif i == 2:
        pass
    elif i == 3:
        SSH.sendline(password)
        SSH.expect (["#"])
    return SSH


# retries 'command' for 'retryCount' times
def retryCommand(command, retryCount, currCount):
	try:
		satUpCount = 0
		cellUpCount = 0
		csdUpCount = 0
		getCellDynamicCount = 0
		getSatDynamicCount = 0
################################################################################################################
		if command == "GET_CELL":
			while rpc.getModemInfo_secure(CELL_DYNAMIC, currCount) == -1 and getCellDynamicCount < retryCount:
				getCellDynamicCount += 1
				print("Error on getCellDynamic. Retrying getCellDynamicInfo attempt: (" + str(getCellDynamicCount) + "/" + str(retryCount) + ")")
				time.sleep(3)
			if getCellDynamicCount > retryCount:
				print("getCellDynamicInfo retry count exceeded")
				return -1
################################################################################################################
		if command == "GET_SAT":
			while rpc.getModemInfo_secure(SAT_DYNAMIC, currCount) == -1 and getSatDynamicCount < retryCount:
				getSatDynamicCount += 1
				print("Error on getSatDynamic. Retrying getSatDynamicInfo attempt: (" + str(getSatDynamicCount) + "/" + str(retryCount) + ")")
				time.sleep(3)
			if getSatDynamicCount > retryCount:
				print("getSatDynamicInfo retry count exceeded")
				return -1
################################################################################################################
		if command == "SAT":
			while rpc.sendCommand_secure(SAT_POWER_DOWN, currCount) == -1 and satUpCount < retryCount:
				satUpCount += 1
				print("retrying SAT_POWER_UP attempt: " + str(satUpCount))
				time.sleep(3)
			if getSatDynamicCount > retryCount:
				print("SAT_POWER_UP retry count exceeded")
				return -1
################################################################################################################
		if command == "CELL":
			while rpc.sendCommand_secure(CELL_POWER_UP, currCount) == -1 and cellUpCount < retryCount:
				cellUpCount += 1
				print("retrying CELL_POWER_UP attempt: " + str(cellUpCount))
				time.sleep(10)
			if cellUpCount > retryCount:
				print("CELL_POWER_UP retry count exceeded")
				return -1
################################################################################################################
		if command == "CSD":
			while rpc.sendCommand_secure(SAT_CSD_UP, currCount) == -1 and csdUpCount < retryCount:
				csdUpCount += 1
				print("retrying CELL_POWER_UP attempt: " + str(csdUpCount))
				time.sleep(10)
			if csdUpCount > retryCount:
				print("SAT_CSD_UP retry count exceeded")
				return -1
		return 0 # success if no return 0
################################################################################################################
	except Exception as e:
		print(str(e))
		print("Iteration: " + str(currCount) + " error")
		logScriptError("retry command error: "+ str(e), currCount)
		pass

# verifies cell operating mode for 'count' times
def verifyCellOpMode(mode, count, currCount):
	try:
		attempt = 0
		if retryCommand("GET_CELL", count, currCount) == -1: return -1
		operatingMode = 0
		while operatingMode != mode and attempt < count:
			print("CELL_OPERATING_MODE: " + cellOpModeLst[operatingMode] + ". GET_CELL_DYNAMIC attempt: (" + str(attempt) + "/" + str(count) + ")")
			time.sleep(15)
			retryCommand("GET_CELL", count, currCount)
			cellDynamicRes = rpc.getModemInfo_secure(CELL_DYNAMIC, currCount)
			if cellDynamicRes == -1: #if after [count] retries still issue, then something is wrong, but avoid prematrure at all costs
				return -1
			else:
				operatingMode = cellDynamicRes.CellDynamicValue.OperatingMode # that way you don't have to run frickin getCellDynamicInfo again in the next millisecond
				attempt += 1
		if operatingMode != mode and attempt >= count:
			print("Verify cell op mode: " + cellOpModeLst[operatingMode] + " limit exceeded")
			return -1
		else:
			print("CELL_OPERATING_MODE: " + cellOpModeLst[operatingMode] + " OK. CONTINUING")
			return 0
	except Exception as e:
		print(str(e))
		print("Iteration: " + str(currCount) + " error")
		logScriptError("verify cellOpMode error: "+ str(e), currCount)
		pass

# verifies sat operating mode for 'count' times
def verifySatOpMode(mode, count, currCount):
	try:
		attempt = 0
		if retryCommand("GET_SAT", count, currCount) == -1: return -1
		operatingMode = 0
		while operatingMode != mode and attempt < count:
			print("SAT_OPERATING_MODE: " + satOpModeLst[operatingMode] + ". GET_SAT_DYNAMIC attempt: (" + str(attempt) + "/" + str(count) + ")")
			time.sleep(10)
			retryCommand("GET_SAT", count, currCount) # op mode has grpc failure between initializing and sbd
			satDynamicRes = rpc.getModemInfo_secure(SAT_DYNAMIC, currCount)
			if satDynamicRes == -1: #if after [count] retries still issue, then something is wrong, but avoid prematrure at all costs
				return -1
			else:
				operatingMode = satDynamicRes.SatDynamicValue.OperatingMode # that way you don't have to run frickin getSatDynamicInfo again in the next millisecond
				attempt += 1
		if operatingMode != mode and attempt >= count:
			print("Verify sat op mode: " + satOpModeLst[operatingMode] + " limit exceeded")
			return -1
		else:
			print("SAT_OPERATING_MODE: " + satOpModeLst[operatingMode] + " OK. CONTINUING")
			return 0
	except Exception as e:
		print(str(e))
		print("Iteration: " + str(currCount) + " error")
		logScriptError("verify satOpMode error: "+ str(e), currCount)
		pass


# THIS IS FOR BOARDS WITH zzDevTools INSTALLED
def sshRsaLogin(HOST, user, password, rsaPath):
    try:
        print("Logging in by ssh to {0}".format(HOST))
        sshCommand = 'ssh -i ' + rsaPath + ' -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p %s -l %s %s'%(sshport, user, HOST)
        print(sshCommand)
        SSH = pexpect.spawn(sshCommand)
        SSH.expect(["./t6rx-id_rsa"])
        time.sleep(1)
        SSH.sendline(password)
        print("Logged in to: " + HOST + ", username: " + user)
        return SSH
    except Exception as e:
        print(e)
        print("SSH log in failed, exiting")
        os._exit(1)

'''
Date: 12/19/2018
Desciption: extracts mac address using the latest thd.py located in tools/ folder
'''
def extractMacAddr():
    print("Extracting mac address from thd.py")
    thdCommand = 'python thd.py -t 1'
    result = subprocess.Popen(thdCommand, shell=True, stdout=subprocess.PIPE)
    mac = ''
    raw = str(result.stdout.read())
    formatted = raw.split('\\r\\n')
    try:
        count = 0
        while count < 4:
            if count == 0 and formatted[0][7:19] != 'AAAAAAAAAAAA': break
            if count == 1 and formatted[1][5:17] != 'AAAAAAAAAAAA': break
            if count == 2 and formatted[2][5:17] != 'AAAAAAAAAAAA': break
            if count == 3 and formatted[3][5:17] != 'AAAAAAAAAAAA': break
            count += 1
        if count == 0: mac = formatted[count][7:19]
        else: mac = formatted[count][5:17]
        if mac == 'AAAAAAAAAAAA' or mac == '':
            print(mac)
            print("No mac address found")
            return -1
        else: print("MAC addr found: " + mac)
    except: pass

    return mac