# Copyright Caterpillar Inc.

import socket
import time
import threading
import binascii

targetIP = '165.26.79.1'
myhost = ""

class THDMessage():
    '''Message class to conform to THD standard-defined messages'''
    thd_request = 0x01
    thd_response = 0x02

    def __init__(self,mac,msgtype=thd_request):
        self.msgbyte = bytearray([msgtype])
        self.macbytes = bytearray(binascii.a2b_hex(bytes(mac,'ascii')))
        #self.macbytes = bytearray(binascii.a2b_hex(bytes(mac)))
        self.packet = self.msgbyte + self.macbytes

class THDHost(object):
    '''Holds information about a THD host'''
    def __init__(self, mac, ip_addr):
        self.mac = mac
        self.ip_addr = ip_addr
        self.last_resp_time = None

    def UpdateLastRespTime(last_time=time.time()):
        '''Update the time of last response for a THD host'''
        self.last_resp_time = last_time

class THDLocalhost(THDHost):
    '''Holds information about the local THD host and can participate in THD protocol.'''
    def __init__(self, mac, ip_addr, port=31580, bcast_addr='165.26.79.255'):

        super(THDLocalhost, self).__init__(mac, ip_addr)
        self.port = port
        self.bcast_addr = bcast_addr

        self.thdsocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.thdsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.thdsocket.bind(('',self.port))
        self.thdsocket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        #print "THD localhost has bound to port {0}".format(port)

        #self.thdrecvlock = threading.Lock()
        self.recv_thread = threading.Thread(target=self.THDRecvWorker)
        self.recv_thread.daemon = True
        self.recv_thread.start()

    def SendRequest(self):
        '''Send a single THD broadcast request to query the network'''
        msg=THDMessage(self.mac)
        self.thdsocket.sendto(msg.packet, (self.bcast_addr, self.port))
        self.thdsocket.settimeout(.1)

    def SendResponse(self):
        '''Send a single THD response to the network'''
        msg=THDMessage(self.mac,msgtype=THDMessage.thd_response)
        self.thdsocket.sendto(msg.packet, (self.bcast_addr, self.port))
        self.thdsocket.settimeout(.1)

    def ProcessRecv(self,recv_msg):
        global targetIP
        '''Process received messages from the socket for THD behavior'''
        message, addr = recv_msg
        if message[0] == THDMessage.thd_response:
            sender_mac = binascii.b2a_hex(message[1:7]).upper().decode('utf-8')
            #print("MAC: " + sender_mac + " IP: " + addr[0])  # ' Need to implement more'
        elif message[0] == THDMessage.thd_request:
            self.SendResponse()

        if addr[0] != '165.26.79.1':
            targetIP = addr[0]



    def THDRecvWorker(self):
        '''Worker function for a socket receiver thread'''
        while True:
            try:
                self.ProcessRecv(self.thdsocket.recvfrom(7))
                if targetIP != '165.26.79.1':
                    break
            except:
                pass

    def THDD(self, request_period_s, duration_s):
        global myhost
        '''Run thdd like behavior'''
        if duration_s == 0:
            while True:
                myhost.SendRequest()
                time.sleep(request_period_s)
        else:
            for per in range(int(duration_s/request_period_s)):
                myhost.SendRequest()
                time.sleep(request_period_s)

        #myhost.SendRequest()

    def close(self):
        '''Cleanup receiver thread and close socket'''
        self.recv_thread.alive = False
        self.recv_thread.join(.01)
        self.thdsocket.close()

class THDNetwork():
    '''Maintains a current active THD network'''
    def __init__(self, port=31580, bcast_addr='165.26.79.255', stale_time=3):

        self.stale_time = stale_time
        self.active_hosts = {}
        self.AddHost(new_host=self.localhost)

    def AddNewHost(self, new_thd_host):
        pass

    def UpdateHost(self, mac):
        pass

    def RemoveHost(self, ip_addr):
        pass


def thdd_print_format(hosts_dict):
    '''Print function that mimic linux thdd C code output'''

    if not hasattr(thdd_print_format, "time"):
        thdd_print_format.time = time.time()
    if not hasattr(thdd_print_format, "cycle"):
        thdd_print_format.cycle = 0

    now = time.time()
    if (now - thdd_print_format.time) > .5:
        thdd_print_format.time = now
        thdd_print_format.cycle += 1
        count = 0
        print("Cycle " + str(thdd_print_format.cycle))
        for host in hosts_dict.keys():
            host_str = host[0:2]+':'+host[2:4]+':'+host[4:6]+':'+host[6:8]+':'+host[8:10]+':'+host[10:12]
            print(" " + str(count) + ": " + hosts_dict[host][0] + "[" + host_str + "]")
            count = count + 1
    else:
        return

'''
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Utility to discover THD hosts')
    parser.add_argument('-t', '--search-time', dest='searchTime', required=True, type=int,
                        help='Time in seconds to discover hosts, 0 for infinite')
    parser.add_argument('--host-mac', dest='hostmac', default='AAAAAAAAAAAA',
                        help='MAC address of this host')
    parser.add_argument('--host-ip', dest='hostip', default='165.26.79.1',
                        help='IP address of this host')
    args = parser.parse_args()
'''

HOSTMAC = 'AAAAAAAAAAAA'
HOSTIP = '165.26.79.0/23'
SEARCHTIME = .5

try:
    # run thdd like behavior with static information
    myhost = THDLocalhost(HOSTMAC, HOSTIP)
    myhost.THDD(.5, SEARCHTIME)

except (KeyboardInterrupt, SystemExit):
    myhost.close()
finally:
    myhost.close()
