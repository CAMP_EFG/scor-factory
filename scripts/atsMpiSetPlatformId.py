import os, sys, argparse

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################

from atsCalampgrpc import *
COMMAND = RadioManager_pb2.ATS_MPI_SET_PLATFORM_ID
from atsModemCommand import grpcAtsSendCommand

'''
Description: Set the platform ID
Command Name: <ATS_MPI_SET_PLATFORM_ID>
Example:
	python atsMpiSetPlatformId.py -v 0001
Response:
	Success: “OK”
	Fail: Grpc send command error
'''
def grpcAtsMpiSetPlatformId(value1 = None, value2 = None, value3 = None, value4 = None):
	grpcAtsSendCommand(COMMAND, value1, value2, value3, value4)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Description:\nAn interface to send grpc ATS commands individually.\nAuthor:\t\tJoshua Liew\nDate Created:\t7/24/2018', formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
	parser.add_argument('-v1', dest='value1', default = '', type=str, help='v1 - AtsCommandValue1')
	parser.add_argument('-v2', dest='value2', default = '', type=str, help='v2 - AtsCommandValue2')
	parser.add_argument('-v3', dest='value3', default = '', type=str, help='v3 - AtsCommandValue3')
	args = parser.parse_args()
	grpcAtsMpiSetPlatformId(args.value, args.value1, args.value2, args.value3)