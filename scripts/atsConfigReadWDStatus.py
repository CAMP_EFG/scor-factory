
import os, time, atsBoardCount
import xml.etree.ElementTree as ET

url = "http://172.31.234.158/ServiceDashboard/ServiceDashboard.xml"
filePath = 'C:/temp'
xmlFile = 'ServiceDashboard.xml'

def readConfigWDStatus():
	absPathOfFile = os.path.dirname(os.path.realpath(__file__))
	currentWd = os.getcwd()
	os.chdir(absPathOfFile)
	import requests
	count = 5

	p = os.path.join(filePath,xmlFile)

	if os.path.exists(p):
		os.remove(p)

	while count != 0:
		try:
			response = requests.get(url,stream = True, timeout = 10)

			with open(p,'wb') as f:
				f.write(response.content)

			tree = ET.parse(p)
			root = tree.getroot()

			modelName =  root.find('Cell/Radio/Model').text
			print(modelName)

			if modelName == "PL243" or modelName == "PL443":
				modemState = root.find('Cell/Radio/ModemState').text
				print("ModemState = " + modemState)
				if modemState == "Wireless disabled":
					modemState2 = root.find('Cell/Radio/Modemstate2').text
					print("Modemstate2 = " + modemState2)
					if modemState2 == "Not Available":
						print("WD status Test Passed")
						atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_CGI', True)
						os.chdir(currentWd)
						return 0

			if modelName == "PL083":
				modemState = root.find('Cell/Radio/ModemState').text
				print("ModemState = " + modemState)
				if modemState == "Wireless disabled":
					modemState2 = root.find('Cell/Radio/Modemstate2').text
					print("Modemstate2 = " + modemState2)
					if modemState2 == "Wireless disabled":
						print("WD status Test Passed")
						atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_CGI', True)
						os.chdir(currentWd)
						return 0

			count = count - 1
			time.sleep(4)
		except Exception as e:
			count -= 1

	print("Returning -1")


	atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_CGI', False)
	os.chdir(currentWd)
	return -1

if __name__ == "__main__":
	readConfigWDStatus()