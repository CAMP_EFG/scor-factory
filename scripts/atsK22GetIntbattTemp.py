from atsModeminfo import *

'''
Description: Display internal battery temp info
Example:
	python atsK22GetIntbattTemp.py
'''
def getk22IntbattTemp():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_INTBATT_TEMP)
    print("Raw output: " + str(response))
    return response

def displayK22IntbattTemp():
    response = getk22IntbattTemp()
    if response == -1:
        print("Error getting ats k22 internal battery temp info")
        return -1
    print("AtsIntBattTemp                   : " + str(response.AtsIntBattTemp.AtsIntBattTemp))

if __name__ == "__main__":
    displayK22IntbattTemp()
