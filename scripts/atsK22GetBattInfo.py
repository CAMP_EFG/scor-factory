from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_K22_GET_BATT_INFO

'''
Description: Display k22 battery info
Example:
	python atsK22GetBattInfo.py
'''
def getk22BattInfo():
    response = grpcGetModemInfo(COMMAND)
    print("Raw output: " + str(response))
    return response

def displayAtsk22BattInfo():
    response = getk22BattInfo()
    if response == -1:
        print("Error getting ats k22 batt info")
        return -1
    print("AtsChargingState                   : " + str(response.AtsBatteryInfo.AtsChargingState))
    print("AtsIntBattHibState                 : " + str(response.AtsBatteryInfo.AtsIntBattHibState))
    print("AtsDACValue100mA                   : " + str(response.AtsBatteryInfo.AtsDACValue100mA))
    print("AtsDACValueZeromA                  : " + str(response.AtsBatteryInfo.AtsDACValueZeromA))
    print("AtsMainBattVolts                   : " + str(response.AtsBatteryInfo.AtsMainBattVolts))
    print("AtsBackUpBattVolts                 : " + str(response.AtsBatteryInfo.AtsBackUpBattVolts))
    print("Ats42BoostVolts                    : " + str(response.AtsBatteryInfo.Ats42BoostVolts))
    print("Ats42SwitchVolts                   : " + str(response.AtsBatteryInfo.Ats42SwitchVolts))
    print("Ats5Volts                          : " + str(response.AtsBatteryInfo.Ats5Volts))
    print("AtsChargerVolts                    : " + str(response.AtsBatteryInfo.AtsChargerVolts))

if __name__ == "__main__":
    displayAtsk22BattInfo()