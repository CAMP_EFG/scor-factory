from atsModeminfo import *
COMMAND = RadioManager_pb2.SAT_STATIC

'''
Description: Get sat static info
Example: python atsSatStatic.py
'''
def getAtsSatStatic():
    response = grpcGetModemInfo(COMMAND)
    print("Raw response: " + str(response))
    return response

def displayAtsSatStatic():
    response = getAtsSatStatic()
    if response == -1:
        print("Error getting software info")
        return -1
    print("SimCardNumber                                : " + str(response.SatStaticValue.SimCardNumber))
    print("InternationalMobileEquipmentID               : " + str(response.SatStaticValue.InternationalMobileEquipmentID))
    print("InternationalMobileSubscriberID              : " + str(response.SatStaticValue.InternationalMobileSubscriberID))
    print("MobileEquipmentIdentifier                    : " + response.SatStaticValue.MobileEquipmentIdentifier)
    print("CircuitSwitchedDialNumber                    : " + response.SatStaticValue.CircuitSwitchedDialNumber)
    print("ModemFirmwareVersion                         : " + response.SatStaticValue.ModemFirmwareVersion)

if __name__ == "__main__":
    displayAtsSatStatic()