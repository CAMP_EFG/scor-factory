import os, sys, configparser

absPathOfFile = os.path.dirname(os.path.realpath(__file__))
currentWd = os.getcwd()
os.chdir(absPathOfFile) # change absolute path of atsMaster.py to import atsCalampgrpc


##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
	sys.path.append(p)

from atsCalampCommonPy35 import atsSetup, getTargetIP
atsSetup()
targetIP = ''
##########################################################################################

import importlib
import atsCalampgrpc
importlib.reload(atsCalampgrpc)
from atsCalampgrpc import *

os.chdir(currentWd) # then change directory back to original cwd

import RadioManager_pb2
import RadioManager_pb2_grpc

'''
Main script to get modem info
Example:
atsGetSyslogStream.py:

    oem-efg@EDP-LAB-ENC-08:~/scorpion-ats-maint/scripts$ python3 atsGetSyslogStream.py
    Jan 1 17:15:07 buildroot user.info syslog: Could not read ignition source from USB
    Jan 1 17:15:09 buildroot user.info syslog: Could not read ignition source from USB
    Jan 1 17:15:10 buildroot user.info syslog: Could not read ignition source from USB
    Jan 1 17:15:11 buildroot user.info syslog: Could not read ignition source from USB
    ^CReceived ctrl-c
    oem-efg@EDP-LAB-ENC-08:~/scorpion-ats-maint/scripts$
'''
def grpcGetSyslogStream():
    try:
        global targetIP
        if targetIP == '': targetIP = getTargetIP()
        channel = getClientChannel(targetIP)
        modemMgr_stub = RadioManager_pb2_grpc.LogManagerStub(channel)
        request = RadioManager_pb2.ATSLog()
        responses = modemMgr_stub.GetSyslogStream(request)
        for response in responses:
            print(str(response.AtsLogContent))
    except Exception as e:
        print(str(e))
        return -1
    except KeyboardInterrupt:
        print("Received ctrl-c")
        channel.close()

grpcGetSyslogStream()