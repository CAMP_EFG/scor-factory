import os, sys, logging, time

absPathOfFile = os.path.dirname(os.path.realpath(__file__))
currentWd = os.getcwd()
os.chdir(absPathOfFile) # change absolute path of atsMaster.py to import atsCalampgrpc


##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
	sys.path.append(p)

from atsCalampCommonPy35 import verifyPyVer, getTargetIP, debugDump, getTargetIP, atsSetup
verifyPyVer()
atsSetup()
targetIP = ''
##########################################################################################

import importlib
import atsCalampgrpc
importlib.reload(atsCalampgrpc)
from atsCalampgrpc import *

os.chdir(currentWd) # then change directory back to original cwd

Logger = logging.getLogger('ATE2_Logger.atsModemCommand')

'''
Main modem command
'''
def grpcAtsSendCommand(msgId, value1 = None, value2 = None, value3 = None, value4 = None, getIP = False):
	try:
		global targetIP
		if getIP == True:
			Logger.debug(f'targetIP before PC = {targetIP}')
			targetIP = getTargetIP()
			Logger.debug(f'targetIP after PC = {targetIP}')
			return targetIP
		if targetIP == '': targetIP = getTargetIP()
		channel = getClientChannel(targetIP)
		modemMgr_stub = RadioManager_pb2_grpc.ModemManagerStub(channel)
		request = RadioManager_pb2.ModemCommandRequest(Command = int(msgId), CommandValue = value1, AtsCommandValue1 = value2, AtsCommandValue2 = value3, AtsCommandValue3 = value4)
		response = modemMgr_stub.SendCommand(request)
		Logger.debug(f'atsModemCommand response = {response}')
		channel.close()
		if debugDump == 'yes':
			if msgId == RadioManager_pb2.ATS_CELL_RX_CW_TEST_MODE_ON_OFF_CTRL:
				if value1 == 'start':
					signals = response.AtsCommandResponse.replace('2Ok ','').replace(' ', ',')
					print('Signals: {}'.format(signals))
			else:
				print(response.AtsCommandResponse)
		print("OK")
		return 0
	except Exception as e:
		printErr(e, 'c')
		return -1