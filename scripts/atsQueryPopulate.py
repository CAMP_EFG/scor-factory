import os, sys

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################


import atsCalampgrpc as grpcLib
import RadioManager_pb2 as pb2


'''
Description: Query all the modem infos according to the ones defined in the RadioManager_pb2
'''
if __name__ == "__main__":
    atsRpc = grpcLib.atsGrpcLocal()
    fq = open("queryResult", "w")

    # QUERY
    idx = 0
    while idx < 100:
        try:
            print("Querying INFO: " + pb2.MODEM_MESSAGE.Name(idx))
            response = atsRpc.atsGrpcGetModeminfo(idx)
            fq.write(pb2.MODEM_MESSAGE.Name(idx) + ": " + str(response) + "\n\n")
            print(response)
        except Exception:
            pass
        idx += 1

    fq.close()