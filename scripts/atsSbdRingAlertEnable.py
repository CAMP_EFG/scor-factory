import os, sys, argparse

####################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
####################################

from atsCalampgrpc import *
COMMAND = RadioManager_pb2.SHORT_BURST_DATA_RING_ALERT_ENABLE
from atsModemCommand import grpcAtsSendCommand

'''
Description: sdb ring alert enable
Example:
	python atsSbdRingAlertEnable.py
'''
def grpcAtsShortBurstDataRingAlertEnable(value1 = None, value2 = None, value3 = None, value4 = None):
	grpcAtsSendCommand(COMMAND, value1, value2, value3, value4)


if __name__ == "__main__":
	grpcAtsShortBurstDataRingAlertEnable()