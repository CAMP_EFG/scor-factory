from atsModeminfo import *
import os, sys, argparse, time, pdb, atsBoardCount

COMMAND = RadioManager_pb2.CELL_DYNAMIC

'''
Description: Displays Cell Dynamic Info
Example:
	python atsCellDynamic.py
'''
def getAtsCellDynamic():
    response = grpcGetModemInfo(COMMAND)
    return response

def checkWD():
    try:
        count = 5
        response = getAtsCellDynamic()
        while count != 0:
            response = getAtsCellDynamic()
            if 'ModemState: MODEM_STATE_WIRELESS_DISABLE' in str(response):
                print("Wireless Disabled")
                atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_GRPC', True)
                return 0
            print("Rechecking wireless disable, sleeping 5 seconds...")
            time.sleep(5)
            count -= 1
    except Exception as e:
        print("Received error: " + str(e))
        atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_GRPC', False)
        return -1
    if count == 0:
        print("Wireless NOT Disabled")
        atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_GRPC', False)
        return -1


if __name__ == "__main__":
    checkWD()