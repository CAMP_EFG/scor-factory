from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_GET_ACCEL_VALUE

'''
Description: Displays the accel value
Example:
	python atsGetGpsInfo.py
'''
def getAtsAccelValue():
    response = grpcGetModemInfo(COMMAND)
    print("Raw output: " + str(response))
    return response

def displayAtsAccelValue():
    response = getAtsAccelValue()
    if response == -1:
        print("Error getting ats accel value")
        return -1
    print("AtsAccelValue                 : " + response.AtsAccelInfo.AtsAccelValue)

if __name__ == "__main__":
    displayAtsAccelValue()