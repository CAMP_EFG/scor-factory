
import os, time, pdb, atsBoardCount

url = "http://172.31.234.158/cgi-bin/set_hibernate.cgi"
filePath = '../temp'
cgiFile = 'set_hibernate.cgi'

def readConfigHibernateState():
	absPathOfFile = os.path.dirname(os.path.realpath(__file__))
	currentWd = os.getcwd()
	os.chdir(absPathOfFile)
	import requests
	count = 0
	countMax = 10
	while count < countMax:
		try:
			p = os.path.join(filePath,cgiFile)
			if os.path.exists(p):
				os.remove(p)
			response = requests.get(url,stream = True, timeout = 10)
			#response = requests.get(url,timeout = 5)
			with open(p,'wb') as f:
				f.write(response.content)
			datafile = open(p,'r')
			for line in datafile:
				print("Content in file: " + line)
				if "Device has been set to Hibernate" in line:
					print(line)
					f.close()
					datafile.close()
					atsBoardCount.boardPassFail('boardPassFail.cfg', 'SET_HIBERNATE', True)
					return 0
			datafile.close()
			f.close()
			print("Hibernate not set. Sleeping 5 minutes...")
			time.sleep(300)
			count += 1
		except Exception as e:
			atsBoardCount.boardPassFail('boardPassFail.cfg', 'SET_HIBERNATE', False)
			print("Received error: " + str(e))
			count += 1
			print("Sleeping 5 minutes...")
			time.sleep(300)
	if count >= countMax:
		atsBoardCount.boardPassFail('boardPassFail.cfg', 'SET_HIBERNATE', False)
		return -1
	else: time.sleep(10)
	os.chdir(currentWd)

if __name__ == "__main__":
	readConfigHibernateState()