from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_GET_TEMP

'''
Description: Display temp info
Example: 
	python atsGetTempInfo.py
'''
def getAtsTempInfo():
    response = grpcGetModemInfo(COMMAND)
    print("Raw response: " + str(response))
    return response

def displayAtsTempInfo():
    response = getAtsTempInfo()
    if response == -1:
        print("Error getting software info")
        return -1
    print("AtsImxTemp                : " + str(response.AtsTempInfo.AtsImxTemp))
    print("AtsCellTemp               : " + str(response.AtsTempInfo.AtsCellTemp))
    print("AtsIridiumTemp            : " + str(response.AtsTempInfo.AtsIridiumTemp))

if __name__ == "__main__":
    displayAtsTempInfo()