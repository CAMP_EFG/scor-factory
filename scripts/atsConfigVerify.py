import os, sys, argparse, pdb

p = os.path.abspath('../lib')
sys.path.append(p)
from atsExtractMacAddr import extractMacAddr

def configVerify(fl2, mac):
    print("Verifying OS and app...")
    ftpushCommand_2 = 'python ../tools/calamp_ftppush.py --fl2 ' + fl2 + ' -z 7z -d ' +  mac + ' --flash_select verify_only'
    print("Flashing App images from FL2 package...")
    if os.system(ftpushCommand_2) != 0: return -1
    print("==================================================")
    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Description:\nFlash OS script', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-fl2',  dest='fl2',  default = '', type=str,  help='v - CommandValue')
    args = parser.parse_args()
    mac = extractMacAddr()
    configVerify(args.fl2, mac)