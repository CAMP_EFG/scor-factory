from atsModeminfo import *
COMMAND = RadioManager_pb2.CELL_STATIC


'''
Description: Displays Cell Static info
Example: 
	python atsCellStatic.py
'''
def getAtsCellStatic():
    response = grpcGetModemInfo(COMMAND)
    print("Raw response: " + str(response))
    return response

def displayAtsCellStatic():
    response = getAtsCellStatic()
    if response == -1:
        print("Error getting software info")
        return -1
    print("SimCardNumber                                : " + str(response.CellStaticValue.SimCardNumber))
    print("InternationalMobileEquipmentID               : " + str(response.CellStaticValue.InternationalMobileEquipmentID))
    print("InternationalMobileSubscriberID              : " + str(response.CellStaticValue.InternationalMobileSubscriberID))
    print("MobileEquipmentIdentifier                    : " + response.CellStaticValue.MobileEquipmentIdentifier)
    print("ModemFirmwareVersion                         : " + response.CellStaticValue.ModemFirmwareVersion)
    print("AccessPointName                              : " + response.CellStaticValue.AccessPointName)
    print("APNUserName                                  : " + response.CellStaticValue.APNUserName)
    print("APNPassword                                  : " + response.CellStaticValue.APNPassword)

if __name__ == "__main__":
    displayAtsCellStatic()