'''
Server prototype in Python
NOTE: Do not run this
'''
import socket
import os
import re
import sys
import atsCalampgrpc as grpcLib

grpcLib.verifyPyVer()
import configparser

cfg = configparser.ConfigParser()
cfg.read('atsGrpcconfig.cfg')
svr = cfg['SERVER_INFO']
msg = cfg['MODEM_MESSAGE']
cmd = cfg['MODEM_COMMAND']

def startServer():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("Socket successfully created")
        host_ip = 'localhost'
        port = int(svr.get('SERVER_PORT'))
        s.bind((host_ip, port))
        print("socket binded to %s" %(port))
        s.listen(5)
        print("socket is listening")

        while True:
            c, addr = s.accept()
            print("Connected by " + str(addr))

            data = c.recv(1024)
            print(data)
            OK = ('OK').encode('utf-8')
            c.sendall(b'OK')
            print("Sent OK")
    except socket.error as err:
        print("socket creation failed with error %s" %(err))

startServer()