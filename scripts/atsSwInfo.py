from atsModeminfo import *
COMMAND = RadioManager_pb2.SOFTWARE_INFO

'''
Description: Displays the software info
Example:
	python atsSwInfo.py
'''
def getAtsSwInfo():
    response = grpcGetModemInfo(COMMAND)
    print("Raw response: " + str(response))
    return response

def displayAtsSwInfo():
    response = getAtsSwInfo()
    if response == -1:
        print("Error getting software info")
        return -1
    print("SupplierSoftwarePartNumber                   : " + str(response.SoftwareInfoValue.SupplierSoftwarePartNumber))
    print("CaterpillarSoftwarePartNumber                : " + str(response.SoftwareInfoValue.CaterpillarSoftwarePartNumber))
    print("ATSCellModemFwVersion                        : " + response.SoftwareInfoValue.AtsCellModemFwVersion)
    print("ATSIridiumModemFwVersion                     : " + response.SoftwareInfoValue.AtsIridiumModemFwVersion)
    print("ATSK22FwVersion                              : " + response.SoftwareInfoValue.AtsK22FwVersion)
    print("ATSKernelVersion                              : " + str(response.SoftwareInfoValue.AtsKernelVersion))

if __name__ == "__main__":
    displayAtsSwInfo()