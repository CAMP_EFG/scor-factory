from atsModeminfo import *

'''
Description: Display battery fault info
Example:
	python atsK22GetBattFault.py
'''
def getk22BattFault():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_BATT_FAULT)
    print("Raw output: " + str(response))
    return response

def displayK22BattFault():
    response = getk22BattFault()
    if response == -1:
        print("Error getting ats k22 batt fault info")
        return -1
    print("AtsBatterHealth                   : " + str(response.AtsBatteryFaultInfo.AtsBatterHealth))

if __name__ == "__main__":
    displayK22BattFault()