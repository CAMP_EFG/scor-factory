import os, subprocess, sys, time, pdb
def extractIPAddr():
    absPathOfFile = os.path.dirname(os.path.realpath(__file__))
    currentWd = os.getcwd()
    os.chdir(absPathOfFile) # change absolute path of atsMaster.py to import atsCalampgrpc

    global IP
    print("Extracting IP address from thd.py")
    thdCommand = 'python ../lib/toolsThd.py -t 1'
    #thdCommand = '/usr/bin/python3.5 thd.py -t 1'
    result = subprocess.Popen(thdCommand, shell=True, stdout=subprocess.PIPE)
    #time.sleep(2)
    IP = ''
    raw = str(result.stdout.read())
    #print("RESULT: " + raw)
    #print("======================================================")
    formatted = raw.split('\\r\\n')
    try:
        count = 0
        while count < 4:
            if count == 0 and formatted[0][26:39].strip() != '165.26.79.1': break
            if count == 1 and formatted[1][24:37].strip() != '165.26.79.1': break
            if count == 2 and formatted[2][24:37].strip() != '165.26.79.1': break
            if count == 3 and formatted[3][24:37].strip() != '165.26.79.1': break
            count += 1
        if count == 0: IP = formatted[count][26:39].strip()
        else: IP = formatted[count][24:37].strip()
        if IP == '165.26.79.1' or IP == '':
            print(IP)
            print("No IP address found")
            return -1
        else:
            print("IP addr found: " + IP)
            os.chdir(currentWd) # then change directory back to original cwd
            return IP
    except: pass

    return 0

#if __name__ == "__main__":
    #extractIPAddr()