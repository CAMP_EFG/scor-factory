#This script is used to download the CGI file from the target by making a http connection
#Call http://172.31.234.158/cgi-bin/display_reprogramming_log.cgi This will download the data to a file in your PC.
#Looks at the last occurrence of “Prepping Kinetis firmware (app_t6rxk22_KINETIS_2018.10.0.kinetis)”.
#When matched, extract and record the version string
#Look for the occurrence of “Flashing Kinetis” string after the above check.
#Check the next line to see if "done" or "status:" and return version or -1 respectively.

import atsBoardCount
import os, time

''' GLobal variables'''
linecount=0
revlinecount=0
matchedline=0
found=False
version=""
#create a Temp folder if not present
directory="../temp"

'''
This function downloads the CGI from target.
Surfs through the CGI file finding the last occurance of "Prepping Kinetis firmware (app_t6rxk22_KINETIS"
Performs parsing and gets the version of K22 '''
def getK22Version():
    absPathOfFile = os.path.dirname(os.path.realpath(__file__))
    currentWd = os.getcwd()
    os.chdir(absPathOfFile)
    
    import requests
    global linecount
    global revlinecount
    global matchedline
    global version
    global found
    # Delete the file “display_reprogramming_log.cgi” if exists
    exists= os.path.isfile("../temp/display_reprogramming_log.cgi")

    if exists:
        os.remove("../temp/display_reprogramming_log.cgi")
        print("Old file removed")
        time.sleep(2)

    ''' The following downloads the CGI file by calling a web service. Equivalent to a wget'''
    url="http://172.31.234.158/cgi-bin/display_reprogramming_log.cgi"
    response =requests.get(url, stream= True)

    with open(directory+"/display_reprogramming_log.cgi",'wb') as f:
       f.write(response.content)
       print("Download complete")
       f.close()  ## Somu
       time.sleep(2)

    #to find the total number of lines in the file
    ## Somu
    linecount=0
    with open(directory+"/display_reprogramming_log.cgi",'r') as f:
        for line in list(f):
            linecount+=1
            #print(linecount)
        f.close() ## Somu
        print("Total Lines in prog file: "+str(linecount))
        time.sleep(2)

    #start looking in reverse order so it is easier to locate the search string
    found = False ## Somu
    VerSplit=""
    revlinecount=0
    with open(directory+"/display_reprogramming_log.cgi",'r') as f:
        for line in reversed(list(f)):
            revlinecount+=1
            if ("Prepping Kinetis firmware (app_t6rxk2x_KINETIS" in line) or ("Prepping Kinetis firmware (app_t6rxk22_KINETIS" in line):
                found = True
                #print(line)
                version=""
                VerSplit=line.split("_")[3]
                for i in VerSplit:
                    if(i!=')'):
                        version += str(i)
                    else:
                        #print ("Matched (")
                        break
                print("Version: "+version)
                print("Reversed Linecount="+str(revlinecount))
                break
        f.close() ## Somu
        time.sleep(2)

## Somu
    matchedline=0
    if(found == False):
        print("K22 version found is False...")
        return "EMPTY"
    else:
        matchedline = linecount-revlinecount
        linecount = 0
        print("K22 version found = True..., matchedline="+str(matchedline))
        os.chdir(currentWd)
        return version
    os.chdir(currentWd)

''' This function finds the occurance of Flashing Kinetics and checks for the message in the following line.
    returns the version if "done" is found
    returns -1 if "status:" is found in the next line'''
def getFlashingKinetisStatus():
    absPathOfFile = os.path.dirname(os.path.realpath(__file__))
    currentWd = os.getcwd()
    os.chdir(absPathOfFile)
    
    
    count = 0
    countMax = 5

    while count < countMax:
        try:
            K22Ver=getK22Version()
            if "EMPTY" in K22Ver:
                print ("K22 version string is EMPTY-"+str(count))
                count += 1
                print("Sleeping 10 seconds...")
                time.sleep(10)
            else:
                break
        except Exception as e:
            print("Sleeping 10 seconds...")
            time.sleep(10)
            os.chdir(currentWd)
            count += 1

    if (count >= countMax):
        print ("FAILED: K22 version string is EMPTY")
        atsBoardCount.boardPassFail('boardPassFail.cfg', 'K22', False)
        return -1

    count = 0
    countMax = 6
    while count < countMax:
        try:
            global linecount
            with open(directory+"/display_reprogramming_log.cgi",'r') as f:
                #for line in reversed(list(datafile)):
                for line in f:
                    linecount+=1
                    if (linecount >= matchedline):
                        #print(line)
                        print(line)
                        print("linecount="+str(linecount))
                        print("matchedline="+str(matchedline))
                        if "Flashing Kinetis" in line:
                            line=next(f)
                            print(line)
                            if "done" in line:
                                print("======================================================")
                                print("K22 VERSION READ SUCCESS: Version: "+K22Ver)
                                print("======================================================")
                                f.close()
                                atsBoardCount.boardPassFail('boardPassFail.cfg', 'K22', True)
                                time.sleep(10)
                                os.chdir(currentWd)
                                return K22Ver
                            elif "status:" in line:
                                f.close()
                                atsBoardCount.boardPassFail('boardPassFail.cfg', 'K22', False)
                                return -1
                f.close()
            count += 1
            print("Sleeping 1 second...")
            time.sleep(1)
        except Exception as e:
            atsBoardCount.boardPassFail('boardPassFail.cfg', 'K22', False)
            print("Exception: Sleeping 5 minutes...")
            time.sleep(300)
            count += 1
    os.chdir(currentWd)
    if count >= countMax:
        print("Flashing Kinetis not found\n")
        atsBoardCount.boardPassFail('boardPassFail.cfg', 'K22', False)
        return -1
    
    

if __name__ == "__main__":
    status=getFlashingKinetisStatus()