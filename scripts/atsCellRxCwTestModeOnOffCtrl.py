import os, sys, argparse

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################

from atsCalampgrpc import *
COMMAND = RadioManager_pb2.ATS_CELL_RX_CW_TEST_MODE_ON_OFF_CTRL
from atsModemCommand import grpcAtsSendCommand

'''
Description: Cell Tx cw Test
Example:
		python atsCellTxCwTestModeOnOffCtrl.py -v start -v1 1 -v2 9400
		python atsCellTxCwTestModeOnOffCtrl.py -v stop
		python atsCellRxCwTestModeOnOffCtrl.py -v start -v1 3 -v2 9400
		python atsCellRxCwTestModeOnOffCtrl.py -v stop
	Response:
			Sucess:
				Signals: -71,-101
				OK
			Fail: Grpc send command error
'''
def grpcAtsCellRxCwTestModeOnOffCtrl(value1 = None, value2 = None, value3 = None, value4 = None):
	grpcAtsSendCommand(COMMAND, value1, value2, value3, value4)


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Description:\nAn interface to send grpc ATS commands individually.\nAuthor:\t\tJoshua Liew\nDate Created:\t7/24/2018', formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
	parser.add_argument('-v1', dest='value1', default = '', type=str, help='v1 - AtsCommandValue1')
	parser.add_argument('-v2', dest='value2', default = '', type=str, help='v2 - AtsCommandValue2')
	parser.add_argument('-v3', dest='value3', default = '', type=str, help='v3 - AtsCommandValue3')
	args = parser.parse_args()
	grpcAtsCellRxCwTestModeOnOffCtrl(args.value, args.value1, args.value2, args.value3)