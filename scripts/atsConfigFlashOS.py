import os, sys, argparse

p = os.path.abspath('../lib')
sys.path.append(p)
from atsExtractMacAddr import extractMacAddr

def flashOS(fl2, mac):
    print("Flashing OS...")
    ftpushCommand_1 = 'python ../tools/ftppush.py --fl2 ' + fl2 + ' -z 7z -d ' +  mac + ' --flash_select os --skip_pn_verify '
    print("Flashing OS images from FL2 package...")
    if os.system(ftpushCommand_1) != 0: return -1
    print("==================================================")
    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Description:\nFlash OS script', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-fl2',  dest='fl2',  default = '', type=str,  help='v - CommandValue')
    args = parser.parse_args()
    mac = extractMacAddr()
    flashOS(args.fl2, mac)