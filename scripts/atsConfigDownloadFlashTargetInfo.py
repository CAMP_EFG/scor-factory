from ftplib import FTP

def ftpGetXML():
    try:
        import atsExtractIP
        with FTP(host=atsExtractIP.extractIPAddr(), passwd = 'anonymous') as ftp_session:
            print("Ftp into device...")
            print(ftp_session.login())
            print(ftp_session.cwd('/ipflash'))
            filename = 'FlashTargetInformation.xml'
            f = open(filename, 'wb')
            print(ftp_session.retrbinary('RETR ' + filename, f.write))
            f.close()
            print("Closing ftp connection")
            ftp_session.close()
    except Exception:
        return -1

if __name__ == "__main__":
    ftpGetXML()
