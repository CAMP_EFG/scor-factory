from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_GET_COUNTRY_PROFILE

'''
Description: Displays the country profile
Example:
    Raw output: AtsGetCountryProfile {
    AtsCountryCfg: COUNTRY_ROW
    AtsCertCodeCfg: CELLULAR_CERTIFICATION_2G
    AtsCertCodeCfg: CELLULAR_CERTIFICATION_3G
    AtsCertCodeCfg: CELLULAR_CERTIFICATION_4G
    AtsLteFactoryCfg: "1,2,3,4,5,7,8,12,18,19,20,28"
    AtsCountryEnv: COUNTRY_ROW
    AtsCertCodeEnv: CELLULAR_CERTIFICATION_2G
    AtsCertCodeEnv: CELLULAR_CERTIFICATION_3G
    AtsCertCodeEnv: CELLULAR_CERTIFICATION_4G
    AtsLteFactoryEnv: "1,2,3,4,5,7,8,12,18,19,20,28"
    AtsLteBandsActive: "1,2,3,4,5,7,8,12,18,19,20,28"
    }

    AtsLteFactoryCfg                 : 1,2,3,4,5,7,8,12,18,19,20,28
    AtsCertCodeCfg                 : 1
    AtsCertCodeCfg                 : 2
    AtsCertCodeCfg                 : 3
    AtsCertCodeCfg                 : [1, 2, 3]
    AtsLteFactoryCfg                 : 1,2,3,4,5,7,8,12,18,19,20,28
    AtsCountryEnv                 : 1
    AtsCertCodeEnv                 : 1
    AtsCertCodeEnv                 : 2
    AtsCertCodeEnv                 : 3
    AtsCertCodeEnv                 : [1, 2, 3]
    AtsLteFactoryEnv                 : 1,2,3,4,5,7,8,12,18,19,20,28
    AtsLteBandsActive                 : 1,2,3,4,5,7,8,12,18,19,20,28
'''
def getAtsCountryProfile():
    response = grpcGetModemInfo(COMMAND)
    print("Raw output: " + str(response))
    return response

def displayAtsCountryProfile():
    response = getAtsCountryProfile()
    if response == -1:
        print("Error getting ats country profile")
        return -1
    print("AtsLteFactoryCfg                 : " + str(response.AtsGetCountryProfile.AtsLteFactoryCfg))

    for certCodeCfg in response.AtsGetCountryProfile.AtsCertCodeCfg:
        print("AtsCertCodeCfg                 : " + str(certCodeCfg))

    print("AtsCertCodeCfg                 : " + str(response.AtsGetCountryProfile.AtsCertCodeCfg))
    print("AtsLteFactoryCfg                 : " + str(response.AtsGetCountryProfile.AtsLteFactoryCfg))
    print("AtsCountryEnv                 : " + str(response.AtsGetCountryProfile.AtsCountryEnv))

    for certCodeEnv in response.AtsGetCountryProfile.AtsCertCodeEnv:
        print("AtsCertCodeEnv                 : " + str(certCodeEnv))

    print("AtsCertCodeEnv                 : " + str(response.AtsGetCountryProfile.AtsCertCodeEnv))
    print("AtsLteFactoryEnv                 : " + str(response.AtsGetCountryProfile.AtsLteFactoryEnv))
    print("AtsLteBandsActive                 : " + str(response.AtsGetCountryProfile.AtsLteBandsActive))

if __name__ == "__main__":
    displayAtsCountryProfile()