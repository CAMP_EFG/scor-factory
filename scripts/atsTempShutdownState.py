import os, sys, argparse

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################

from atsCalampgrpc import *
COMMAND = RadioManager_pb2.ATS_MDM_THERMAL_SHUTDOWN_CTRL
from atsModemCommand import grpcAtsSendCommand

'''
Description: Enable/disble THERMAL SHUTDOWN
Example:
	python atsTempshutdownState.py -v on
'''
def grpcAtsTempShutdownState(value1 = None):
	grpcAtsSendCommand(COMMAND, value1)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Description:\nAn interface to send grpc ATS commands individually.\nAuthor:\t\tJoshua Liew\nDate Created:\t7/24/2018', formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')

	args = parser.parse_args()
	grpcAtsTempShutdownState(args.value)
