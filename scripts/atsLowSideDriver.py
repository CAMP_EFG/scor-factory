import os, sys, argparse

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################

from atsCalampgrpc import *
COMMAND = RadioManager_pb2.ATS_LOWSIDE_DRIVER
from atsModemCommand import grpcAtsSendCommand

'''
Description: Execute lowside driver
Command Name: <ATS_LOWSIDE_DRIVER>
	Example:
		python atsLowSideDriver.py -v1 write -v2 enable -v3 1

		python3 atsLowSideDriver.py -v1 status
		output status
		200-Output 1 Short to GND terminal
		200-Output 2 Short to GND terminal
		200 Success
		$
		OK
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsLowsideDriver(value1 = None, value2 = None, value3 = None, value4 = None):
	grpcAtsSendCommand(COMMAND, value1, value2, value3, value4)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Description:\nAn interface to send grpc ATS commands individually.\nAuthor:\t\tJoshua Liew\nDate Created:\t7/24/2018', formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
	parser.add_argument('-v1', dest='value1', default = '', type=str, help='v1 - AtsCommandValue1')
	parser.add_argument('-v2', dest='value2', default = '', type=str, help='v2 - AtsCommandValue2')
	parser.add_argument('-v3', dest='value3', default = '', type=str, help='v3 - AtsCommandValue3')
	args = parser.parse_args()
	grpcAtsLowsideDriver(args.value, args.value1, args.value2, args.value3)