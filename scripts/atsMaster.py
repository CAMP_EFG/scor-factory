'''
Contains all the individual scripts in this directory for convenient import purposes
'''
import os, sys, argparse, requests, subprocess, time, requests, webbrowser, base64, urllib, configparser
from ftplib import FTP
import xml.etree.ElementTree as ET


import importlib
import atsModeminfo, atsModemCommand
importlib.reload(atsModeminfo)
importlib.reload(atsModemCommand)

from atsModeminfo import grpcGetModemInfo
from atsModemCommand import grpcAtsSendCommand

##########################################################################################s
absPathOfFile = os.path.dirname(os.path.realpath(__file__))
currentWd = os.getcwd()
os.chdir(absPathOfFile) # change absolute path of atsMaster.py to import atsCalampgrpc


p = os.path.abspath('../lib/')
if p not in sys.path:
	sys.path.append(p)


from atsCalampCommonPy35 import targetIP
from atsCalampgrpc import *
os.chdir(currentWd) # then change directory back to original cwd
##########################################################################################

INVALID_COUNTRY = 0
COUNTRY_ROW = 1
COUNTRY_CHINA = 2



def grpcAtsAtPassthruOnOffCtrl(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_AT_PASSTHRU_ON_OFF_CTRL, value1, value2, value3, value4)

'''
Description: Turn on to auto reconnect
Command Name: <ATS_AUTO_RECONNECT>
Example:
	python atsAutoReconnect.py -v on
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsAutoReconnect(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_AUTO_RECONNECT, value1, value2, value3, value4)

'''
Run the script to burn OTP fuses by downloading app image or verity image
Command Name: <ATS_BRUN_OTP_FUSES>
	Example:
		python atsBurnOtpFuses.py -v install -v1 otp_script.sh -v2 165.26.79.1
Response:
		Success: “OK”
		Fail: Grpc send command error

'''
def grpcAtsBurnOtpFuses(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_BURN_OTP_FUSES, value1, value2, value3, value4)

'''
Description: Run CAN Test
Example:
	python atsCanTestOnOffCtrl.py
'''
def grpcCanTestOnOffCtrl(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_CAN_TEST_ON_OFF_CTRL, value1, value2, value3, value4)


'''
Description: Displays Cell Dynamic Info
Example:
	python atsCellDynamic.py
'''
def getAtsCellDynamic():
    response = grpcGetModemInfo(RadioManager_pb2.CELL_DYNAMIC)
    print("Raw response: " + str(response))
    return response

def displayAtsCellDynamic():
    response = getAtsCellDynamic()
    if response == -1:
        print("Error getting software info")
        return -1
    print("MobileCountryCode                                : " + str(response.CellDynamicValue.MobileCountryCode))
    print("MobileNetworkCode                                : " + str(response.CellDynamicValue.MobileNetworkCode))
    print("SignalStrength                                   : " + str(response.CellDynamicValue.SignalStrength))
    print("ServiceProviderName                              : " + response.CellDynamicValue.ServiceProviderName)
    print("RadioPreferredNetworkMode                        : " + response.CellDynamicValue.RadioPreferredNetworkMode)
    print("PdpContextActive                                 : " + str(response.CellDynamicValue.PdpContextActive))
    print("AntennaDiagnostic                                : " + str(response.CellDynamicValue.AntennaDiagnostic))
    print("Band                                             : " + str(response.CellDynamicValue.Band))
    print("Channel                                          : " + str(response.CellDynamicValue.Channel))
    print("RadioFrequencyChannelNumber                      : " + str(response.CellDynamicValue.RadioFrequencyChannelNumber))
    print("AccessPointBand                                  : " + str(response.CellDynamicValue.AccessPointBand))
    print("ModemState                                       : " + str(response.CellDynamicValue.ModemState))
    print("OperatingMode                                    : " + str(response.CellDynamicValue.OperatingMode))

'''
Description: Cell power down command
Example:
	python atsCellPowerDown.py
'''
def grpcAtsCellPowerDown(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.CELL_POWER_DOWN, value1, value2, value3, value4)

'''
Description: Cell power up command
Example:
	python atsCellPowerUp.py
'''
def grpcAtsCellPowerUp(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.CELL_POWER_UP, value1, value2, value3, value4)

'''
Description: Displays Cell Static info
Example:
	python atsCellStatic.py
'''
def getAtsCellStatic():
    response = grpcGetModemInfo(RadioManager_pb2.CELL_STATIC)
    print("Raw response: " + str(response))
    return response

def displayAtsCellStatic():
    response = getAtsCellStatic()
    if response == -1:
        print("Error getting software info")
        return -1
    print("SimCardNumber                                : " + str(response.CellStaticValue.SimCardNumber))
    print("InternationalMobileEquipmentID               : " + str(response.CellStaticValue.InternationalMobileEquipmentID))
    print("InternationalMobileSubscriberID              : " + str(response.CellStaticValue.InternationalMobileSubscriberID))
    print("MobileEquipmentIdentifier                    : " + response.CellStaticValue.MobileEquipmentIdentifier)
    print("ModemFirmwareVersion                         : " + response.CellStaticValue.ModemFirmwareVersion)
    print("AccessPointName                              : " + response.CellStaticValue.AccessPointName)
    print("APNUserName                                  : " + response.CellStaticValue.APNUserName)
    print("APNPassword                                  : " + response.CellStaticValue.APNPassword)

'''
Description: Cell Tx cw Test
Example:
		python atsCellTxCwTestModeOnOffCtrl.py -v start
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsCellTxCwTestModeOnOffCtrl(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_CELL_TX_CW_TEST_MODE_ON_OFF_CTRL, value1, value2, value3, value4)

'''
Description: Cell Rx cw Test
Example:
		python atsCellRxCwTestModeOnOffCtrl.py -v start
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsCellRxCwTestModeOnOffCtrl(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_CELL_RX_CW_TEST_MODE_ON_OFF_CTRL, value1, value2, value3, value4)


'''
Description: Cell wan down command
Example:
	python atsCellWanDown.py
'''
def grpcAtsCellWanDown(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.CELL_WAN_DOWN, value1, value2, value3, value4)

'''
Description: Cell wan up command
Example:
	python atsCellWanUp.py
'''
def grpcAtsCellWanUp(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.CELL_WAN_UP, value1, value2, value3, value4)


def ftpGetXML():
    try:

        with FTP(host=atsExtractIP.extractIPAddr(), passwd = 'anonymous') as ftp_session:
            print("Ftp into device...")
            print(ftp_session.login())
            print(ftp_session.cwd('/ipflash'))
            filename = 'FlashTargetInformation.xml'
            f = open(filename, 'wb')
            print(ftp_session.retrbinary('RETR ' + filename, f.write))
            f.close()
            print("Closing ftp connection")
            ftp_session.close()
    except Exception:
        return -1



def flashApp(fl2, mac):
    print("flashing App...")
    ftpushCommand_2 = 'python ../tools/ftppush.py --fl2 ' + fl2 + ' -z 7z -d ' +  mac + ' --flash_select app --skip_pn_verify '
    print("Flashing App images from FL2 package...")
    if os.system(ftpushCommand_2) != 0: return -1
    print("==================================================")
    return 0




def flashOS(fl2, mac):
    print("Flashing OS...")
    ftpushCommand_1 = 'python ../tools/ftppush.py --fl2 ' + fl2 + ' -z 7z -d ' +  mac + ' --flash_select os --skip_pn_verify '
    print("Flashing OS images from FL2 package...")
    if os.system(ftpushCommand_1) != 0: return -1
    print("==================================================")
    return 0


''' GLobal variables'''
linecount=0
revlinecount=0
matchedline=0
found=False
version=""
#create a Temp folder if not present
directory="C:\Temp"

'''
This function downloads the CGI from target.
Surfs through the CGI file finding the last occurance of "Prepping Kinetis firmware (app_t6rxk22_KINETIS"
Performs parsing and gets the version of K22 '''
def getK22Version():
    global linecount
    global revlinecount
    global matchedline
    global version
    global found
    # Delete the file “display_reprogramming_log.cgi” if exists
    exists= os.path.isfile("C:\Temp\/display_reprogramming_log.cgi")

    if exists:
        os.remove("C:\Temp\/display_reprogramming_log.cgi")
        print("Old file removed")
        time.sleep(2)

    ''' The following downloads the CGI file by calling a web service. Equivalent to a wget'''
    url="http://172.31.234.158/cgi-bin/display_reprogramming_log.cgi"
    response =requests.get(url, stream= True)

    with open(directory+"/display_reprogramming_log.cgi",'wb') as f:
       f.write(response.content)
       print("Download complete")
       f.close()  ## Somu
       time.sleep(2)

    #to find the total number of lines in the file
    ## Somu
    linecount=0
    with open(directory+"/display_reprogramming_log.cgi",'r') as f:
        for line in list(f):
            linecount+=1
            #print(linecount)
        f.close() ## Somu
        print("Total Lines in prog file: "+str(linecount))
        time.sleep(2)

    #start looking in reverse order so it is easier to locate the search string
    found = False ## Somu
    VerSplit=""
    revlinecount=0
    with open(directory+"/display_reprogramming_log.cgi",'r') as f:
        for line in reversed(list(f)):
            revlinecount+=1
            if "Prepping Kinetis firmware (app_t6rxk22_KINETIS" in line:
                found = True
                #print(line)
                version=""
                VerSplit=line.split("_")[3]
                for i in VerSplit:
                    if(i!=')'):
                        version += str(i)
                    else:
                        #print ("Matched (")
                        break
                print("Version: "+version)
                print("Reversed Linecount="+str(revlinecount))
                break
        f.close() ## Somu
        time.sleep(2)

## Somu
    matchedline=0
    if(found == False):
        print("K22 version found is False...")
        return "EMPTY"
    else:
        matchedline = linecount-revlinecount
        linecount = 0
        print("K22 version found = True..., matchedline="+str(matchedline))
        return version

''' This function finds the occurance of Flashing Kinetics and checks for the message in the following line.
    returns the version if "done" is found
    returns -1 if "status:" is found in the next line'''
def getFlashingKinetisStatus():
        count = 0
        countMax = 5

        while count < countMax:
            try:
                K22Ver=getK22Version()
                if "EMPTY" in K22Ver:
                    print ("K22 version string is EMPTY-"+str(count))
                    count += 1
                    print("Sleeping 10 seconds...")
                    time.sleep(10)
                else:
                    break
            except Exception as e:
                print("Sleeping 10 seconds...")
                time.sleep(10)
                count += 1

        if (count >= countMax):
            print ("FAILED: K22 version string is EMPTY")
            atsBoardCount.boardPassFail('boardPassFail.cfg', 'K22', False)
            return -1

        count = 0
        countMax = 6
        while count < countMax:
            try:
                global linecount
                with open(directory+"/display_reprogramming_log.cgi",'r') as f:
                    #for line in reversed(list(datafile)):
                    for line in f:
                        linecount+=1
                        if (linecount >= matchedline):
                            #print(line)
                            print(line)
                            print("linecount="+str(linecount))
                            print("matchedline="+str(matchedline))
                            if "Flashing Kinetis" in line:
                                line=next(f)
                                print(line)
                                if "done" in line:
                                    print("======================================================")
                                    print("K22 VERSION READ SUCCESS: Version: "+K22Ver)
                                    print("======================================================")
                                    f.close()
                                    atsBoardCount.boardPassFail('boardPassFail.cfg', 'K22', True)
                                    time.sleep(10)
                                    return K22Ver
                                elif "status:" in line:
                                    f.close()
                                    atsBoardCount.boardPassFail('boardPassFail.cfg', 'K22', False)
                                    return -1
                    f.close()
                count += 1
                print("Sleeping 1 second...")
                time.sleep(1)
            except Exception as e:
                print("Exception: Sleeping 5 minutes...")
                time.sleep(300)
                count += 1

        if count >= countMax:
            print("Flashing Kinetis not found\n")
            atsBoardCount.boardPassFail('boardPassFail.cfg', 'K22', False)
            return -1



url = "http://172.31.234.158/ServiceDashboard/ServiceDashboard.xml"
filePath = 'C:/temp'
xmlFile = 'ServiceDashboard.xml'

def readConfigWDStatus():

	count = 5

	p = os.path.join(filePath,xmlFile)

	if os.path.exists(p):
		os.remove(p)

	while count != 0:
		try:
			response = requests.get(url,stream = True, timeout = 10)

			with open(p,'wb') as f:
				f.write(response.content)

			tree = ET.parse(p)
			root = tree.getroot()

			modelName =  root.find('Cell/Radio/Model').text
			print(modelName)

			if modelName == "PL243" or modelName == "PL443":
				modemState = root.find('Cell/Radio/ModemState').text
				print("ModemState = " + modemState)
				if modemState == "Wireless disabled":
					modemState2 = root.find('Cell/Radio/Modemstate2').text
					print("Modemstate2 = " + modemState2)
					if modemState2 == "Not Available":
						print("WD status Test Passed")
						atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_CGI', True)
						return 0

			if modelName == "PL083":
				modemState = root.find('Cell/Radio/ModemState').text
				print("ModemState = " + modemState)
				if modemState == "Wireless disabled":
					modemState2 = root.find('Cell/Radio/Modemstate2').text
					print("Modemstate2 = " + modemState2)
					if modemState2 == "Wireless disabled":
						print("WD status Test Passed")
						atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_CGI', True)
						return 0

			count = count - 1
			time.sleep(4)
		except Exception as e:
			count -= 1

	print("Returning -1")


	atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_CGI', False)
	return -1


url = "http://172.31.234.158/cgi-bin/set_hibernate.cgi"
filePath = 'C:/temp'
cgiFile = 'set_hibernate.cgi'

def readConfigHibernateState():
	count = 0
	countMax = 10
	while count < countMax:
		try:
			p = os.path.join(filePath,cgiFile)
			if os.path.exists(p):
				os.remove(p)
			response = requests.get(url,stream = True, timeout = 10)
			#response = requests.get(url,timeout = 5)
			with open(p,'wb') as f:
				f.write(response.content)
			datafile = open(p,'r')
			for line in datafile:
				print("Content in file: " + line)
				if "Device has been set to Hibernate" in line:
					print(line)
					f.close()
					datafile.close()
					atsBoardCount.boardPassFail('boardPassFail.cfg', 'SET_HIBERNATE', True)
					return 0
			datafile.close()
			f.close()
			print("Hibernate not set. Sleeping 5 minutes...")
			time.sleep(300)
			count += 1
		except Exception as e:
			print("Received error: " + str(e))
			count += 1
			print("Sleeping 5 minutes...")
			time.sleep(300)
	if count >= countMax:
		atsBoardCount.boardPassFail('boardPassFail.cfg', 'SET_HIBERNATE', False)
		return -1
	else: time.sleep(10)


def configVerify(fl2, mac):
    absPathOfFile = os.path.dirname(os.path.realpath(__file__))
    currentWd = os.getcwd()
    os.chdir(absPathOfFile)
    print("Verifying OS and app...")
    ftpushCommand_2 = 'python ../tools/calamp_ftppush.py --fl2 ' + fl2 + ' -z 7z -d ' +  mac + ' --flash_select verify_only'
    print("Flashing App images from FL2 package...")
    if os.system(ftpushCommand_2) != 0: return -1
    print("==================================================")
    os.chdir(currentWd)
    return 0


'''
Description: Displays Cell Dynamic Info
Example:
	python atsCellDynamic.py
'''
def getAtsCellDynamic():
    response = grpcGetModemInfo(RadioManager_pb2.CELL_DYNAMIC)
    return response

def checkWD():
    try:
        count = 5
        response = getAtsCellDynamic()
        while count != 0:
            response = getAtsCellDynamic()
            if 'ModemState: MODEM_STATE_WIRELESS_DISABLE' in str(response):
                print("Wireless Disabled")
                return 0
            print("Rechecking wireless disable, sleeping 5 seconds...")
            time.sleep(5)
            count -= 1
    except Exception as e:
        print("Received error: " + str(e))
        atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_GRPC', False)
        return -1
    if count == 0:
        print("Wireless NOT Disabled")
        atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_GRPC', False)
        return -1

    atsBoardCount.boardPassFail('boardPassFail.cfg', 'WIRELESS_DISABLE_GRPC', True)



'''
Description: Set the unit model of the board currently connected to the pc
Command Name: <ATS_ENV_SET_UNIT_MODEL>
	Example:
		python atsEnvSetUnitModel.py -v PL243
		Response:
			Success: “OK”
			Fail: Grpc send command error
'''
def grpcAtsEnvSetUnitModel(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_ENV_SET_UNIT_MODEL, value1, value2, value3, value4)

'''
Description: Execute a certain script in the OS kernel
Command Name: <ATS_EXECUTE_SCRIPT>
	Example:
		python atsSendAtCmd.py -v S01mmgr -v2 stop
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsExecuteScript(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_EXECUTE_SCRIPT, value1, value2, value3, value4)


'''
Date: 2/14/2019
Desciption: extracts mac address using the latest thd.py located in tools/ folder
'''
def extractMacAddr():
    print("Extracting mac address from thd.py")
    thdCommand = 'python ../lib/toolsThd.py -t 1'
    result = subprocess.Popen(thdCommand, shell=True, stdout=subprocess.PIPE)
    mac = ''
    raw = str(result.stdout.read())
    formatted = raw.split('\\r\\n')
    try:
        count = 0
        while count < 4:
            if count == 0 and formatted[0][7:19] != 'AAAAAAAAAAAA': break
            if count == 1 and formatted[1][5:17] != 'AAAAAAAAAAAA': break
            if count == 2 and formatted[2][5:17] != 'AAAAAAAAAAAA': break
            if count == 3 and formatted[3][5:17] != 'AAAAAAAAAAAA': break
            count += 1
        if count == 0: mac = formatted[count][7:19]
        else: mac = formatted[count][5:17]
        if mac == 'AAAAAAAAAAAA' or mac == '':
            print(mac)
            print("No mac address found")
            return -1
        else: print("MAC addr found: " + mac)
    except: pass

    return mac



'''
Description: Displays the accel value
Example:
	python atsGetGpsInfo.py
'''
def getAtsAccelValue():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_GET_ACCEL_VALUE)
    print("Raw output: " + str(response))
    return response

def displayAtsAccelValue():
    response = getAtsAccelValue()
    if response == -1:
        print("Error getting ats accel value")
        return -1
    print("AtsAccelValue                 : " + response.AtsAccelInfo.AtsAccelValue)

'''
Description: Wrapper script to get info from swinfo and satstatic
python atsGetFWInfoRead.py
'''
def displayFWInfoRead():
    swInfo = getAtsSwInfo()
    if swInfo == -1:
        print("Error getting software info")
        return -1
    print("SupplierSoftwarePartNumber                   : " + str(swInfo.SoftwareInfoValue.SupplierSoftwarePartNumber))
    print("CaterpillarSoftwarePartNumber                : " + str(swInfo.SoftwareInfoValue.CaterpillarSoftwarePartNumber))
    #print("ATSCalAmpFwVersion                           : " + str(swInfo.SoftwareInfoValue.AtsCalAmpFwVersion))
    print("ATSCellModemFwVersion                        : " + swInfo.SoftwareInfoValue.AtsCellModemFwVersion)
    print("ATSIridiumModemFwVersion                     : " + swInfo.SoftwareInfoValue.AtsIridiumModemFwVersion)
    print("ATSK22FwVersion                              : " + swInfo.SoftwareInfoValue.AtsK22FwVersion)

    satStatic = getAtsSatStatic()
    if satStatic == -1:
        print("Error getting software info")
        return -1
    print("ModemFirmwareVersion                         : " + satStatic.SatStaticValue.ModemFirmwareVersion)


'''
Description: Displays the gps info
Example:
	python atsGetGpsInfo.py
'''
def getAtsGpsInfo():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_GET_GPS_INFO)
    print("Raw output: " + str(response))
    return response

def displayAtsGpsInfo():
    response = getAtsGpsInfo()
    if response == -1:
        print("Error getting ats gps info")
        return -1
    print("AtsGpsFix                       : " + response.AtsGpsInfo.AtsGpsFix)
    print("AtsLatLong                      : " + response.AtsGpsInfo.AtsLatLong)
    print("AtsDateTime                     : " + response.AtsGpsInfo.AtsDateTime)
    print("AtsNumOfSatInView               : " + response.AtsGpsInfo.AtsNumOfSatInView)
    print("AtsGroundSpeed                  : " + response.AtsGpsInfo.AtsGroundSpeed)
    print("AtsSatCNRInfo                   : " + response.AtsGpsInfo.AtsSatCNRInfo)
    print("AtsCNRAverage                   : " + response.AtsGpsInfo.AtsCNRAverage)



'''
Description: Displays the gps sat info
Example:
	python atsGetGpsSatInfo.py
'''
def getAtsGpsSatInfo():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_GET_GPS_SAT_INFO)
    print("Raw output: " + str(response))
    return response

def displayAtsGpsSatInfo():
    response = getAtsGpsSatInfo()
    gpsInfo = getAtsGpsInfo()

    if response == -1 or gpsInfo == -1:
        print("Error getting ats gps sat or gps info")
        return -1

    output = ""
    output += str(gpsInfo.AtsGpsInfo.AtsGpsFix) + ":"
    count = 0
    #print("==================================================================================")
    for satInfo in response.AtsGpsSatInfo.AtsSatvsCnr:
        #print("AtsSatNumber " + str(count) + "                    : " + str(satInfo.AtsSatNumber))
        #print("AtsSatCnr " + str(count) + "                       : " + str(satInfo.AtsSatCnr))
        #print("==================================================================================")
        count += 1

        output += str(satInfo.AtsSatNumber) + "," + str(satInfo.AtsSatCnr) + ":"
    print(output)


'''
Description: Display input states
Example:
	python atsGetInputStates.py
'''
def getAtsInputStates():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_GET_INPUT_STATES)
    print("Raw output: " + str(response))
    return response

def displayAtsInputStates():
    response = getAtsInputStates()
    if response == -1:
        print("Error getting ats input state")
        return -1
    print("AtsKeySwState                 : " + response.AtsGpioInfo.AtsKeySwState)
    print("AtsWirelessDisState           : " + response.AtsGpioInfo.AtsWirelessDisState)
    print("AtsK22KeySwState              : " + response.AtsGpioInfo.AtsK22KeySwState)
    print("AtsK22WirelessDisState        : " + response.AtsGpioInfo.AtsK22WirelessDisState)
    print("AtsK22AccelIntState           : " + response.AtsGpioInfo.AtsK22AccelIntState)
    print("AtsK22RtcIntState             : " + response.AtsGpioInfo.AtsK22RtcIntState)
    print("AtsK22eFuse                   : " + response.AtsGpioInfo.AtsK22eFuse)
    print("AtsK22eWakeUpState            : " + response.AtsGpioInfo.AtsK22eWakeUpState)


'''
Description: Display rtc time
Example:
	python atsGetRtcTime.py
'''
def getAtsRtcTime():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_GET_RTC_TIME)
    return response

def displayAtsRtcTime():
    response = getAtsRtcTime()
    if response == -1:
        print("Error getting ats rtc time")
        return -1
    print("AtsRtcDateTime                          : " + str(response.AtsRTCTime.AtsRtcDateTime))


'''
Description: Display temp info
Example:
	python atsGetTempInfo.py
'''
def getAtsTempInfo():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_GET_TEMP)
    print("Raw response: " + str(response))
    return response

def displayAtsTempInfo():
    response = getAtsTempInfo()
    if response == -1:
        print("Error getting software info")
        return -1
    print("AtsImxTemp                : " + str(response.AtsTempInfo.AtsImxTemp))
    print("AtsCellTemp               : " + str(response.AtsTempInfo.AtsCellTemp))
    print("AtsIridiumTemp            : " + str(response.AtsTempInfo.AtsIridiumTemp))


'''
Description: Turn on/off gps
Example:
		python atsGpsOnOffCtrl.py -v off
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsGpsOnOffCtrl(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_GPS_ON_OFF_CTRL, value1, value2, value3, value4)

'''
Description: Displays the hardware info
Example:
	python atsHardwareInfo.py
'''
def getAtsHardwareInfo():
    response = grpcGetModemInfo(RadioManager_pb2.HARDWARE_INFO)
    print("Raw response: " + str(response))
    return response

def displayAtsHardwareInfo():
    response = getAtsHardwareInfo()
    if response == -1:
        print("Error getting ats hardware info")
        return -1
    print("Serial Number                        : " + str(response.HardwareInfoValue.SerialNumber))
    print("HardwarePartNumber                   : " + str(response.HardwareInfoValue.HardwarePartNumber))
    print("RadioType                            : " + str(response.HardwareInfoValue.RadioType))
    print("EthernetMacAddress                   : " + str(response.HardwareInfoValue.EthernetMacAddress))
    print("SupplierPartNumber                   : " + str(response.HardwareInfoValue.SupplierPartNumber))
    print("ATSMpiCustPartNum                    : " + str(response.HardwareInfoValue.AtsMpiCustPartNum))
    print("ATSMpiAfterMarketID                  : " + str(response.HardwareInfoValue.AtsMpiAfterMarketID))
    print("ATSMpiEcmID                          : " + str(response.HardwareInfoValue.AtsMpiEcmID))
    print("ATSMpiPlatformID                     : " + str(response.HardwareInfoValue.AtsMpiPlatformID))
    print("ATSMpiMfgDate                        : " + str(response.HardwareInfoValue.AtsMpiMfgDate))
    print("ATSK22HwBoardId                      : " + str(response.HardwareInfoValue.AtsK22HwBoardId))
    print("ATSK22ActivePwrSource                : " + str(response.HardwareInfoValue.AtsK22ActivePwrSource))
    print("ATSK22LastRestartCause               : " + str(response.HardwareInfoValue.AtsK22LastRestartCause))


'''
Description: Enable/disble CELL WAN status
Example:
	python atsHmiCellWanStatus -v on
'''
def grpcAtsHmiCellWanStatus(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_HMI_CELL_WAN_STATUS, value1, value2, value3, value4)

'''
Description: Wrapper script to dispplay info from software info cell dynamic, and sat static
Example:
	python atsInfoCellRead.py
'''
def displayInfoCellRead():
    swInfo = getAtsSwInfo()
    if swInfo == -1:
        print("Error getting software info")
        return -1
    #print("ATSCalAmpFwVersion                           : " + str(swInfo.SoftwareInfoValue.AtsCalAmpFwVersion))
    print("ATSCellModemFwVersion                        : " + str(swInfo.SoftwareInfoValue.AtsCellModemFwVersion))
    print("ATSIridiumModemFwVersion                     : " + str(swInfo.SoftwareInfoValue.AtsIridiumModemFwVersion))
    print("ATSK22FwVersion                              : " + str(swInfo.SoftwareInfoValue.AtsK22FwVersion))

    cellDynamic = getAtsCellDynamic()
    if cellDynamic == -1:
        print("Error getting software info")
        return -1
    print("RSSI                                             : " + str(cellDynamic.CellDynamicValue.SignalStrength))
    print("ServiceProviderName                              : " + str(cellDynamic.CellDynamicValue.ServiceProviderName))

    satStatic = getAtsSatStatic()
    if satStatic == -1:
        print("Error getting software info")
        return -1
    print("ICCID                                        : " + str(satStatic.SatStaticValue.SimCardNumber))
    print("InternationalMobileEquipmentID               : " + str(satStatic.SatStaticValue.InternationalMobileEquipmentID))
    print("InternationalMobileSubscriberID              : " + str(satStatic.SatStaticValue.InternationalMobileSubscriberID))
    print("ModemFirmwareVersion                         : " + satStatic.SatStaticValue.ModemFirmwareVersion)

    hwInfo = getAtsHardwareInfo()
    if hwInfo == -1:
        print("Error getting ats hardware info")
        return -1

    print("RadioType                            : " + str(hwInfo.HardwareInfoValue.RadioType))


'''
Description: Installs image based on the parameters
Command Name: <ATS_INSTALL_IMAGE>
Example:
	python atsPingIp.py -v install -v1 calamp_apps_02018101901.verity -v2 165.26.79.1
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsInstallImage(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_INSTALL_IMAGE, value1, value2, value3, value4)


'''
Description: Display k22 battery info
Example:
	python atsK22GetBattInfo.py
'''
def getk22BattInfo():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_BATT_INFO)
    print("Raw output: " + str(response))
    return response

def displayAtsk22BattInfo():
    response = getk22BattInfo()
    if response == -1:
        print("Error getting ats k22 batt info")
        return -1
    print("AtsChargingState                   : " + str(response.AtsBatteryInfo.AtsChargingState))
    print("AtsIntBattHibState                 : " + str(response.AtsBatteryInfo.AtsIntBattHibState))
    print("AtsDACValue100mA                   : " + str(response.AtsBatteryInfo.AtsDACValue100mA))
    print("AtsDACValueZeromA                  : " + str(response.AtsBatteryInfo.AtsDACValueZeromA))
    print("AtsMainBattVolts                   : " + str(response.AtsBatteryInfo.AtsMainBattVolts))
    print("AtsBackUpBattVolts                 : " + str(response.AtsBatteryInfo.AtsBackUpBattVolts))
    print("Ats42BoostVolts                    : " + str(response.AtsBatteryInfo.Ats42BoostVolts))
    print("Ats42SwitchVolts                   : " + str(response.AtsBatteryInfo.Ats42SwitchVolts))
    print("Ats5Volts                   		  : " + str(response.AtsBatteryInfo.Ats5Volts))
    print("AtsChargerVolts                    : " + str(response.AtsBatteryInfo.AtsChargerVolts))

'''
Description: Display K22 get rterm frequencies
Example:
	python atsK22GetRtermFreq.py
'''
def getAtsk22RtermFreq():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_RTEM_FREQ)
    return response

def displayAtsk22RtermFreq():
    response = getAtsk22RtermFreq()
    if response == -1:
        print("Error getting ats k22 rterm freq")
        return -1
    print("AtsRtermValue                 : " + response.AtsRtermFreq.AtsRtermValue)


'''
Description: Display rtc time
Example:
	python atsGetRtcTime.py
'''
def grpcAtsK22RunBattHealthCheck(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_K22_RUN_BATT_HEALTH_CHECK, value1, value2, value3, value4)

'''
Description: Battery charge configuration
Command Name: <ATS_K22_SET_HIBER_STATE>
Example:
	python atsK22SetBattChrgState.py -v off | boost | trickle
Response:
		Success: “OK”
		Fail: Grpc send command error

'''
def grpcK22SetBattChrgState(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_K22_SET_BATT_CHARGING_STATE, value1, value2, value3, value4)

'''
Description: Turn on or off the override state from any grpc client
Command Name: <ATS_K22_SET_CHARGE_OVERRIDE_STATE>
Example:
	python atsK22SetChargeOverrideState.py -v on | off
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsK22SetChargeOverrideState(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_K22_SET_CHARGE_OVERRIDE_STATE, value1, value2, value3, value4)

'''
Description: Turn on/off hibernate state
Command Name: <ATS_K22_SET_HIBER_STATE>
Example:
	python atsK22SetHiberState.py -v off
Response:
		Success: “OK”
		Fail: Grpc send command error

'''
def grpcAtsK22SetHiberState(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_K22_SET_HIBER_STATE, value1, value2, value3, value4)




'''
Description: write to output dis
Command Name: <ATS_K22_SET_OUTPUT_DIS>
	Example:
		python atsK22SetOutputDis.py -v status
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsK22SetOutputDis(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_K22_SET_OUTPUT_DIS, value1, value2, value3, value4)

'''
Description: Set the K22 Power source
'''
def grpcAtsK22SetPwrSource(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_K22_SET_PWR_SOURCE, value1, value2, value3, value4)

'''
Description: Stop K22 wd
'''
def grpcAtsK22StopWd(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_K22_STOP_WD, value1, value2, value3, value4)

'''
Description: trim Dac
'''
def grpcAtsK22TrimDac(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_K22_TRIM_DAC, value1, value2, value3, value4)

"""
Description: Set the battery load
Example:
    python atsK22SetBattLoad.py -v on | off
    AtsCommandResponse: "Success"

    OK
"""
def grpcAtsK22SetBattLoad(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_K22_SET_BATT_LOAD, value1, value2, value3, value4)

"""
Description: get k22 battery info
"""
def getk22BattLoad():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_BATT_LOAD)
    print("Raw output: " + str(response))
    return response


'''
Description: flash led on board
Command Name: <ATS_LED_ALL_ON_OFF_CTRL>
Example:
	python atsLedAllOnOffCtrl.py -v yellow -v1 off
Response:
	Success: “OK”
	Fail: Grpc send command error
'''
def grpcAtsLedAllOnOffCtrl(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_LED_ALL_ON_OFF_CTRL, value1, value2, value3, value4)

'''
Description: Execute lowside driver
Command Name: <ATS_LOWSIDE_DRIVER>
	Example:
		python atsLowSideDriver.py -v1 write -v2 enable -v3 1
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsLowsideDriver(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_LOWSIDE_DRIVER, value1, value2, value3, value4)


'''
Description: Set the amid
Command Name: <ATS_MPI_SET_AMID>
	Example:
		python atsMpiSetCustPartNum.py -v 0001
		Response:
			Success: “OK”
			Fail: Grpc send command error

'''
def grpcAtsMpiSetAmid(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_AMID, value1, value2, value3, value4)

'''
Description: Mpi set asset prot
'''
def grpcAtsMpiSetAssetProt(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_ASSET_PROT, value1, value2, value3, value4)

'''
Description: Mpi set csf footer
Example:
	python atsMpiSetCsfFooter.py
'''
def grpcAtsMpiSetCsfFooter(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_CSF_FOOTER, value1, value2, value3, value4)

'''
Description: Mpi set csf footer
Example:
	python atsMpiSetCsfFooter.py
'''
def grpcAtsMpiSetCsfHdr(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_CSF_HDR, value1, value2, value3, value4)

'''
Description: Mpi set customer part number
Command Name: <ATS_MPI_SET_CUST_PART_NUM>
	Example:
		python atsMpiSetCustPartNum.py -v 00000000
		Response:
			Success: “OK”
			Fail: Grpc send command error
'''
def grpcAtsMpiSetCustPartNum(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_CUST_PART_NUM, value1, value2, value3, value4)

'''
Description: Set Default valus in the mpi
Command Name: <ATS_MPI_SET_DEF_VALUES>
	Example:
		python atsMpiSetDefValues.py -v install -v1 pl243_dv2_mpi_template_ski.bin -v2 165.26.79.1
		Response:
			Success: “OK”
			Fail: Grpc send command error
'''
def grpcAtsMpiSetDefValues(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_DEF_VALUES, value1, value2, value3, value4)

'''
Description: Set ecm ID through mpi
Command Name: <ATS_MPI_SET_DEF_VALUES>
	Example:
		python atsMpiSetDefValues.py -v install -v1 pl243_dv2_mpi_template_ski.bin -v2 165.26.79.1
		Response:
			Success: “OK”
			Fail: Grpc send command error

'''
def grpcAtsMpiSetEcmid(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_ECMID, value1, value2, value3, value4)

'''
Description: Mpi set hardware part number
Command Name: <ATS_MPI_SET_HW_PART_NUM>
	Example:
		python atsMpiSetHwPartNum.py  -v 1234567-89
		Response:
			Success: “OK”
			Fail: Grpc send command error
'''
def grpcAtsMpiSetHwPartNum(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_HW_PART_NUM, value1, value2, value3, value4)

'''
Description: Mpi set imei
'''
def grpcAtsMpiSetImei(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_IMEI, value1, value2, value3, value4)

'''
Description: Set key prot
'''
def grpcAtsMpiSetKeyProt(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_KEY_PROT, value1, value2, value3, value4)

'''
Description: Mpi set lan mac address
Command Name: <ATS_MPI_SET_LAN_MAC_ADDR>
	Example:
		python atsMpiSetLanMacAddr.py -v 12:34:56:78:90:12
		Response:
			Success: “OK”
			Fail: Grpc send command error
'''
def grpcAtsMpiSetLanMacAddr(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_LAN_MAC_ADDR, value1, value2, value3, value4)

'''
Description: Set the manufacturing date
Command Name: <ATS_MPI_SET_MFG_DATE>
	Example:
		python atsMpiSetMfgDate.py -v 2018/10/19
		Response:
			Success: “OK”
			Fail: Grpc send command error
'''
def grpcAtsMpiSetMfgDate(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_MFG_DATE, value1, value2, value3, value4)

'''
Description: Set the platform ID
Command Name: <ATS_MPI_SET_PLATFORM_ID>
Example:
	python atsMpiSetPlatformId.py -v 0001
Response:
	Success: “OK”
	Fail: Grpc send command error
'''
def grpcAtsMpiSetPlatformId(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_PLATFORM_ID, value1, value2, value3, value4)

'''
Description: Set serial number
Command Name: <ATS_MPI_SET_SERIAL_NUM>
Example:
	python atsMpiSetSerialNum.py -v 18062000H002003F
Response:
	Success: “OK”
	Fail: Grpc send command error
'''
def grpcAtsMpiSetSerialNum(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_SERIAL_NUM, value1, value2, value3, value4)

'''
Description: Ping the IP
Command Name: <ATS_PING_IP>
Example:
	python atsPingIp.py -v 165.26.79.1
Response:
		Success: “OK”
		Fail: Grpc send command error

'''
def grpcAtsPingIp(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_PING_IP, value1, value2, value3, value4)

def pingIP():
	pingIPCount = 0
	satIPCount = 0
	satDynamicRes = getAtsSatDynamic()
	if satDynamicRes == -1: return -1
	while satIPCount < 10 and str(satDynamicRes.SatDynamicValue.AtsSatWanIpAddr).strip() == "":
		print("Waiting for Satellite IP...")
		time.sleep(10)
		satDynamicRes = getAtsSatDynamic()
		if satDynamicRes == -1: return -1
		satIPCount += 1

	if satIPCount >= 10: return -1
	else:
		peepIPaddr = str(satDynamicRes.SatDynamicValue.AtsSatWanIpAddr).strip()
		print("Found Satellite IP" + peepIPaddr)
	if grpcAtsSetIridumRssiAverage("1") == -1: return -1
	while pingIPCount < 15:
		print("Retrying to ping satellite IP...")
		if grpcAtsPingIp(peepIPaddr) == -1:
			if grpcAtsSatCsdDown() == -1: return -1
			time.sleep(60)
			if grpcAtsSatCsdUp() == -1: continue # don't ping if csd up fails
		else:
			print("PING SUCCESS")
			return 0
		pingIPCount += 1
	if pingIPCount >= 15: return -1


'''
Description: Reboots the device
'''
def grpcAtsRebootDevice(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_REBOOT_DEVICE, value1, value2, value3, value4)


'''
Description: Run the RS485 test
'''
def grpcAtsRS485Test(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_RS485_TEST, value1, value2, value3, value4)

'''
Description: Displays the rterm frequency
Example: python atsRtermFreq.py
'''
def AtsRtermFreq():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_RTEM_FREQ)
    print("Raw output: " + str(response))
    return response

def displayAtsRtermFreq():
    response = AtsRtermFreq()
    if response == -1:
        print("Error getting ats gps info")
        return -1
    print("AtsRtermValue                       : " + response.AtsRtermFreq.AtsRtermValue)


'''
Description: Send SAT_CSD_DOWN command
Example: python atsSatCsdDown.py
'''
def grpcAtsSatCsdDown(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.SAT_CSD_DOWN, value1, value2, value3, value4)

'''
Description: Send SAT_CSD_UP command
Example: python atsSatCsdUp.py
'''
def grpcAtsSatCsdUp(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.SAT_CSD_UP, value1, value2, value3, value4)

'''
Description: Gets the Sat Dynamic information
'''
def getAtsSatDynamic():
    response = grpcGetModemInfo(RadioManager_pb2.SAT_DYNAMIC)
    print("Raw response: " + str(response))
    return response

def displayAtsSatDynamic():
    response = getAtsSatDynamic()
    if response == -1:
        print("Error getting software info")
        return -1
    print("ConsistentSignalQualityAverage                               : " + str(response.SatDynamicValue.ConsistentSignalQualityAverage))
    print("OperatingMode                                                : " + str(response.SatDynamicValue.OperatingMode))
    print("ModemAvailable                                               : " + str(response.SatDynamicValue.ModemAvailable))
    print("ModemPowerFailure                                            : " + str(response.SatDynamicValue.ModemPowerFailure))
    print("ModemState                                                   : " + str(response.SatDynamicValue.ModemState))
    print("OperatingMode                                                : " + str(response.SatDynamicValue.OperatingMode))
    print("AtsPeerPPPIpAddr                                             : " + str(response.SatDynamicValue.AtsPeerPPPIpAddr))


'''
Description: Send SAT_POWER_DOWN command
Example: python atsSatPowerDown.py
'''
def grpcAtsSatPowerDown(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.SAT_POWER_DOWN, value1, value2, value3, value4)

'''
Description: Send SAT_POWER_UP command
Example: python atsSatPowerUp.py
'''
def grpcAtsSatPowerUp(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.SAT_POWER_UP, value1, value2, value3, value4)

'''
Description: Send SAT_SET_CSD_DIAL_NUMBER command
Command Name: <SAT_SET_CSD_DIAL_NUMBER>
Example:
	python atsSatSetCsdDialNumber.py -v 88160001101
	Response:
		Success: “OK”
		Fail: Grpc send command error

'''
def grpcAtsSatSetCsdDialNumber(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.SAT_SET_CSD_DIAL_NUMBER, value1, value2, value3, value4)

'''
Description: Get sat static info
Example: python atsSatStatic.py
'''
def getAtsSatStatic():
    response = grpcGetModemInfo(RadioManager_pb2.SAT_STATIC)
    print("Raw response: " + str(response))
    return response

def displayAtsSatStatic():
    response = getAtsSatStatic()
    if response == -1:
        print("Error getting software info")
        return -1
    print("SimCardNumber                                : " + str(response.SatStaticValue.SimCardNumber))
    print("InternationalMobileEquipmentID               : " + str(response.SatStaticValue.InternationalMobileEquipmentID))
    print("InternationalMobileSubscriberID              : " + str(response.SatStaticValue.InternationalMobileSubscriberID))
    print("MobileEquipmentIdentifier                    : " + response.SatStaticValue.MobileEquipmentIdentifier)
    print("CircuitSwitchedDialNumber                    : " + response.SatStaticValue.CircuitSwitchedDialNumber)
    print("ModemFirmwareVersion                         : " + response.SatStaticValue.ModemFirmwareVersion)


'''
Main SBD command
'''
def grpcAtsSBDCommand(value1 = None, value2 = None, value3 = None, value4 = None):
	try:
		sbd_stub = RadioManager_pb2_grpc.ShortBurstDataManagerStub(getClientChannel(targetIP))
		request = RadioManager_pb2.ShortBurstDataMessage(MessageContent=bytes(value1.encode()))
		response = sbd_stub.SendShortBurstDataMessage(request)
		print(str(response)[28:36])
		print("OK")
		return str(response)[28:36]
	except Exception as e:
		printErr(e, 'f')
		return -1

'''
Description: sdb ring alert disable
Example:
	python atsSbdRingAlertDisable.py
'''
def grpcAtsShortBurstDataRingAlertDisable(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.SHORT_BURST_DATA_RING_ALERT_DISABLE, value1, value2, value3, value4)

'''
Description: sdb ring alert enable
Example:
	python atsSbdRingAlertEnable.py
'''
def grpcAtsShortBurstDataRingAlertEnable(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.SHORT_BURST_DATA_RING_ALERT_ENABLE, value1, value2, value3, value4)

'''
Description: Send AT command
Command Name: <ATS_SEND_AT_CMD>
Example:
	python atsSendAtCmd.py -v AT+CREG?
Response:
		Success: “OK”
		Fail: Grpc send command error

'''
def grpcAtsSendAtCmd(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_SEND_AT_CMD, value1, value2, value3, value4)

'''
Description: Set cat sw part number
Command Name: <SET_CATERPILLAR_SOFTWARE_PART_NUMBER>
Example:
	python atsSetCatSwPartNum.py -v 123498A23532561
Response:
	Success: “OK”
	Fail: Grpc send command error
'''
def grpcAtsSetCatSwPartNum(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.SET_CATERPILLAR_SOFTWARE_PART_NUMBER, value1, value2, value3, value4)

'''
Description: Set Cell APN
Command Name: <CELL_SET_ACCESS_POINT_NAME>
Example:
	python atsSetCellApn.py -v BROADBAND
	Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsSetCellApn(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.CELL_SET_ACCESS_POINT_NAME, value1, value2, value3, value4)

'''
Description: Set apn password
Command Name: <CELL_SET_APN_PASSWORD>
Example:
	python atsSetCellApnPassword.py -v SCORPASS
Response:
	Success: “OK”
	Fail: Grpc send command error
'''
def grpcAtsSetCellApnPassword(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.CELL_SET_APN_PASSWORD, value1, value2, value3, value4)

'''
Description: Set apn username
Command Name: <CELL_SET_APN_USERNAME>
Example:
	python atsSetCellApnUsername.py -v SCORNAME
Response:
	Success: “OK”
	Fail: Grpc send command error
'''
def grpcAtsSetCellApnUsername(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.CELL_SET_APN_USERNAME, value1, value2, value3, value4)

'''
Description: Set the iridium rssi average
Command Name: <ATS_SET_IRIDIUM_RSSI_AVERAGE>
Example:
	python atsSetIridiumRssiAverage.py -v 4
Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsSetIridumRssiAverage(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_SET_IRIDIUM_RSSI_AVERAGE, value1, value2, value3, value4)

'''
Description: set the rtc alarm
'''
def grpcAtsSetRtcAlarm(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_SET_RTC_ALARM, value1, value2, value3, value4)

'''
Description: Set rtc time
Command Name: <ATS_SET_RTC_TIME>
Example:
	python atsSetRtcTime.py -v """2018-08-03 10:20:40"""
Response:
		Success: “OK”
		Fail: Grpc send command error

'''
def grpcAtsSetRtcTime(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_SET_RTC_TIME, value1, value2, value3, value4)

'''
Description: Run the ski staging command
'''
def grpcAtsSkiStaging(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_SKI_STAGING, value1, value2, value3, value4)

'''
Description: Displays the software info
Example:
	python atsSwInfo.py
'''
def getAtsSwInfo():
    response = grpcGetModemInfo(RadioManager_pb2.SOFTWARE_INFO)
    print("Raw response: " + str(response))
    return response

def displayAtsSwInfo():
    response = getAtsSwInfo()
    if response == -1:
        print("Error getting software info")
        return -1
    print("SupplierSoftwarePartNumber                   : " + str(response.SoftwareInfoValue.SupplierSoftwarePartNumber))
    print("CaterpillarSoftwarePartNumber                : " + str(response.SoftwareInfoValue.CaterpillarSoftwarePartNumber))
    print("ATSCellModemFwVersion                        : " + response.SoftwareInfoValue.AtsCellModemFwVersion)
    print("ATSIridiumModemFwVersion                     : " + response.SoftwareInfoValue.AtsIridiumModemFwVersion)
    print("ATSK22FwVersion                              : " + response.SoftwareInfoValue.AtsK22FwVersion)



'''
Description: Enable/disble THERMAL SHUTDOWN
Example:
	python atsTempshutdownState.py -v on
'''
def grpcAtsTempShutdownState(value1 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_MDM_THERMAL_SHUTDOWN_CTRL, value1)

'''
Description: Display internal battery fault info
Example:
	python atsK22GetIntbattVolts.py
'''
def getk22IntbattVolts():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_INTBATT_VOLTS)
    print("Raw output: " + str(response))
    return response

def displayK22IntbattVolts():
    response = getk22IntbattVolts()
    if response == -1:
        print("Error getting ats k22 batt info")
        return -1
    print("AtsIntBattVolts                   : " + str(response.AtsIntBattVolts.AtsIntBattVolts))


'''
Description: Display battery fault info
Example:
	python atsK22GetBattFault.py
'''
def getk22BattFault():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_BATT_FAULT)
    print("Raw output: " + str(response))
    return response

def displayK22BattFault():
    response = getk22BattFault()
    if response == -1:
        print("Error getting ats k22 batt info")
        return -1
    print("AtsBatterHealth                   : " + str(response.AtsBatteryFaultInfo.AtsBatterHealth))

'''
Description: Display internal battery temp info
Example:
	python atsK22GetIntbattTemp.py
'''
def getk22IntbattTemp():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_INTBATT_TEMP)
    print("Raw output: " + str(response))
    return response

def displayK22IntbattTemp():
    response = getk22IntbattTemp()
    if response == -1:
        print("Error getting ats k22 internal battery temp info")
        return -1
    print("AtsIntBattTemp                   : " + str(response.AtsIntBattTemp.AtsIntBattTemp))

'''
Description: Displays the country profile
Example:
    Raw output: AtsGetCountryProfile {
    AtsCountryCfg: COUNTRY_ROW
    AtsCertCodeCfg: CELLULAR_CERTIFICATION_2G
    AtsCertCodeCfg: CELLULAR_CERTIFICATION_3G
    AtsCertCodeCfg: CELLULAR_CERTIFICATION_4G
    AtsLteFactoryCfg: "1,2,3,4,5,7,8,12,18,19,20,28"
    AtsCountryEnv: COUNTRY_ROW
    AtsCertCodeEnv: CELLULAR_CERTIFICATION_2G
    AtsCertCodeEnv: CELLULAR_CERTIFICATION_3G
    AtsCertCodeEnv: CELLULAR_CERTIFICATION_4G
    AtsLteFactoryEnv: "1,2,3,4,5,7,8,12,18,19,20,28"
    AtsLteBandsActive: "1,2,3,4,5,7,8,12,18,19,20,28"
    }

    AtsLteFactoryCfg                 : 1,2,3,4,5,7,8,12,18,19,20,28
    AtsCertCodeCfg                 : 1
    AtsCertCodeCfg                 : 2
    AtsCertCodeCfg                 : 3
    AtsCertCodeCfg                 : [1, 2, 3]
    AtsLteFactoryCfg                 : 1,2,3,4,5,7,8,12,18,19,20,28
    AtsCountryEnv                 : 1
    AtsCertCodeEnv                 : 1
    AtsCertCodeEnv                 : 2
    AtsCertCodeEnv                 : 3
    AtsCertCodeEnv                 : [1, 2, 3]
    AtsLteFactoryEnv                 : 1,2,3,4,5,7,8,12,18,19,20,28
    AtsLteBandsActive                 : 1,2,3,4,5,7,8,12,18,19,20,28
'''
def getAtsCountryProfile():
	response = grpcGetModemInfo(RadioManager_pb2.ATS_GET_COUNTRY_PROFILE)
	print("Raw output: " + str(response))
	return response

'''
Description: Set Cell APN
Command Name: <ATS_SET_COUNTRY_PROFILE>
Example:
	python atsSetCountryProfile.py -v
	Response:
		Success: “OK”
		Fail: Grpc send command error
'''
def grpcAtsSetCountryProfile(value1 = None, value2 = None, value3 = None, value4 = None):
	return grpcAtsSendCommand(RadioManager_pb2.ATS_SET_COUNTRY_PROFILE, value1, value2, value3, value4)



def grpcAtsPurgeRmaCfg(value1 = None, value2 = None, value3 = None, value4 = None):
    """
    Description: Purge RMA Cfg
    Command Name: <PURGE_RMA_CFG>
    Example:
        python atsPurgeRmaCfg.py
    Response:
            Success 
            OK
            Fail: Grpc send command error

    """
    return grpcAtsSendCommand(RadioManager_pb2.PURGE_RMA_CFG, value1, value2, value3, value4)

#######################################################################################################
import io, sys, subprocess, os, base64, time
derBase64 = ""
commonName = ""
'''
Convert DER cert into base64
'''
def convertDerToBase64():
	try:
		global derBase64
		derCert = open("uik_public.der", 'rb')
		derBase64 = base64.b64encode(derCert.read())
		derCert.close()
	except Exception as e:
		derBase64 = -1
		return -1

def getDerBase64():
	return derBase64

'''
Convert PEM to DER cert
'''
def convertPemToDer():
	try:
		result = subprocess.Popen('openssl x509 -outform der -in uik_public.cer -out uik_public.der', shell=True, stdout=subprocess.PIPE)
		result.wait()
		return 0
	except Exception as e:
		return -1

'''
Get the Common Name (CN) from PEM cert
'''
def getCommonName():
	try:
		global commonName
		result = subprocess.Popen("openssl x509 -noout -subject -in uik_public.cer", shell=True, stdout=subprocess.PIPE)
		result.wait()
		raw = str(result.stdout.read())
		cn = raw.split('CN')[1]
		cn = cn[:-5].replace(" ", "").replace("=", "")
		commonName = cn
		f = open("uik_public_cn.txt", 'wb') #save CN to file
		f.write(str.encode(cn))
		f.close()
		return commonName
	except Exception as e:
		return -1

'''
Get PEM cert
'''
def getPemCert():
	try:
		pemCert = io.BytesIO()

		#Get .PEM and convert. PEM should ALWAYS be available
		print("\r\nDownloading uik_public.cer from FTP server\r\n")
		pemCert = ftpGetCert("uik_public.cer")
		return pemCert
	except Exception as e:
		return -1

def ftpGetCert(filename):
	try:
		import atsExtractIP
		with FTP(host=atsExtractIP.extractIPAddr(), passwd = 'anonymous') as ftp_session:
			print("Ftp into device...")
			print(ftp_session.login())

			if filename in ftp_session.nlst():
				f = open(filename, 'wb')
				ftp_session.retrbinary('RETR ' + filename, f.write)
				f.close()
			else:
				print("Cert file not found!")
				return -1

			print("Closing ftp connection")
			ftp_session.close()
	except Exception as e:
		print("Unable to connect to T6RX" + str(e))
		return -1

	return 0


def getScorpionCerts():
	try:
		os.remove("uik_public.cer")
		os.remove("uik_public.der")
		os.remove("uik_public_cn.txt")
		os.remove("uik_public.der.b64")
	except:
		pass

	if getPemCert() == -1: return -1
	if getCommonName() == -1: return -1
	if convertPemToDer() == -1: return -1
	if convertDerToBase64() == -1: return -1
#######################################################################################################


def grpcGetSyslogStream():
    try:
        global targetIP
        if targetIP == '': targetIP = getTargetIP()
        channel = getClientChannel(targetIP)
        modemMgr_stub = RadioManager_pb2_grpc.LogManagerStub(channel)
        request = RadioManager_pb2.ATSLog()
        responses = modemMgr_stub.GetSyslogStream(request)
        for response in responses:
            print(str(response.AtsLogContent))
    except Exception as e:
        print(str(e))
        return -1
    except KeyboardInterrupt:
        print("Received ctrl-c")
        channel.close()

def getAtsCalampSkiCheck():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_SKI_CHECK)
    return response

linecountk20=0
revlinecountk20=0
matchedlinek20=0
foundk20=False
versionk20=""
#create a Temp folder if not present
directory="C:\Temp"
'''
This function downloads the CGI from target.
Surfs through the CGI file finding the last occurance of "Prepping Kinetis firmware (app_t6rxk2x_KINETIS"
Performs parsing and gets the version of K20 '''
def getK20Version():
    absPathOfFile = os.path.dirname(os.path.realpath(__file__))
    currentWd = os.getcwd()
    os.chdir(absPathOfFile)
    
    import requests
    global linecountk20
    global revlinecountk20
    global matchedlinek20
    global versionk20
    global foundk20
    # Delete the file “display_reprogramming_log.cgi” if exists
    exists= os.path.isfile("../temp/display_reprogramming_log.cgi")

    if exists:
        os.remove("../temp/display_reprogramming_log.cgi")
        print("Old file removed")
        time.sleep(2)

    ''' The following downloads the CGI file by calling a web service. Equivalent to a wget'''
    url="http://172.31.234.158/cgi-bin/display_reprogramming_log.cgi"
    response =requests.get(url, stream= True)

    with open(directory+"/display_reprogramming_log.cgi",'wb') as f:
       f.write(response.content)
       print("Download complete")
       f.close()  ## Somu
       time.sleep(2)

    #to find the total number of lines in the file
    ## Somu
    linecountk20=0
    with open(directory+"/display_reprogramming_log.cgi",'r') as f:
        for line in list(f):
            linecountk20+=1
            #print(linecount)
        f.close() ## Somu
        print("Total Lines in prog file: "+str(linecountk20))
        time.sleep(2)

    #start looking in reverse order so it is easier to locate the search string
    foundk20 = False ## Somu
    VerSplit=""
    revlinecountk20=0
    with open(directory+"/display_reprogramming_log.cgi",'r') as f:
        for line in reversed(list(f)):
            #print("Version: "+line)				#cquinones-09012021
            revlinecountk20+=1
            if "(app_t6rxk2x_KINETIS" in line:		#cquinones-09012021
                foundk20 = True
                #print(line)
                versionk20=""
                VerSplit=line.split("_")[3]
                for i in VerSplit:
                    if(i!=')'):
                        versionk20 += str(i)
                    else:
                        #print ("Matched (")
                        break
                print("Version: "+versionk20)
                print("Reversed Linecount="+str(revlinecountk20))
                break
        f.close() ## Somu
        time.sleep(2)

## Somu
    matchedlinek20=0
    if(foundk20 == False):
        print("K20 version found is False...")
        return "EMPTY"
    else:
        matchedlinek20 = linecountk20-revlinecountk20
        linecountk20 = 0
        print("K20 version found = True..., matchedline="+str(matchedlinek20))
        os.chdir(currentWd)
        return versionk20
    os.chdir(currentWd)
