import os, sys, argparse

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################

from atsCalampgrpc import *
COMMAND = RadioManager_pb2.ATS_GNSS_CONFIG
from atsModemCommand import grpcAtsSendCommand

'''
Description: GNSS config command
Example:
		python atsConfigGNSS.py -v External
		python atsConfigGNSS.py -v Internal

	Response:
			Sucess:
				OK
			Fail: Grpc send command error
'''
def grpcAtsConfigGNSS(value1 = None, value2 = None, value3 = None, value4 = None):
	grpcAtsSendCommand(COMMAND, value1, value2, value3, value4)


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Description:\nAn interface to send grpc ATS GNSS config command.\nAuthor:\t\tJeugin\nDate Created:\t5/27/2022', formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
	parser.add_argument('-v1', dest='value1', default = '', type=str, help='v1 - AtsCommandValue1')
	parser.add_argument('-v2', dest='value2', default = '', type=str, help='v2 - AtsCommandValue2')
	parser.add_argument('-v3', dest='value3', default = '', type=str, help='v3 - AtsCommandValue3')
	args = parser.parse_args()
	grpcAtsConfigGNSS(args.value, args.value1, args.value2, args.value3)