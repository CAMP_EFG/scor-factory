import os, sys, configparser, logging, time

absPathOfFile = os.path.dirname(os.path.realpath(__file__))
currentWd = os.getcwd()
os.chdir(absPathOfFile) # change absolute path of atsMaster.py to import atsCalampgrpc


##########################################################################################
p = os.path.abspath('../lib/')
if p not in sys.path:
	sys.path.append(p)

from atsCalampCommonPy35 import atsSetup, getTargetIP
atsSetup()
targetIP = ''
##########################################################################################

import importlib
import atsCalampgrpc
importlib.reload(atsCalampgrpc) 
from atsCalampgrpc import *

os.chdir(currentWd) # then change directory back to original cwd

import RadioManager_pb2
import RadioManager_pb2_grpc

Logger = logging.getLogger('ATE2_Logger.atsModeminfo')

'''
Main script to get modem info
'''
def grpcGetModemInfo(msgId, getIP = False):
    try:
        global targetIP
        if getIP == True:
            Logger.debug(f'targetIP before PC = {targetIP}')
            targetIP = getTargetIP()
            Logger.debug(f'targetIP after PC = {targetIP}')
            return targetIP
        if targetIP == '': targetIP = getTargetIP()
        print(f'targetIP modeminfo after = {targetIP}')
        channel = getClientChannel(targetIP)
        modemMgr_stub = RadioManager_pb2_grpc.ModemManagerStub(channel)
        request = RadioManager_pb2.ModemManagerFilterMessage(ModemManagerFilter = int(msgId))
        response = modemMgr_stub.GetInfo(request)
        Logger.debug(f'atsModeminfo response = {response}')
        channel.close()
        return response
    except Exception as e:
        printErr(e, 'q')
        return -1
