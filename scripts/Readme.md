# ATS gRPC client-server example
Welcome to the ATS insecure folder
## Prerequisites

Make sure you have python >= 3.x installed in your OS

## Instructions

atsPopulate.py is to run pre-defined command for the queries and commands all at once
Run python3 atsPopulate.py

atsGrpcCommandIterate.py is running single commands with the interface by giving you options
Run python3 atsGrpcCommandIterate.py

All other python files are individual commands to run 
EXAMPLE: 
Run python3 atsExecuteScript.py -v "S01mmgr" -v1 "stop"
    python3 atsExecuteScript.py -h 

You may also run these commands individually:
EXAMPLE:
Run python3 atsGetAccelValue.py

## Notes

*  The current atsGrpcServer.py is just a template for reference purposes with random data inserted.
