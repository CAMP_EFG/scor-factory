'''
Author: Joshua Liew
Date: 12/11/2018
run thd and extract mac
flash os
extract cat K22 version (to be written to file later)
flash app
get FlashTargetInfo.xml and extract the following:
    - mac addr
    - ecm part number
    - ecm serial number
    - os part number
    - app part number
    - app change number
    - k22 version (from atsConfigK22Version.py)
write to file with format SN_DATE_TIME.txt
'''
import os, subprocess, sys, time, pdb
from ftplib import FTP
from time import gmtime, strftime
from xml.dom.minidom import parse
import xml.dom.minidom
mac = ''
fileName = 'FlashTargetInformation.xml'
ECM_pn = ''
ECM_sn = ''
os_pn = ''
app_pn = ''
app_cn = ''
free_txt = ''
catK22Ver = ''
def extractFromXML(xmlFile):
    global mac, ECM_pn, ECM_sn, os_pn, app_pn, app_cn, catK22Ver
    # Open XML document using minidom parser
    DOMTree = xml.dom.minidom.parse(xmlFile)
    collection = DOMTree.documentElement

    flashTargetInformationMessage = collection.getElementsByTagName("FlashTargetInformationMessage")
    print("MAC address: " + str(mac))
    for infoMsg in flashTargetInformationMessage:
        ECMPartNumber = infoMsg.getElementsByTagName('ECMPartNumber')[0]
        ECM_pn = ECMPartNumber.childNodes[0].data
        print("ECMPartNumber: %s" % ECMPartNumber.childNodes[0].data)

        ECMSerialNumber = infoMsg.getElementsByTagName('ECMSerialNumber')[0]
        ECM_sn = ECMSerialNumber.childNodes[0].data
        print("ECMSerialNumber: %s" % ECMSerialNumber.childNodes[0].data)

        operatingSystem = collection.getElementsByTagName("OperatingSystem")
        for os_info in operatingSystem:
            partNumber = os_info.getElementsByTagName('PartNumber')[0]
            os_pn = partNumber.childNodes[0].data
            print("OS Part Number: %s" % partNumber.childNodes[0].data)

        application = collection.getElementsByTagName("Application")
        for app_info in application:
            partNumber = app_info.getElementsByTagName('PartNumber')[0]
            app_pn = partNumber.childNodes[0].data
            print("App Part Number: %s" % partNumber.childNodes[0].data)
            changeNumber = app_info.getElementsByTagName('ChangeNumber')[0]
            app_cn = changeNumber.childNodes[0].data
            print("App Change Number: %s" % changeNumber.childNodes[0].data)


    #get time and date
    dateAndTime = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
    SN_Date_n_Time = ""
    SN_Date_n_Time += ECM_sn + "_" + dateAndTime
    # write to file
    f = open(SN_Date_n_Time + '.txt', 'w')
    f.write("MAC address: " + str(mac) + "\n")
    f.write("ECM Part Number: " + ECM_pn + "\n")
    f.write("ECM SerialNumber: " + ECM_sn + "\n")
    f.write("OS Part Number: " + os_pn + "\n")
    f.write("App Part Number: " + app_pn + "\n")
    f.write("App Change Number: " + app_cn + "\n")
    f.write("Cat K22 Version: " + catK22Ver + "\n")
    print("Cat K22 Version: " + catK22Ver + "\n")
    if str(mac) == '' or ECM_pn == '' or ECM_sn == '' or os_pn == '' or app_pn == '' or app_cn == '':
        raise Exception("KEY VALUES EMPTY")
    global free_txt
    free_txt = str(mac) + "," + ECM_pn + "," + ECM_sn + "," + os_pn + "," + app_pn + "," + app_cn
    f.write(free_txt)
    f.close()

def ftpGetXML():
    import extractIP
    with FTP(host=extractIP.extractIPAddr(), passwd = 'anonymous') as ftp_session:
        print("Ftp into device...")
        print(ftp_session.login())
        print(ftp_session.cwd('/ipflash'))
        filename = 'FlashTargetInformation.xml'
        f = open(filename, 'wb')
        print(ftp_session.retrbinary('RETR ' + filename, f.write))
        f.close()
        print("Closing ftp connection")
        ftp_session.close()

def extractMacAddr():
    global mac
    print("Extracting mac address from thd.py")
    thdCommand = 'python thd.py -t 1'
    result = subprocess.Popen(thdCommand, shell=True, stdout=subprocess.PIPE)
    mac = ''
    raw = str(result.stdout.read())
    formatted = raw.split('\\r\\n')
    try:
        count = 0
        while count < 4:
            if count == 0 and formatted[0][7:19] != 'AAAAAAAAAAAA': break
            if count == 1 and formatted[1][5:17] != 'AAAAAAAAAAAA': break
            if count == 2 and formatted[2][5:17] != 'AAAAAAAAAAAA': break
            if count == 3 and formatted[3][5:17] != 'AAAAAAAAAAAA': break
            count += 1
        if count == 0: mac = formatted[count][7:19]
        else: mac = formatted[count][5:17]
        if mac == 'AAAAAAAAAAAA' or mac == '':
            print(mac)
            print("No mac address found")
            return -1
        else: print("MAC addr found: " + mac)
    except: pass

    return 0

def flashOS(fl2):
    print("Flashing OS...")
    ftpushCommand_1 = 'python ./ftppush.py --fl2 ' + fl2 + ' -z 7z -d ' +  mac + ' --flash_select os --skip_pn_verify '
    print("Flashing OS images from FL2 package...")
    if os.system(ftpushCommand_1) != 0: return -1
    print("==================================================")
    return 0

def extractK22Ver():
    print("Extracting Cat K22 Version...")
    global catK22Ver
    sys.path.append('../scripts')
    import atsConfigK22Version
    catK22Ver = atsConfigK22Version.getFlashingKinetisStatus()
    if catK22Ver == -1: return -1
    return 0

def flashApp(fl2):
    print("flashing App...")
    ftpushCommand_2 = 'python ./ftppush.py --fl2 ' + fl2 + ' -z 7z -d ' +  mac + ' --flash_select app --skip_pn_verify '
    print("Flashing App images from FL2 package...")
    if os.system(ftpushCommand_2) != 0: return -1
    print("==================================================")
    return 0

def wirelessDisableCheck():
    print("Checking wireless disable...")
    sys.path.append('../scripts')
    import atsConfigReadWDStatus
    return atsConfigReadWDStatus.readConfigWDStatus()

def setHibernateOn():
    print("Setting hibernate mode on...")
    sys.path.append('../scripts')
    import atsConfigSetHibernateMode
    return atsConfigSetHibernateMode.readConfigHibernateState()

def run(fl2Path):
    absPathOfFile = os.path.dirname(os.path.realpath(__file__))
    currentWd = os.getcwd()
    os.chdir(absPathOfFile) # change absolute path of atsMaster.py to import atsCalampgrpc

    count = 0
    while extractMacAddr() == -1 and count < 3:
        print("Retrying extractMac()")
        count += 1
    if mac != 'AAAAAAAAAAAA' and mac != '':
        print("Device MAC, starting FTP...")
        if flashOS(fl2Path) != 0: raise Exception("flashOS not working")
        if extractK22Ver() != 0: raise Exception("extractK22Version not working")
        if flashApp(fl2Path) != 0: raise Exception("flashApp not working")
        if wirelessDisableCheck() != 0: raise Exception("wirelessDisableCheck not working")
    else:
        print("Default MAC, skipping FTP...")

    print("Getting XML file...")
    ftpGetXML()
    print("Extracting mac")
    extractFromXML(fileName)

    if setHibernateOn() != 0: raise Exception("Set hibernate on not working")

    os.chdir(currentWd) # then change directory back to original cwd

