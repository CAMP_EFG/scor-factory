'''
Description: Ats grpc client interface that sends modem commands and query individually
Example:
    - 1st asks user to choose between query and command
    - then ask for command number
    - return: grpc response
Author: Joshua Liew
Date: 7/26/2018

Updates:
11/14/2018
    now supports ssl and nonSsl with nonSsl as default
'''
import os, sys, argparse

####################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
####################################

import atsCalampgrpc as grpcLib
import RadioManager_pb2 as pb2


def printInfo():
    idx = 0
    while idx < 100:
        try:
            print(str(idx) + " - " + pb2.MODEM_MESSAGE.Name(idx))
        except Exception:
            pass
        idx += 1

def printCommand():
    idx = 0
    while idx < 100:
        try:
            print(str(idx) + " - " + pb2.MODEM_COMMAND.Name(idx))
        except Exception:
            pass
        idx += 1

if __name__ == '__main__':
    grpcLib.verifyPyVer()
    import configparser

    atsRpc = grpcLib.atsGrpcLocal()

    try:
        idx = 0
        mode = input("MODE? INFO - 1 or COMMAND - 2 (TYPE 1 or 2) ")
        if int(mode) == 1:
            print("INFO TYPE?")
            printInfo()
            query = input()
            atsRpc.atsGrpcGetModeminfo(query)

        elif int(mode) == 2:
            print("COMMAND TYPE?")
            printCommand()
            command = input()
            print("ARGUMENTS? Press [Enter] if no arguments\n space seperated argument (e.g yellow on)")
            argument1 = input("argument 1: ")
            argument2 = input("argument 2: ")
            argument3 = input("argument 3: ")
            argument4 = input("argument 4: ")
            atsRpc.atsGrpcSendCommand(command, value1 = argument1, value2 = argument2, value3 = argument3, value4 = argument4)

    except Exception as e:
        print(e)