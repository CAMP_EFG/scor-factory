import os, sys, argparse, pdb

####################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
####################################

from atsCalampgrpc import *

'''
Main SBD command
Example:
	python atsSBDCommand.py -v hello
	Searching for IP target...
	Found: 165.26.78.3
	MOMSN=18
	OK
'''
def grpcAtsSBDCommand(value1 = None, value2 = None, value3 = None, value4 = None):
	try:
		sbd_stub = RadioManager_pb2_grpc.ShortBurstDataManagerStub(getClientChannel())
		request = RadioManager_pb2.ShortBurstDataMessage(MessageContent=bytes(value1.encode()))
		response = sbd_stub.SendShortBurstDataMessage(request)
		print(str(response)[28:36])
		print("OK")
		return 0
	except Exception as e:
		printErr(e, 'f')
		return -1

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Description:\nAn interface to send grpc ATS commands individually.\nAuthor:\t\tJoshua Liew\nDate Created:\t7/24/2018', formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
	parser.add_argument('-v1', dest='value1', default = '', type=str, help='v1 - AtsCommandValue1')
	parser.add_argument('-v2', dest='value2', default = '', type=str, help='v2 - AtsCommandValue2')
	parser.add_argument('-v3', dest='value3', default = '', type=str, help='v3 - AtsCommandValue3')
	args = parser.parse_args()
	grpcAtsSBDCommand(args.value, args.value1, args.value2, args.value3)