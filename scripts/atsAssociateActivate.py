'''
Author: Joshua Liew
Date Modified: 1/7/2019
Description: Make assocatiate and activate calls
'''

import atsCellStatic, requests, pdb, webbrowser, base64, urllib
cellStatic = atsCellStatic.getAtsCellStatic()

def getICCID():
    return str(cellStatic.CellStaticValue.SimCardNumber)

def getIMEI():
    return str(cellStatic.CellStaticValue.InternationalMobileEquipmentID)

def associate(iccid, imei):
    #url = "http://ptcweb/Support/WebServices/cat_iridium_override.php?ICCID=8988169555000766996&IMEI=300125060891740&Method=association"
    url = "http://ptcweb/Support/WebServices/cat_iridium_override.php?ICCID=" + iccid + "&IMEI=" + imei + "&Method=association"

    with urllib.request.urlopen(url) as response:
        html = response.read()

    #TODO: do future steps from html response
    return 0

def activate(imei):
    url = "http://ptcweb/Support/WebServices/cat_iridium_override.php?IMEI=" + imei + "&Method=activation"

    with urllib.request.urlopen(url) as response:
        html = response.read()

    #TODO: do future steps from html response
    return 0

if __name__ == "__main__":
    #TODO: catch exception for each function below
    iccid = getICCID()
    imei = getIMEI()

    associate(iccid, imei)
    activate(imei)