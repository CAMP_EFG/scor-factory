from atsModeminfo import *
from atsGetGpsInfo import getAtsGpsInfo
COMMAND = RadioManager_pb2.ATS_GET_GPS_SAT_INFO

'''
Description: Displays the gps sat info
Example:
	python atsGetGpsSatInfo.py
'''
def getAtsGpsSatInfo():
    response = grpcGetModemInfo(COMMAND)
    print("Raw output: " + str(response))
    return response

def displayAtsGpsSatInfo():
    response = getAtsGpsSatInfo()
    gpsInfo = getAtsGpsInfo()

    if response == -1 or gpsInfo == -1:
        print("Error getting ats gps sat or gps info")
        return -1

    output = ""
    output += str(gpsInfo.AtsGpsInfo.AtsGpsFix) + ":"
    count = 0
    for satInfo in response.AtsGpsSatInfo.AtsSatvsCnr:
        count += 1
        output += str(satInfo.AtsSatNumber) + "," + str(satInfo.AtsSatCnr) + ":"
    print(output)

if __name__ == "__main__":
    displayAtsGpsSatInfo()