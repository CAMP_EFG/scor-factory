import os, sys, argparse

####################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
####################################

from atsCalampgrpc import *
COMMAND = RadioManager_pb2.ATS_SKI_STAGING
from atsModemCommand import grpcAtsSendCommand

'''
Description: Run the ski staging command
'''
def grpcAtsSkiStaging(value1 = None, value2 = None, value3 = None, value4 = None):
	grpcAtsSendCommand(COMMAND, value1, value2, value3, value4)

if __name__ == "__main__":
	grpcAtsSkiStaging()