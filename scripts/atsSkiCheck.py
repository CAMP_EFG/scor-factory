'''
Author: Joshua Liew
Date Modified: 1/30/2019
Description: Runs cat's ski check, sets ssl option to yes if true
'''
import os, sys, configparser
from atsCalampgrpc import getTargetIP

def atsSkiCheck():
    cfg = configparser.ConfigParser()
    ip = getTargetIP()
    if os.system('python ../tools/skicheck.py -d ' + ip) == 0:
        cfgFile = open('../config/atsGrpcconfig.cfg', 'w')
        cfg.read('../config/atsGrpcconfig.cfg')
        cfg.set('SSL', 'option', 'yes')
        cfg.write(cfgFile)
        cfgFile.close()
    else: return -1

if __name__ == "__main__":
    if atsSkiCheck() == -1: print("Ski check fail")
