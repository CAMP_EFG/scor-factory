'''
Author: Joshua Liew
Date Modified: 12/21/2018
Description: Get sysLog through tftp using paramiko library
'''
import os, sys, argparse
from time import gmtime, strftime

####################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
####################################

import sys, paramiko


password = 'p30r1a'
username = "root"
port = 22
key = '../ssl/paramikoKey'

def tftpSysLog():
    try:
        atsSetup()
        from atsCalampgrpc import getTargetIP
        hostname = getTargetIP()
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.WarningPolicy)

        client.connect(hostname, port=port, username=username, password=password, key_filename = key)

        import configparser
        cfg = configparser.ConfigParser()
        cfg.read('../config/atsGrpcconfig.cfg')
        pths = cfg['PATHS']
        path = pths.get('sysLogPath')

        #adds date and time
        #TODO fileName as an option, go to console
        dateAndTime = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
        stdin, stdout, stderr = client.exec_command('cp /var/log/messages ' + path + dateAndTime + '_sysLog')
        stdin, stdout, stderr = client.exec_command('tftp -p -l' + path + dateAndTime + '_sysLog 165.26.79.1')

    except Exception as e:
        print("Received error: " + str(e))
    finally:
        client.close()

if __name__ == "__main__":
    tftpSysLog()