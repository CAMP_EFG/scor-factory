import os, sys, argparse

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################

from atsCalampgrpc import *
COMMAND = RadioManager_pb2.ATS_BURN_OTP_FUSES
from atsModemCommand import grpcAtsSendCommand

'''
Run the script to burn OTP fuses by downloading app image or verity image
Command Name: <ATS_BRUN_OTP_FUSES>
	Example:
		python atsBurnOtpFuses.py -v install -v1 otp_script.sh -v2 165.26.79.1
Response:
		Success: “OK”
		Fail: Grpc send command error

'''
def grpcAtsBurnOtpFuses(value1 = None, value2 = None, value3 = None, value4 = None):
	grpcAtsSendCommand(COMMAND, value1, value2, value3, value4)


if __name__ == "__main__":
	grpcAtsBurnOtpFuses()