from sys import path
import platform
from os.path import abspath
from ftplib import FTP
from paramiko_expect import SSHClientInteraction
from io import StringIO
from re import search
from scp import SCPClient
import time
import re
import configparser
import sys
import subprocess
import paramiko
import argparse

p = abspath('../lib/')
if p not in path:
    path.append(p)

from atsCalampCommonPy35 import getTargetIP
CERT_FILE = 'uik_public.cer'

class DeviceIPType():
    PL641_IP_BLT = '192.168.1.101'
    PL641_IP_ULT = '192.168.1.40'

class UpgradeFile():
    FW_UTIL_PL243_FILENAME = 'glinswup_v_1_0_5_scorpion'
    FW_UTIL_PL641_FILENAME = 'glinswup_v_1_0_5_PL641'
    PROD_FW_FILENAME = 'pls83-w_rev01.202_arn01.000.04_sign02prod.usf'

class SSHInfo():
    PL243_SKI_SSH_KEY = '../ssl/t6rx-id_rsa'
    PL243_UNSKI_SSH_KEY = '../ssl/id_scorp'
    PL641_SKI_SSH_KEY = '../ssl/t5r2devmode-id_rsa'
    PW = 'p30r1a'
class CellModemFwUpgrade():
    """
    Please refer to SCOR-1060 for more details
    """
    FW_VERSION_MAJOR = '01.202'
    FW_VERSION_MINOR = '01.000.04'

    def __init__(self, fw_file = UpgradeFile.PROD_FW_FILENAME, v_major = FW_VERSION_MAJOR, v_minor = FW_VERSION_MINOR) -> None:
        cfg = configparser.ConfigParser()
        cfg.read('../config/atsGrpcconfig.cfg')
        self.username = 'root'
        self.ip = self.get_target_ip() # update again locally since getTargetIP only covers 165.x.x.x
        self.password = SSHInfo.PW
        self.update_util_filename = None
        self.client = self.get_ssh_login()
        assert self.client != -1, 'ssh login using both ssh keys failed'
        self.fw_filename = fw_file
        self.cell_modem_filepath = '../cell_modem_fw/'
        self.modem_power_ctrl_path = '/opt/calamp/sbin/calamp_modem_pwrctrl'
        self.fw_version_major = v_major
        self.fw_version_minor = v_minor

    def sleepPrint(self, t):
        """
        prints a dot every second during sleepPrint
        """
        try:
            count = 0
            while count < t:
                time.sleep(1)
                print(".", end="", flush=True)
                count += 1
            print("\n")
        except KeyboardInterrupt:
            print("Keyboard Interrupt")

    def get_ssh_login(self):
        """
        PL243:
            - ski: SSHInfo.PL243_SKI_SSH_KEY
            - unski: SSHInfo.PL243_UNSKI_SSH_KEY
        PL641:
            - ski: SSHInfo.PL641_SKI_SSH_KEY
            - unski: No key
        """
        if '192' in self.ip:
            self.update_util_filename = UpgradeFile.FW_UTIL_PL641_FILENAME
            self.ski_status = 'no'
            return self.sshLogin(self.ip)
        elif '165' in self.ip:
            self.ski_status = self.get_ski_status()
            if self.ski_status == 'yes':
                pl243_client = self.sshLogin(self.ip, SSHInfo.PL243_SKI_SSH_KEY)
                if pl243_client != -1:
                    self.update_util_filename = UpgradeFile.FW_UTIL_PL243_FILENAME
                    return pl243_client
                pl641_client = self.sshLogin(self.ip, SSHInfo.PL641_SKI_SSH_KEY)
                if pl641_client != -1:
                    self.update_util_filename = UpgradeFile.FW_UTIL_PL641_FILENAME
                    return pl641_client
            elif self.ski_status == 'no':
                self.update_util_filename = UpgradeFile.FW_UTIL_PL243_FILENAME
                return self.sshLogin(self.ip, SSHInfo.PL243_UNSKI_SSH_KEY)
        assert self.update_util_filename != None, 'Update util filename not found'
        return -1

    def sshLogin(self, IP, key=None):
        """
        Creates a ssh session
        """
        print(f'SSH logging in to {IP}')
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        cfg = {
                'hostname': IP,
                'username': self.username,
                'disabled_algorithms': dict(pubkeys=['rsa-sha2-512', 'rsa-sha2-256']),
                'password': self.password
        }
        try:
            """
            3 ways to login to the device depending on the device:
            pl243:
                - skied: login using ssl/t6rx-id_rsa credentials
                - unskied: login using ssl/id_scorp credentials
            pl641:
                - skied: login using ssl/t6rx-id_rsa credentials
                - unskied: login without using credentials
            """
            client.connect(**cfg, key_filename=key, timeout=60)
            return client
        except Exception as e:
            print("Received error: " + str(e))
            return -1

    def download_fw_ftp(self, filename, path):
        ftp = None
        if '192' in self.ip:
            # download using scp
            scp = SCPClient(self.client.get_transport())
            scp.put(f'../cell_modem_fw/{filename}')
            self.client.exec_command(f'mv /root/{filename} /tmp/ && sync')
        else:
            with FTP(host=self.ip, passwd='anonymous') as ftp:
                print('ftp: {}'.format(self.ip))
                ftp.login()
                ftp.cwd('/ipflash')
                f = open(path + filename, 'rb')
                ftp.storbinary('STOR ' + filename, f)
                f.close()
                print("Closing ftp connection")
                ftp.close()
            self.client.exec_command(f'mv /opt/rpa-space/ipflash/{filename} /tmp/ && sync')

    def ping(self, host) -> bool:
        """
        Returns True if host (str) responds to a ping request.
        Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
        """
        try:
            print(f'Testing ping {host}')
            # Option for the number of packets as a function of
            param = '-n' if platform.system().lower() == 'windows' else '-c'

            # Building the command. Ex: "ping -c 1 google.com"
            command = ['ping', param, '1', host]
            output = subprocess.check_output(command)
            if 'time=' in str(output) and 'TTL=' in str(output): # unique in a success ping case
                return True
            else:
                return False
        except Exception as e:
            return False

    def get_ski_status(self):
        print('Getting SKI status')
        ftp = None
        with FTP(host=self.ip, passwd='anonymous') as ftp:
            ftp.login()
            tmp = sys.stdout
            res = StringIO()
            sys.stdout = res
            print(ftp.retrlines('LIST'))
            sys.stdout = tmp
            ski_res = res.getvalue()
            ftp.close()
            pattern = re.compile(CERT_FILE)
            if search(CERT_FILE, ski_res):
                print('Unit is SKIED')
                return 'yes'
            else:
                print('Unit is UNSKIED')
                return 'no'

    def get_target_ip(self):
        """
        Local function that adds additional ping test to the 192.xx.xx.xx IP in addition to the stock getTargetIP() which is only for
        165.xx.xx.xx
        """
        if self.ping(DeviceIPType.PL641_IP_BLT):
            return DeviceIPType.PL641_IP_BLT
        elif self.ping(DeviceIPType.PL641_IP_ULT):
            return DeviceIPType.PL641_IP_ULT
        else:
            return getTargetIP() # returns the original if pings above failed (belongs to 165.x.x.x)

    def verify_enum_ports(self):
        """
        Verify that the ports are enumerated correctly
        """
        print("Verifying enumeration ports")
        MAX_RETRIES = 10

        while MAX_RETRIES > 0:
            try:
                stdin, stdout, stderr = self.client.exec_command("ls /dev/ttyACM* | cut -f2 -d 'M'")
                acm_ports = stdout.read().decode().split('\n')
                print(f'{acm_ports}')
                if self.ski_status == 'no':
                    if '192' in self.ip:
                        count = 4
                    else:
                        count = 5
                elif self.ski_status == 'yes':
                    count = 4
                print(f'Expected count: {count}')
                for i in range(count):
                    if i != int(acm_ports[i]):
                        raise(Exception, 'Verify enumeration ports failed')
                break

            except Exception as e:
                print('Retrying verify enumeration port')
                time.sleep(5)
                MAX_RETRIES -= 1

        assert MAX_RETRIES > 0, f'Verify enumeration ports failed'

    def start_modem(self):
        print("Turning off app and starting modem", flush=True)
        with SSHClientInteraction(self.client, timeout=240, display = True) as interact:
            interact.send('export LD_LIBRARY_PATH=/lib:/opt/calamp/lib')
            interact.send('/opt/calamp/etc/init.d/S01mmgr stop')
            interact.expect('#*.', timeout = 60)
            print("Turning ON calamp modem")
            interact.expect('#*.', timeout = 2)
            interact.send(f'{self.modem_power_ctrl_path} on')
            interact.expect('#*.', timeout = 60)

    def start_apps(self):
        print("Turning off modem and starting apps", flush=True)
        with SSHClientInteraction(self.client, timeout=240, display = True) as interact:
            interact.send('export LD_LIBRARY_PATH=/lib:/opt/calamp/lib')
            interact.send(f'{self.modem_power_ctrl_path} off')
            interact.expect('#*.', timeout = 60)
            self.sleepPrint(2)
            interact.send('/opt/calamp/etc/init.d/S01mmgr start')
            interact.expect('#*.', timeout = 60)

    def restart_calamp_modem(self):
        print('Restarting Calamp Modem', flush=True)
        with SSHClientInteraction(self.client, timeout=240, display = True) as interact:
            interact.send('export LD_LIBRARY_PATH=/lib:/opt/calamp/lib')
            print("Turning OFF calamp modem")
            interact.send(f'{self.modem_power_ctrl_path} off')
            interact.expect('#*.', timeout = 60)
            print("Turning ON calamp modem")
            interact.expect('#*.', timeout = 2)
            interact.send(f'{self.modem_power_ctrl_path} on')
            interact.expect('#*.', timeout = 60)

    def verify_current_fw(self):
        MAX_RETRY = 10
        while MAX_RETRY > 0:
            try:
                with SSHClientInteraction(self.client, timeout=240, display = True) as interact:
                    interact.send('rm /var/lock/LCK..ttyACM1')
                    interact.expect('#*.', timeout = 60)
                    interact.send('microcom /dev/ttyACM1')
                    interact.expect(' ', timeout = 2)
                    interact.send("ati1")
                    interact.expect(' ', timeout = 2)
                    if self.fw_version_major in interact.current_output and self.fw_version_minor in interact.current_output:
                        break
                print("Retrying verify firmware")
                MAX_RETRY -= 1
                time.sleep(5)
            except Exception:
                pass
        assert MAX_RETRY > 0, 'Verify firmware fail'

    def download_fw(self):
        print("Downloading firmware onto device")
        self.download_fw_ftp(self.fw_filename, self.cell_modem_filepath)

    def download_update_util(self):
        print("Downloading update utility onto device")
        self.download_fw_ftp(self.update_util_filename, self.cell_modem_filepath)
        stdin, stdout, stderr = self.client.exec_command(f'chmod 755 /tmp/{self.update_util_filename}')

    def flash_fw(self):
        print('Flashing firmware', end='', flush=True)
        with SSHClientInteraction(self.client, timeout=240, display = True) as interact:
            interact.send(f'/tmp/{self.update_util_filename} -p /dev/ttyACM1 -f /tmp/{self.fw_filename} -u 1 -v')
            interact.expect(".*finished.*", timeout = 180)
            self.sleepPrint(5)

    def upgrade(self):
        """
        When giving a fw_file, fw major and minor are required for verification
        """
        self.download_fw()
        self.download_update_util()
        self.start_modem()
        self.verify_enum_ports()
        self.flash_fw()
        self.restart_calamp_modem()
        self.verify_enum_ports()
        self.verify_current_fw()
        self.start_apps()
        print(f'CELL MODEM FW UPGRADE PASS\n')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-fw_file',  dest='fw_file',  default = None, type=str,  help='fw_file - firmware file name')
    parser.add_argument('-v_major',  dest='v_major',  default = None, type=str,  help='v_major - major version')
    parser.add_argument('-v_minor',  dest='v_minor',  default = None, type=str,  help='v_minor - minor version')
    args = parser.parse_args()
    fw_upgrade = CellModemFwUpgrade()
    fw_upgrade.upgrade()