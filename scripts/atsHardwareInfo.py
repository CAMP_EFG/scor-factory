from atsModeminfo import *

COMMAND = RadioManager_pb2.HARDWARE_INFO

'''
Description: Displays the hardware info
Example:
	python atsHardwareInfo.py
'''
def getAtsHardwareInfo():
    response = grpcGetModemInfo(COMMAND)
    print("Raw response: " + str(response))
    return response

def displayAtsHardwareInfo():
    response = getAtsHardwareInfo()
    if response == -1:
        print("Error getting ats hardware info")
        return -1
    print("Serial Number                        : " + str(response.HardwareInfoValue.SerialNumber))
    print("HardwarePartNumber                   : " + str(response.HardwareInfoValue.HardwarePartNumber))
    print("RadioType                            : " + str(response.HardwareInfoValue.RadioType))
    print("EthernetMacAddress                   : " + str(response.HardwareInfoValue.EthernetMacAddress))
    print("SupplierPartNumber                   : " + str(response.HardwareInfoValue.SupplierPartNumber))
    print("ATSMpiCustPartNum                    : " + str(response.HardwareInfoValue.AtsMpiCustPartNum))
    print("ATSMpiAfterMarketID                  : " + str(response.HardwareInfoValue.AtsMpiAfterMarketID))
    print("ATSMpiEcmID                          : " + str(response.HardwareInfoValue.AtsMpiEcmID))
    print("ATSMpiPlatformID                     : " + str(response.HardwareInfoValue.AtsMpiPlatformID))
    print("ATSMpiMfgDate                        : " + str(response.HardwareInfoValue.AtsMpiMfgDate))
    print("ATSK22HwBoardId                      : " + str(response.HardwareInfoValue.AtsK22HwBoardId))
    print("ATSK22ActivePwrSource                : " + str(response.HardwareInfoValue.AtsK22ActivePwrSource))
    print("ATSK22LastRestartCause               : " + str(response.HardwareInfoValue.AtsK22LastRestartCause))

if __name__ == "__main__":
    displayAtsHardwareInfo()
