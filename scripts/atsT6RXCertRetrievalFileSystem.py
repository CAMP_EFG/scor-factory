'''
Author: Corey
Date Modified: 2/25/2019
Description: This class contains commands to retrieve and convert T6RX certificates
'''
import io, sys, subprocess, os, base64, time
from ftplib import FTP

'''
Download file from FTP server
'''
def ftpGetCert(filename):
	try:
		import extractIP
		with FTP(host=extractIP.extractIPAddr(), passwd = 'anonymous') as ftp_session:
			print("Ftp into device...")
			print(ftp_session.login())

			if filename in ftp_session.nlst():
				f = open(filename, 'wb')
				ftp_session.retrbinary('RETR ' + filename, f.write)
				f.close()
			else:
				print("Cert file not found!")
				exit()

			print("Closing ftp connection")
			ftp_session.close()
	except Exception as e:
		print("Unable to connect to T6RX" + str(e))
		exit()

	return

'''
Get PEM cert
'''
def getPemCert():
	pemCert = io.BytesIO()

	#Get .PEM and convert. PEM should ALWAYS be available
	print("\r\nDownloading uik_public.cer from FTP server\r\n")
	pemCert = ftpGetCert("uik_public.cer")
	#pemCert = ftpGetCert("FlashSystemInformation.xml")
	return pemCert

'''
Get the Common Name (CN) from PEM cert
'''
def getCommonName():
	result = subprocess.Popen("openssl x509 -noout -subject -in uik_public.cer", shell=True, stdout=subprocess.PIPE)
	result.wait()
	raw = str(result.stdout.read())
	cn = raw.split('CN')[1]
	cn = cn[:-5].replace(" ", "").replace("=", "")
	f = open("uik_public_cn.txt", 'wb') #save CN to file
	f.write(str.encode(cn))
	f.close()
	return

'''
Convert PEM to DER cert
'''
def convertPemToDer():
	result = subprocess.Popen('openssl x509 -outform der -in uik_public.cer -out uik_public.der', shell=True, stdout=subprocess.PIPE)
	result.wait()
	return

'''
Convert DER cert into base64
'''
def convertDerToBase64():
	derCert = open("uik_public.der", 'rb')
	derBase64 = base64.b64encode(derCert.read())
	derCert.close()

	derBase64Cert = open("uik_public.der.b64", 'wb')
	derBase64Cert.write(derBase64)
	derBase64Cert.close()

	# #Test base64 encoding
	# derBase64Cert = open("uik_public.der.b64", 'rb')
	# derBase64Decoded = base64.b64decode(derBase64Cert.read())
	# derBase64Cert.close()

	# derBase64DecodedCert = open("uik_public.der.b64.der", 'wb')
	# derBase64DecodedCert.write(derBase64Decoded)
	# derBase64DecodedCert.close()

	return

def getScorpionCerts():
	try:
		#os.remove("uik_public.cer")
		os.remove("uik_public.der")
		os.remove("uik_public_cn.txt")
		os.remove("uik_public.der.b64")
	except:
		pass

	getPemCert()
	getCommonName()
	convertPemToDer()
	convertDerToBase64()

if __name__ == "__main__":
	getScorpionCerts()
