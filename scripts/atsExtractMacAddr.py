import subprocess

'''
Date: 2/14/2019
Desciption: extracts mac address using the latest thd.py located in tools/ folder
'''
def extractMacAddr():
    print("Extracting mac address from thd.py")
    thdCommand = 'python ../lib/toolsThd.py -t 1'
    result = subprocess.Popen(thdCommand, shell=True, stdout=subprocess.PIPE)
    mac = ''
    raw = str(result.stdout.read())
    formatted = raw.split('\\r\\n')
    try:
        count = 0
        while count < 4:
            if count == 0 and formatted[0][7:19] != 'AAAAAAAAAAAA': break
            if count == 1 and formatted[1][5:17] != 'AAAAAAAAAAAA': break
            if count == 2 and formatted[2][5:17] != 'AAAAAAAAAAAA': break
            if count == 3 and formatted[3][5:17] != 'AAAAAAAAAAAA': break
            count += 1
        if count == 0: mac = formatted[count][7:19]
        else: mac = formatted[count][5:17]
        if mac == 'AAAAAAAAAAAA' or mac == '':
            print(mac)
            print("No mac address found")
            return -1
        else: print("MAC addr found: " + mac)
    except: pass

    return mac