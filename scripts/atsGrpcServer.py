'''
Description: Server simulator that populates info with random data when querying modem info and sends '0' for modem command
Author: Joshua Liew
Date: 7/26/2018
'''
from concurrent import futures
import time

import grpc

import RadioManager_pb2 as pb2
import RadioManager_pb2_grpc
import atsCalampgrpc as lib

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


'''class Greeter(helloworld_pb2_grpc.GreeterServicer):

    def SayHello(self, request, context):
        return helloworld_pb2.HelloReply(message='Hello, %s!' % request.name)'''

class ModemManager(RadioManager_pb2_grpc.ModemManagerServicer):
    def GetInfo(self, request, context):
        info = request.ModemManagerFilter
        if info == pb2.ATS_GET_TEMP: #30
            return pb2.ModemInfoMessage(AtsTempInfo=pb2.ATSTempInfo(\
            AtsImxTemp="23 degC", AtsCellTemp="24 degC", AtsIridiumTemp="25 degC"))

        if info == pb2.ATS_GET_RTC_TIME: #31
            return pb2.ModemInfoMessage(AtsRTCTime=pb2.ATSRTCTime(\
            AtsRtcDateTime="unknown"))
        
        if info == pb2.ATS_GET_ACCEL_VALUE: #32
            return pb2.ModemInfoMessage(AtsAccelInfo=pb2.ATSAccelInfo(\
            AtsAccelValue="unknown"))

        if info == pb2.ATS_GET_INPUT_STATES: #33
            return pb2.ModemInfoMessage(AtsAccelInfo=pb2.ATSAccelInfo(\
            AtsAccelValue="NOT IMPLEMENTED")) # NOT IMPLEMENTED IN PROTO FILE

        if info == pb2.ATS_GET_GPS_INFO: #34
            return pb2.ModemInfoMessage(AtsGpsInfo=pb2.ATSGpsInfo(\
            AtsGpsFix="unknown", AtsLatLong="unknown", AtsDateTime="unknown", AtsNumOfSatInView="unknown",  \
            AtsGroundSpeed="unknown",   AtsSatCNRInfo="unknown",AtsCNRAverage="unknown"))

        if info == pb2.ATS_GET_GPS_SAT_INFO: #35
            populated = pb2.ATSGpsSatInfo.ATSSatvsCNR(AtsSatNumber="unknown", AtsSatCnr="unknown")
            return pb2.ModemInfoMessage(AtsGpsSatInfo=pb2.ATSGpsSatInfo(AtsSatvsCnr=[populated])) #  POPULATE IN PROGRESS

        if info == pb2.ATS_K22_GET_RTEM_FREQ: #36
            return pb2.ModemInfoMessage(AtsRtermFreq=pb2.ATSRtermFreq(\
            AtsRtermValue="100Hz")) 

        if info == pb2.ATS_K22_GET_BATT_INFO: #37
            return pb2.ModemInfoMessage(AtsBatteryInfo=pb2.ATSBatteryInfo(\
            AtsChargingState="Boost Charging", AtsIntBattVolts="4.917 v", AtsIntBattTemp="15 degC", AtsIntBattHibState="Inactive",  \
            AtsBatterHealth="Battery OK",   AtsDACValue100mA="unknown",AtsDACValueZeromA="unknown",AtsMainBattVolts="17.422 v",     \
            AtsBackUpBattVolts="17.518 v",Ats42BoostVolts="4.905 v",Ats42SwitchVolts="4.247 v",Ats5Volts="5.139 v",AtsChargerVolts="5.133 v"))

    def SendCommand(self, request, context):
        return pb2.ModemCommandResponse(AtsCommandResponse="0")

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    RadioManager_pb2_grpc.add_ModemManagerServicer_to_server(ModemManager(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    print("Server started")
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()
