"""
Description: Display k22 battery info
Example:
	python atsK22GetBattLoad.py
    Raw output: AtsBatteryLoad {
                AtsBattLoad: "Enabled\r"
                }

    AtsBattLoad                   : Enabled

"""
from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_K22_GET_BATT_LOAD

def getk22BattLoad():
    response = grpcGetModemInfo(COMMAND)
    print("Raw output: " + str(response))
    return response

def displayAtsk22BattLoad():
    response = getk22BattLoad()
    if response == -1:
        print("Error getting ats k22 batt info")
        return -1
    print("AtsBattLoad                   : " + str(response.AtsBatteryLoad.AtsBattLoad))

if __name__ == "__main__":
    displayAtsk22BattLoad()