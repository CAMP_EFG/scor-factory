from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_GET_RTC_TIME

'''
Description: Display rtc time
Example: 
	python atsGetRtcTime.py
'''
def getAtsRtcTime():
    response = grpcGetModemInfo(COMMAND)
    return response

def displayAtsRtcTime():
    response = getAtsRtcTime()
    if response == -1:
        print("Error getting ats rtc time")
        return -1
    print("AtsRtcDateTime                          : " + str(response.AtsRTCTime.AtsRtcDateTime))

if __name__ == "__main__":
    displayAtsRtcTime()