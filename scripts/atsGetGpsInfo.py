from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_GET_GPS_INFO


'''
Description: Displays the gps info
Example:
	python atsGetGpsInfo.py
'''
def getAtsGpsInfo():
    response = grpcGetModemInfo(COMMAND)
    print("Raw output: " + str(response))
    return response

def displayAtsGpsInfo():
    response = getAtsGpsInfo()
    if response == -1:
        print("Error getting ats gps info")
        return -1
    print("AtsGpsFix                       : " + response.AtsGpsInfo.AtsGpsFix)
    print("AtsLatLong                      : " + response.AtsGpsInfo.AtsLatLong)
    print("AtsDateTime                     : " + response.AtsGpsInfo.AtsDateTime)
    print("AtsNumOfSatInView               : " + response.AtsGpsInfo.AtsNumOfSatInView)
    print("AtsGroundSpeed                  : " + response.AtsGpsInfo.AtsGroundSpeed)
    print("AtsSatCNRInfo                   : " + response.AtsGpsInfo.AtsSatCNRInfo)
    print("AtsCNRAverage                   : " + response.AtsGpsInfo.AtsCNRAverage)

if __name__ == "__main__":
    displayAtsGpsInfo()
