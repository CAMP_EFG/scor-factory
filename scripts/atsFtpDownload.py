from ftplib import FTP
import argparse

def ftpGetXML(filename):
    try:
        import atsExtractIP
        with FTP(host=atsExtractIP.extractIPAddr(), passwd = 'anonymous') as ftp_session:
            print("Ftp into device...")
            print(ftp_session.login())
            print(ftp_session.cwd('/ipflash'))
            f = open(filename, 'wb')
            print(ftp_session.retrbinary('RETR ' + filename, f.write))
            f.close()
            print("Closing ftp connection")
            ftp_session.close()
    except Exception:
        return -1

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='download filename from ftp', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-f',  dest='filename',  default = '', type=str,  help='f - filename')
    args = parser.parse_args()
    ftpGetXML(args.filename)
