import configparser, os

def boardPassFail(cfgPath, funct, passFail):
    try:
        absPathOfFile = os.path.dirname(os.path.realpath(__file__))
        currentWd = os.getcwd()
        os.chdir(absPathOfFile)

        cfg = configparser.ConfigParser()
        cfg.read(cfgPath)

        if funct == "K22":
            if passFail:
                k22PassCnt = int(cfg['K22'].get('pass'))
                k22PassCnt += 1
                cfg.set('K22', 'pass', str(k22PassCnt))
            else:
                k22PassCnt = int(cfg['K22'].get('fail'))
                k22PassCnt -= 1
                cfg.set('K22', 'fail', str(k22PassCnt))


        if funct == "SET_HIBERNATE":
            if passFail:
                setHibernateCnt = int(cfg['SET_HIBERNATE'].get('pass'))
                setHibernateCnt += 1
                cfg.set('SET_HIBERNATE', 'pass', str(setHibernateCnt))
            else:
                setHibernateCnt = int(cfg['SET_HIBERNATE'].get('fail'))
                setHibernateCnt -= 1
                cfg.set('SET_HIBERNATE', 'fail', str(setHibernateCnt))


        if funct == "WIRELESS_DISABLE_CGI":
            if passFail:
                wdCfgCnt = int(cfg['WIRELESS_DISABLE_CGI'].get('pass'))
                wdCfgCnt += 1
                cfg.set('WIRELESS_DISABLE_CGI', 'pass', str(wdCfgCnt))
            else:
                wdCfgCnt = int(cfg['WIRELESS_DISABLE_CGI'].get('fail'))
                wdCfgCnt -= 1
                cfg.set('WIRELESS_DISABLE_CGI', 'fail', str(wdCfgCnt))


        if funct == "WIRELESS_DISABLE_GRPC":
            if passFail:
                wdCfgGrpcCnt = int(cfg['WIRELESS_DISABLE_GRPC'].get('pass'))
                wdCfgGrpcCnt += 1
                cfg.set('WIRELESS_DISABLE_GRPC', 'pass', str(wdCfgGrpcCnt))
            else:
                wdCfgGrpcCnt = int(cfg['WIRELESS_DISABLE_GRPC'].get('fail'))
                wdCfgGrpcCnt -= 1
                cfg.set('WIRELESS_DISABLE_GRPC', 'fail', str(wdCfgGrpcCnt))

        cfgFile = open(cfgPath, 'w+')
        cfg.write(cfgFile)
        cfgFile.close()

        os.chdir(currentWd)

    except Exception as e:
        pass
