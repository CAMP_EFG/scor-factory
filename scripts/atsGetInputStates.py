from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_GET_INPUT_STATES

import RadioManager_pb2 as pb2

'''
Description: Display input states
Example: 
	python atsGetInputStates.py
'''
def getAtsInputStates():
    response = grpcGetModemInfo(COMMAND)
    print("Raw output: " + str(response))
    return response

def displayAtsInputStates():
    response = getAtsInputStates()
    if response == -1:
        print("Error getting ats input state")
        return -1
    print("AtsKeySwState                 : " + response.AtsGpioInfo.AtsKeySwState)
    print("AtsWirelessDisState           : " + response.AtsGpioInfo.AtsWirelessDisState)
    print("AtsK22KeySwState              : " + response.AtsGpioInfo.AtsK22KeySwState)
    print("AtsK22WirelessDisState        : " + response.AtsGpioInfo.AtsK22WirelessDisState)
    print("AtsK22AccelIntState           : " + response.AtsGpioInfo.AtsK22AccelIntState)
    print("AtsK22RtcIntState             : " + response.AtsGpioInfo.AtsK22RtcIntState)
    print("AtsK22eFuse                   : " + response.AtsGpioInfo.AtsK22eFuse)
    print("AtsK22eWakeUpState            : " + response.AtsGpioInfo.AtsK22eWakeUpState)

if __name__ == "__main__":
    displayAtsInputStates()