import os, sys, argparse

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################

from atsCalampgrpc import *
COMMAND = RadioManager_pb2.CELL_WAN_DOWN

from atsModemCommand import grpcAtsSendCommand
'''
Description: Cell wan down command
Example:
	python atsCellWanDown.py
'''
def grpcAtsCellWanDown(value1 = None, value2 = None, value3 = None, value4 = None):
	grpcAtsSendCommand(COMMAND, value1, value2, value3, value4)

if __name__ == "__main__":
	grpcAtsCellWanDown()