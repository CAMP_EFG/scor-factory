from atsModeminfo import *
from atsSwInfo import getAtsSwInfo
from atsCellDynamic import getAtsCellDynamic
from atsSatStatic import getAtsSatStatic
from atsHardwareInfo import getAtsHardwareInfo
from atsCellStatic import getAtsCellStatic

'''
Description: Wrapper script to get info from swinfo and satstatic
python atsGetFWInfoRead.py
'''
def displayFWInfoRead():
    swInfo = getAtsSwInfo()
    if swInfo == -1:
        print("Error getting software info")
        return -1
    fwInfoOutput = ""
    fwInfoOutput += "SupplierSoftwarePartNumber                   : " + str(swInfo.SoftwareInfoValue.SupplierSoftwarePartNumber) + "\n"
    fwInfoOutput += "CaterpillarSoftwarePartNumber                : " + str(swInfo.SoftwareInfoValue.CaterpillarSoftwarePartNumber) + "\n"
    fwInfoOutput += "ATSCellModemFwVersion                        : " + swInfo.SoftwareInfoValue.AtsCellModemFwVersion + "\n"
    fwInfoOutput += "ATSIridiumModemFwVersion                     : " + swInfo.SoftwareInfoValue.AtsIridiumModemFwVersion + "\n"
    fwInfoOutput += "ATSK22FwVersion                              : " + swInfo.SoftwareInfoValue.AtsK22FwVersion + "\n"
    fwInfoOutput += "ATSKernelVersion                              : " + str(swInfo.SoftwareInfoValue.AtsKernelVersion) + "\n"


    print("SupplierSoftwarePartNumber                   : " + str(swInfo.SoftwareInfoValue.SupplierSoftwarePartNumber))
    print("CaterpillarSoftwarePartNumber                : " + str(swInfo.SoftwareInfoValue.CaterpillarSoftwarePartNumber))
    print("ATSCellModemFwVersion                        : " + swInfo.SoftwareInfoValue.AtsCellModemFwVersion)
    print("ATSIridiumModemFwVersion                     : " + swInfo.SoftwareInfoValue.AtsIridiumModemFwVersion)
    print("ATSK22FwVersion                              : " + swInfo.SoftwareInfoValue.AtsK22FwVersion)
    print("ATSKernelVersion                              : " + str(swInfo.SoftwareInfoValue.AtsKernelVersion))

    '''satStatic = getAtsSatStatic()
    if satStatic == -1:
        print("Error getting software info")
        return -1
    fwInfoOutput += "ModemFirmwareVersion                         : " + satStatic.SatStaticValue.ModemFirmwareVersion + "\n"
    print("ModemFirmwareVersion                         : " + satStatic.SatStaticValue.ModemFirmwareVersion)'''

    return fwInfoOutput

if __name__ == "__main__":
    displayFWInfoRead()