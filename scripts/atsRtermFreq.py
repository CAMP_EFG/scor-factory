from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_K22_GET_RTEM_FREQ

'''
Description: Displays the rterm frequency
Example: python atsRtermFreq.py
'''
def AtsRtermFreq():
    response = grpcGetModemInfo(COMMAND)
    print("Raw output: " + str(response))
    return response

def displayAtsRtermFreq():
    response = AtsRtermFreq()
    if response == -1:
        print("Error getting ats gps info")
        return -1
    print("AtsRtermValue                       : " + response.AtsRtermFreq.AtsRtermValue)

if __name__ == "__main__":
    displayAtsRtermFreq()
