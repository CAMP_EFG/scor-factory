from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_SKI_CHECK

import RadioManager_pb2 as pb2

'''
Description: The primary use of this script is for units that run RMA firmware
            to check if the device is SKIed or not. This way the ATS CONFIG
            station does not have to distinguish between RMA or regular unit.
Example:
	atsCalampSkiCheck.py:
    python3 atsCalampSkiCheck.py
    AtsSkiStatus                  : SKI not Provisioned
'''
def getAtsCalampSkiCheck():
    response = grpcGetModemInfo(COMMAND)
    return response

def displayAtsSkiCheck():
    response = getAtsCalampSkiCheck()
    if response == -1:
        print("Error getting ats ski check")
        return -1
    print("AtsSkiStatus                  : " + response.AtsSkiCheck.AtsSkiStatus)

if __name__ == "__main__":
    displayAtsSkiCheck()