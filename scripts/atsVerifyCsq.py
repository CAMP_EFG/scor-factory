'''
Author: Joshua
Date: 12/4/2018
Description: Verifies CSQ >= 3
'''

import atsSetIridiumRssiAverage, atsSatDynamic, time

def verifyCSQ():
	csq = 0
	csqTryCnt = 0
	csqTotalTryCnt = 200 # acts like an infinite loop but 200 to be safe
	# set rssi avg to 1 using grpc below
	if atsSetIridiumRssiAverage.grpcAtsSetIridumRssiAverage("1") == -1: return -1
	while int(csq) < 3 and csqTryCnt < csqTotalTryCnt:
		satDynamicRes = atsSatDynamic.getAtsSatDynamic()
		if satDynamicRes == -1: return -1
		csq = str(satDynamicRes.SatDynamicValue.ConsistentSignalQualityAverage)
		print("Current CSQ: " + csq)
		if int(csq) > 2:
			time.sleep(5) # sleep 5 sec when csq > 2 to be stable
			#Write_QTS_Tests(property='SBD_SEQ_NUMBER', result = 'Passed', value='', minRange='', maxRange='', free_text = '', step_duration = 0) #TBD
		else: # csq <= 2 here
			print("CSQ [" + csq + "] lower than 3, waiting 20s until CSQ improves. getCSQ attempt: (" + str(csqTryCnt) + "/" + str(csqTotalTryCnt) + ")")
			time.sleep(20)
			csqTryCnt += 1

if __name__ == "__main__":
    verifyCSQ()