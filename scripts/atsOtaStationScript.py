'''
verify wan IP from cellDynamic
modify mac, sn, part number, and mfg if arguments exist
read and verify hardware info
write to txt file
do ski-staging
'''
import os, sys, time

absPathOfFile = os.path.dirname(os.path.realpath(__file__))
currentWd = os.getcwd()
os.chdir(absPathOfFile) # change absolute path of atsMaster.py to import atsCalampgrpc

p = os.path.abspath('../nonSsl/')
if p not in sys.path:
    sys.path.append(p)

from atsCalampgrpc import RadioManager_pb2, atsGrpcLocal
from atsGetFWInfoRead import displayFWInfoRead
import argparse, os
from time import gmtime, strftime
rpc = atsGrpcLocal()

serialNo = ""

os.chdir(currentWd) # then change directory back to original cwd

def verifyWanIp():
    response = rpc.grpcAtsGetModemInfo(RadioManager_pb2.CELL_DYNAMIC)

    if response == -1:
        return -2

    # verify wan IP here
    if response.CellDynamicValue.AtsCellWanIpAddr == None or str(response.CellDynamicValue.AtsCellWanIpAddr) == "":
        return -1
    else:
        print("WAN IP is " + str(response.CellDynamicValue.AtsCellWanIpAddr))
    return 0

def modify(name, value, value1, value2, value3):
    if name == "mac":
        print("Setting mac address")
        rpc.grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_LAN_MAC_ADDR, value, value1, value2, value3)
    if name == "sn":
        print("Setting serial number")
        rpc.grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_SERIAL_NUM, value, value1, value2, value3)
    if name == "partno":
        print("Setting part number")
        rpc.grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_HW_PART_NUM, value, value1, value2, value3)
    if name == "mfg":
        print("Setting manufacturing date")
        rpc.grpcAtsSendCommand(RadioManager_pb2.ATS_MPI_SET_MFG_DATE, value, value1, value2, value3)

def readAndVerifyHwInfo(f):
    global serialNo
    #read
    response = rpc.grpcAtsGetModemInfo(RadioManager_pb2.HARDWARE_INFO)
    if response == -1: return -1

    #verify
    serialNo = response.HardwareInfoValue.SerialNumber
    hardwarePartNumber = response.HardwareInfoValue.HardwarePartNumber
    ethernetMacAddr = response.HardwareInfoValue.EthernetMacAddress
    if len(serialNo) < 16:
        f.write("Invalid serialNo: Invalid length\n")
        return -1
    elif len(hardwarePartNumber) < 10:
        f.write("Invalid hardwarePartNumber: Invalid length\n")
        return -1
    elif len(ethernetMacAddr) < 17:
        f.write("Invalid ethernetMacAddr: Invalid length\n")
        return -1

    f.write("HwInfo Raw Response: " + str(response) + "\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='See header comment', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-n',  dest='name',  default = '', type=str,  help='n - Name to modify')
    parser.add_argument('-v', dest='value', default = '', type=str, help='v1 - CommandValue')
    parser.add_argument('-v1', dest='value1', default = '', type=str, help='v2 - AtsCommandValue1')
    parser.add_argument('-v2', dest='value2', default = '', type=str, help='v3 - AtsCommandValue')
    parser.add_argument('-v3', dest='value3', default = '', type=str, help='v3 - AtsCommandValue3')
    args = parser.parse_args()

    '''
        loop verifywanip, if fail wait 5 sec, repeat 6 times
        ignore count +=1 if grpc COMMAND FAIL error
    '''
    '''if verifyWanIp() == -1:
        print("Cell OTA WAN Test FAILED!")
        os._exit(-1)
    else:
        print("Cell OTA WAN Test PASSED!")'''
    count = 0
    grpcCommErrCount = 0
    res = verifyWanIp()
    while (res == -1 and count < 6) or (res == -2 and grpcCommErrCount < 8):
        print("Verifying Wan IP, retrying...")
        time.sleep(5)
        if res == -1: count += 1
        if res == -2: grpcCommErrCount += 1
        res = verifyWanIp()

    if count >= 6:
        print("Cell OTA WAN Test FAILED!")
        os._exit(-1)
    else:
        print("Cell OTA WAN Test PASSED!")

    if args.name != "":
        modify(args.name, args.value, args.value1, args.value2, args.value3)

    #create file
    os.system("del C:\Temp\PL243_PV1\DUT.txt")
    f = open('C:\Temp\PL243_PV1\DUT.txt', 'w')
    if readAndVerifyHwInfo(f) == -1:
        f.write("HW Info Unavailable\n")
        os._exit(-1)
    else:
        fwOut = displayFWInfoRead()
        if fwOut == -1:
            f.write("FW Info Unavailable\n")
            f.close()
            os._exit(-1)
        else:
            f.write(str(fwOut)) #write fw info

    result = -1
    # ski staging here
    # atsSkiStaging.py -v "" -v1 prod_uboot.imx -v2 "" -v3 userspace_burn_efuses.sh

    print("Preparing the Device Under Test for SKI...")
    if rpc.grpcAtsSendCommand(RadioManager_pb2.ATS_SKI_STAGING, "", "prod_uboot.imx", "", "userspace_burn_efuses.sh") == 0:
        print("SKI staging SUCCESSFUL!")
        f.write("=====SKI staging SUCCESSFUL!=====")
        result = 0
    else:
        print("SKI staging FAILED!")
        f.write("=====SKI staging FAILED!=====")
        result = 1

    f.close()

    #get time and date
    dateAndTime = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
    SN_Date_n_Time = ""
    SN_Date_n_Time += serialNo + "_" + dateAndTime
    txtName = "C:\Temp\PL243_PV1\\" + SN_Date_n_Time + ".txt"
    cmdName = "move C:\Temp\PL243_PV1\DUT.txt " + txtName
    os.system(cmdName)

    #log sysLog to local PC
    import atsTftpSyslog as tftp
    tftp.tftpSysLog()

    if result != 0:
        print("OTA Test Complete - Test FAILED!")
        os._exit(-1)
    else:
        print("OTA Test Complete - Test SUCCESSFUL! Power Down the Device Under Test...")
        os._exit(0)
