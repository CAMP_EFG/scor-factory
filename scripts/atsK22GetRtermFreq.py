from atsModeminfo import *
COMMAND = RadioManager_pb2.ATS_K22_GET_RTEM_FREQ

'''
Description: Display K22 get rterm frequencies
Example: 
	python atsK22GetRtermFreq.py
'''
def getAtsk22RtermFreq():
    response = grpcGetModemInfo(COMMAND)
    return response

def displayAtsk22RtermFreq():
    response = getAtsk22RtermFreq()
    if response == -1:
        print("Error getting ats k22 rterm freq")
        return -1
    print("AtsRtermValue                 : " + response.AtsRtermFreq.AtsRtermValue)

if __name__ == "__main__":
    displayAtsk22RtermFreq()