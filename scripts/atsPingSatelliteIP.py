'''
Author: Joshua
Date Modified: 1/2/2018
Description: ping satellite IP
'''

import sys, atsSatDynamic, time, atsPingIp, atsSetIridiumRssiAverage, atsSatCsdDown, atsSatCsdUp
def pingIP():
	sys.path.append('C:/CalAmpATE2/Models/GRPC/nonSsl')
	pingIPCount = 0

	satIPCount = 0
	satDynamicRes = atsSatDynamic.getAtsSatDynamic()
	if satDynamicRes == -1: return -1
	while satIPCount < 10 and str(satDynamicRes.SatDynamicValue.AtsPeerPPPIpAddr).strip() == "":
		print("Waiting for Satellite IP...")
		time.sleep(10)
		satDynamicRes = atsSatDynamic.getAtsSatDynamic()
		if satDynamicRes == -1: return -1
		satIPCount += 1

	if satIPCount >= 10: return -1
	else:
		peepIPaddr = str(satDynamicRes.SatDynamicValue.AtsPeerPPPIpAddr).strip()
		print("Found Satellite IP" + peepIPaddr)
	if atsSetIridiumRssiAverage.grpcAtsSetIridumRssiAverage("1") == -1: return -1
	while pingIPCount < 15:
		print("Retrying to ping satellite IP...")
		# the satellite IP address is statically written down
		#if atsPingIp.grpcAtsPingIp('192.168.53.254') == -1:
		if atsPingIp.grpcAtsPingIp(peepIPaddr) == -1:
			if atsSatCsdDown.grpcAtsSatCsdDown() == -1: return -1
			time.sleep(60)
			if atsSatCsdUp.grpcAtsSatCsdUp() == -1: continue # don't ping if csd up fails
		else:
			print("PING SUCCESS")
			return 0
		pingIPCount += 1
	if pingIPCount >= 15: return -1

if __name__ == "__main__":
    pingIP()