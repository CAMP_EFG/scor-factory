from atsModeminfo import *
'''
Description: Display internal battery volt info
Example:
	python atsK22GetIntbattVolts.py
'''
def getk22IntbattVolts():
    response = grpcGetModemInfo(RadioManager_pb2.ATS_K22_GET_INTBATT_VOLTS)
    print("Raw output: " + str(response))
    return response

def displayK22IntbattVolts():
    response = getk22IntbattVolts()
    if response == -1:
        print("Error getting ats k22 internal battery volt info")
        return -1
    print("AtsIntBattVolts                   : " + str(response.AtsIntBattVolts.AtsIntBattVolts))

if __name__ == "__main__":
    displayK22IntbattVolts()