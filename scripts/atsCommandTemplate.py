import os, sys, argparse

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################

from atsCalampgrpc import *
COMMAND = grpcLib.cmd.get('NUMBER')
from atsModemCommand import grpcAtsSendCommand

'''
Description: Template for every atsCommand script there is in this directory
NOTE: (11/15/2018) keeping manual version of self contained grpc command just in case
'''
def grpcAts(value1 = None, value2 = None, value3 = None, value4 = None):
	try:
		modemMgr_stub = RadioManager_pb2_grpc.ModemManagerStub(getClientChannel())
		request = RadioManager_pb2.ModemCommandRequest(Command = int(COMMAND), CommandValue = value1, AtsCommandValue1 = value2, AtsCommandValue2 = value3, AtsCommandValue3 = value4)
		response = modemMgr_stub.SendCommand(request)
		print(response)
		print("OK")
	except Exception as e:
		print("Grpc send command error: " + str(e))
		return -1

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Description:\nAn interface to send grpc ATS commands individually.\nAuthor:\t\tJoshua Liew\nDate Created:\t7/24/2018', formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v',  dest='value',  default = '', type=str,  help='v - CommandValue')
	parser.add_argument('-v1', dest='value1', default = '', type=str, help='v1 - AtsCommandValue1')
	parser.add_argument('-v2', dest='value2', default = '', type=str, help='v2 - AtsCommandValue2')
	parser.add_argument('-v3', dest='value3', default = '', type=str, help='v3 - AtsCommandValue3')
	args = parser.parse_args()
	grpcAts(args.value, args.value1, args.value2, args.value3)