import os, sys, argparse

##########################################################################################
p = os.path.abspath('../lib/')
sys.path.append(p)
from atsCalampCommonPy35 import atsSetup
atsSetup()
##########################################################################################

from atsCalampgrpc import *
#debug
import pdb

'''
Description: Runs all ats grpc commands at once with specified parameters for each of them
			- Does not run commands that will affect the modem operating mode or state
Example:
	python atsCommandPopulate.py
'''
if __name__ == "__main__":
    atsRpc = atsGrpcLocal()
    fc = open("commandResult", "w")

    # COMMAND
    idx = 0
    while idx < 100:
        try:
            print("Sending COMMAND: " + RadioManager_pb2.MODEM_COMMAND.Name(idx))
            #if idx == 11:
            #    response = atsRpc.atsGrpcSendCommand(idx, "123")
            #    fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '123', Response: " + str(response) + "\n\n")
            if idx == 12:
                response = atsRpc.atsGrpcSendCommand(idx, "123")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '123', Response: " + str(response) + "\n\n")
            elif idx == 13:
                response = atsRpc.atsGrpcSendCommand(idx, "joshua")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: 'joshua', Response: " + str(response) + "\n\n")
            elif idx == 14:
                response = atsRpc.atsGrpcSendCommand(idx, "123")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '123', Response: " + str(response) + "\n\n")
            elif idx == 15:
                response = atsRpc.atsGrpcSendCommand(idx, "123")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '123', Response: " + str(response) + "\n\n")
            elif idx == 49:
                response = atsRpc.atsGrpcSendCommand(idx, "install", "pl243_dv2_mpi_template_ski.bin", "165.26.79.1")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 50:
                response = atsRpc.atsGrpcSendCommand(idx, "1234567-89")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '1234567-89', Response: " + str(response) + "\n\n")
            elif idx == 51:
                response = atsRpc.atsGrpcSendCommand(idx, "00000000")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '00000000', Response: " + str(response) + "\n\n")
            elif idx == 52:
                response = atsRpc.atsGrpcSendCommand(idx, "0001")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '0001', Response: " + str(response) + "\n\n")
            elif idx == 53:
                response = atsRpc.atsGrpcSendCommand(idx, "0000")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '0000', Response: " + str(response) + "\n\n")
            elif idx == 54:
                response = atsRpc.atsGrpcSendCommand(idx, "12:34:56:78:90:12")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '12:34:56:78:90:12', Response: " + str(response) + "\n\n")
            elif idx == 55:
                response = atsRpc.atsGrpcSendCommand(idx, "")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 56:
                response = atsRpc.atsGrpcSendCommand(idx, "18062000H002003F")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '1234567999', Response: " + str(response) + "\n\n")
            elif idx == 57:
                response = atsRpc.atsGrpcSendCommand(idx, "2018/10/19")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '2018/08/14', Response: " + str(response) + "\n\n")
            elif idx == 58:
                response = atsRpc.atsGrpcSendCommand(idx, "0000")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '0000', Response: " + str(response) + "\n\n")
            elif idx == 59:
                response = atsRpc.atsGrpcSendCommand(idx, "")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 60:
                response = atsRpc.atsGrpcSendCommand(idx, "")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 61:
                response = atsRpc.atsGrpcSendCommand(idx, "")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 62:
                response = atsRpc.atsGrpcSendCommand(idx, "")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 63:
                response = atsRpc.atsGrpcSendCommand(idx, "PL083")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: 'PL083', Response: " + str(response) + "\n\n")
            elif idx == 64:
                response = atsRpc.atsGrpcSendCommand(idx, "\"2018-08-03 10:20:40\"")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '2018-08-03 10:20:40', Response: " + str(response) + "\n\n")
            elif idx == 65:
                response = atsRpc.atsGrpcSendCommand(idx)
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '2018-08-03 10:20:40', Response: " + str(response) + "\n\n")
            elif idx == 66:
                response = atsRpc.atsGrpcSendCommand(idx, "4")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '25', Response: " + str(response) + "\n\n")
            elif idx == 67:
                response = atsRpc.atsGrpcSendCommand(idx, "start", "24", "35")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: 'start, 24, 25', Response: " + str(response) + "\n\n")
            elif idx == 68:
                response = atsRpc.atsGrpcSendCommand(idx, "")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 69:
                response = atsRpc.atsGrpcSendCommand(idx, "yellow", "on")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: 'yellow', Response: " + str(response) + "\n\n")
            elif idx == 70:
                response = atsRpc.atsGrpcSendCommand(idx, "")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 71:
                response = atsRpc.atsGrpcSendCommand(idx, "off")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: 'gps', Response: " + str(response) + "\n\n")
            elif idx == 72:
                response = atsRpc.atsGrpcSendCommand(idx, "sendatcmd", "AT+^12at^SCFG?")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: 'sendatcmd, AT+^12', Response: " + str(response) + "\n\n")
            elif idx == 73:
                response = atsRpc.atsGrpcSendCommand(idx, "S01mmgr", "start")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: 'S01mmgr stop', Response: " + str(response) + "\n\n")
            elif idx == 74:
                response = atsRpc.atsGrpcSendCommand(idx, "on")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: 'on', Response: " + str(response) + "\n\n")
            elif idx == 75:
                response = atsRpc.atsGrpcSendCommand(idx, "165.26.79.1")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '165.26.79.1', Response: " + str(response) + "\n\n")
            #elif idx == 76:
            #    response = atsRpc.atsGrpcSendCommand(idx, "install", "calamp_apps_02018101901.verity", "165.26.79.1")
            #    fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '165.26.79.1', Response: " + str(response) + "\n\n")
            elif idx == 77:
                response = atsRpc.atsGrpcSendCommand(idx)
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 78:
                response = atsRpc.atsGrpcSendCommand(idx)
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 79:
                response = atsRpc.atsGrpcSendCommand(idx, "off")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '165.26.79.1', Response: " + str(response) + "\n\n")
            elif idx == 80:
                response = atsRpc.atsGrpcSendCommand(idx, "write", "disable", "1")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '165.26.79.1', Response: " + str(response) + "\n\n")
            elif idx == 81:
                response = atsRpc.atsGrpcSendCommand(idx)
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 82:
                response = atsRpc.atsGrpcSendCommand(idx)
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " Response: " + str(response) + "\n\n")
            elif idx == 83:
                response = atsRpc.atsGrpcSendCommand(idx, "enable")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '165.26.79.1', Response: " + str(response) + "\n\n")
            elif idx == 84:
                response = atsRpc.atsGrpcSendCommand(idx, "", "write", "enable", "1")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '165.26.79.1', Response: " + str(response) + "\n\n")
            elif idx == 85:
                response = atsRpc.atsGrpcSendCommand(idx, "on")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '165.26.79.1', Response: " + str(response) + "\n\n")
            elif idx == 87:
                response = atsRpc.atsGrpcSendCommand(idx, "install", "otp_script.sh", "165.26.79.1")
                fc.write(RadioManager_pb2.MODEM_COMMAND.Name(idx) + " ARGUMENTS: '165.26.79.1', Response: " + str(response) + "\n\n")
            print(response)

        except Exception:
            pass
        idx += 1


    fc.close()