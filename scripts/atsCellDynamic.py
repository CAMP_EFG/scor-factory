from atsModeminfo import *
import os, sys, argparse



COMMAND = RadioManager_pb2.CELL_DYNAMIC

'''
Description: Displays Cell Dynamic Info
Example: 
	python atsCellDynamic.py
'''
def getAtsCellDynamic():
    response = grpcGetModemInfo(COMMAND)
    print("Raw response: " + str(response))
    return response

def displayAtsCellDynamic():
    response = getAtsCellDynamic()
    if response == -1:
        print("Error getting software info")
        return -1
    print("MobileCountryCode                                : " + str(response.CellDynamicValue.MobileCountryCode))
    print("MobileNetworkCode                                : " + str(response.CellDynamicValue.MobileNetworkCode))
    print("SignalStrength                                   : " + str(response.CellDynamicValue.SignalStrength))
    print("ServiceProviderName                              : " + response.CellDynamicValue.ServiceProviderName)
    print("RadioPreferredNetworkMode                        : " + response.CellDynamicValue.RadioPreferredNetworkMode)
    print("PdpContextActive                                 : " + str(response.CellDynamicValue.PdpContextActive))
    print("AntennaDiagnostic                                : " + str(response.CellDynamicValue.AntennaDiagnostic))
    print("Band                                             : " + str(response.CellDynamicValue.Band))
    print("Channel                                          : " + str(response.CellDynamicValue.Channel))
    print("RadioFrequencyChannelNumber                      : " + str(response.CellDynamicValue.RadioFrequencyChannelNumber))
    print("AccessPointBand                                  : " + str(response.CellDynamicValue.AccessPointBand))
    print("ModemState                                       : " + str(response.CellDynamicValue.ModemState))
    print("OperatingMode                                    : " + str(response.CellDynamicValue.OperatingMode))

if __name__ == "__main__":
    displayAtsCellDynamic()