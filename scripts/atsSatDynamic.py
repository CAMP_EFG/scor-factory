from atsModeminfo import *
COMMAND = RadioManager_pb2.SAT_DYNAMIC

'''
Description: Gets the Sat Dynamic information
'''
def getAtsSatDynamic():
    response = grpcGetModemInfo(COMMAND)
    print("Raw response: " + str(response))
    return response

def displayAtsSatDynamic():
    response = getAtsSatDynamic()
    if response == -1:
        print("Error getting software info")
        return -1
    print("ConsistentSignalQualityAverage                               : " + str(response.SatDynamicValue.ConsistentSignalQualityAverage))
    print("OperatingMode                                                : " + str(response.SatDynamicValue.OperatingMode))
    print("ModemAvailable                                               : " + str(response.SatDynamicValue.ModemAvailable))
    print("ModemPowerFailure                                            : " + str(response.SatDynamicValue.ModemPowerFailure))
    print("ModemState                                                   : " + str(response.SatDynamicValue.ModemState))
    print("OperatingMode                                                : " + str(response.SatDynamicValue.OperatingMode))
    print("AtsPeerPPPIpAddr                                             : " + str(response.SatDynamicValue.AtsPeerPPPIpAddr))

if __name__ == "__main__":
    displayAtsSatDynamic()