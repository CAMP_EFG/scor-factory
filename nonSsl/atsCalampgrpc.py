'''This class contains commands for grpc's MODEM_COMMAND and MODEM_MESSAGE'''
import os
import re
import sys, time
import grpc
import RadioManager_pb2
import RadioManager_pb2_grpc


def verifyPyVer():
	if not re.search("3", sys.version):
		print("You need to use Python version 3.x.x")
		os._exit(1)

'''
NOTE: Assumes exception to be a grpc error message
grpcType:
c - command
q - query
f - in full detail of grpc error message
'''
def printErr(e, grpcType):
	if grpcType   == 'c':
		print("GRPC COMMAND FAILED")
		print(e.details())
	elif grpcType == 'q':
		print("GRPC QUERY FAILED")
		print(e.details())
	elif grpcType == 'f':
		print(str(e))
	print(str(e.code()))

verifyPyVer()
import configparser
'''configparser'''

cfg = configparser.ConfigParser()
absPath = os.path.dirname(os.path.realpath(__file__))
cwd = os.getcwd()
os.chdir(absPath)

cfg.read('../config/atsGrpcconfig.cfg')
svr = cfg['SERVER_INFO']

os.chdir(cwd)

'''GETTERS'''
def getClientChannel(targetIP):
	if targetIP == '': targetIP = getTargetIP()
	channel = grpc.insecure_channel(getServerAddress(targetIP))
	return channel

'''
Example: 165.26.79.1:60051
'''
def getServerAddress(targetIP):
	serverAddress	=	''
	serverAddress = targetIP+':' + svr.get('SERVER_PORT')
	return serverAddress


'''
Gets the IP address of the 1st instance of the board connected
to the PC
'''
def getTargetIP():
	print("Searching for IP target...")
	if (svr.get('target_ip') != ""):
		targetIP = svr.get('target_ip')
		print("Target IP found in config file: " + targetIP)
		return targetIP

	absPath = os.path.dirname(os.path.realpath(__file__))
	cwd = os.getcwd()
	os.chdir(absPath)
	import importlib
	p = os.path.abspath('../lib/')
	if p not in sys.path:
		sys.path.append(p)
	import thd35
	importlib.reload(thd35)

	if(thd35.targetIP == "" or thd35.targetIP == svr.get('HOST_IP')):
		print("No target IP found")
		return ''
	else:
		print("Target IP found: " + thd35.targetIP)
		return thd35.targetIP

	os.chdir(cwd)

class atsGrpcLocal():
	def __init__(self):
		pass

	def atsGrpcSendCommand(self, msgId, value1 = None, value2 = None, value3 = None, value4 = None):
		try:
			modemMgr_stub = RadioManager_pb2_grpc.ModemManagerStub(getClientChannel())
			request = RadioManager_pb2.ModemCommandRequest(Command = int(msgId), CommandValue = value1, AtsCommandValue1 = value2, AtsCommandValue2 = value3, AtsCommandValue3 = value4)
			response = modemMgr_stub.SendCommand(request)
			if msgId != RadioManager_pb2.ATS_SKI_STAGING:
				print("Response: " + str(response.AtsCommandResponse))
			return 0
		except Exception as e:
			printErr(e, 'c')
			return -1

	def atsGrpcGetModeminfo(self, msgId):
		try:
			modemMgr_stub = RadioManager_pb2_grpc.ModemManagerStub(getClientChannel())
			request = RadioManager_pb2.ModemManagerFilterMessage(ModemManagerFilter = int(msgId))
			response = modemMgr_stub.GetInfo(request)
			print("OK")
			print("Response: " + str(response))
			return response
		except Exception as e:
			printErr(e, 'q')
			return -1

	def atsGetModemInfoStream(self, msgId):
		try:
			f = open("report", "a")
			channel = getClientChannel()
			modemMgr_stub = RadioManager_pb2_grpc.ModemManagerStub(channel)
			request = RadioManager_pb2.ModemManagerFilterMessage(ModemManagerFilter = int(msgId))
			responses = modemMgr_stub.GetModemDataStream(request)
			channel.close()
			initial = True
			prev = ""
			for response in responses:
				f.write("Tag Id: " + str(hex(id(responses))) + "\n")
				f.write(str(response) + "\n")
				if initial == True:
					prev = id(responses)
					initial = False
				elif prev != id(responses):
					print("Stream " + msg.get(str(msgId)) + ": FAIL")
					return -1
				else:
					f.write("Verified ID: " + str(hex(id(responses))) + "\n")
					print("Stream " + msg.get(str(msgId)) + ": PASS")
					return 0
			f.close()
		except Exception as e:
			print("grpc stream info error")
			print(e)
			return str(e)
