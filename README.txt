DEPENDENCIES

Python 3.5 and above

python requests library
	python get-pip.py requests
latest openssl
	https://slproweb.com/products/Win32OpenSSL.html
	add C:\OpenSSL-Win64\bin to environment variable for your account
Set up 172 series
	172.31.234.157/255.255.255.224
7-zip
	https://www.7-zip.org/download.html
	download 64-bit .exe file
	add C:\Program Files\7-Zip to environment variable for your account

CHANGELOG:
	targetIP now becomes a global variable within GRPC to avoid running thd multiple times
	