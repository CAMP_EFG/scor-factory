'''
Author: Joshua
Date Modified: 1/2/2019
Description: This class contains commands for secure grpc's MODEM_COMMAND and MODEM_MESSAGE
'''
import grpc, subprocess, sys, re, os, RadioManager_pb2, time, RadioManager_pb2_grpc


def verifyPyVer():
	if not re.search("3", sys.version):
		print("You need to use Python version 3.x.x")
		os._exit(1)

'''
grpcType:
c - command
q - query
f - in full detail of grpc error message
'''
def printErr(e, grpcType):
	if grpcType   == 'c':
		print("GRPC COMMAND FAILED")
		print(e.details())
	elif grpcType == 'q':
		print("GRPC QUERY FAILED")
		print(e.details())
	elif grpcType == 'f':
		print(str(e))
	print(str(e.code()))


verifyPyVer()
import configparser
'''configparser'''

cfg = configparser.ConfigParser()
absPath = os.path.dirname(os.path.realpath(__file__))
cwd = os.getcwd()
os.chdir(absPath)

cfg.read('../config/atsGrpcconfig.cfg')
svr = cfg['SERVER_INFO']
rmacfg = cfg['RMA']
COMMON_NAME = 'GRPCRMA'

os.chdir(cwd)

'''GETTERS'''
def getClientChannel(targetIP):
	if(rmacfg.get('RMA_GRPC')):
		rkey = None
		rcer = None
		rca_cer = open('../ssl/gRPCRMAServer.crt').read().encode('utf-8')
		creds = grpc.ssl_channel_credentials(rca_cer, rkey, rcer)
		channel = grpc.secure_channel(targetIP+':60053', creds, \
			options=(('grpc.ssl_target_name_override', COMMON_NAME,),))
	else:
		chain = open('../ssl/CAScorpion.chain').read().encode('utf-8')
		pem = open('../ssl/calamp_t6rx_eol_unen.pem').read().encode('utf-8')
		cer = open('../ssl/calamp_t6rx_eol_testing_only--cert0.cer').read().encode('utf-8')

		creds = grpc.ssl_channel_credentials(chain , pem, cer)
		commonName = getCommonName()
		channel = grpc.secure_channel('172.31.234.158:60051', creds, \
			#options=(('grpc.ssl_target_name_override', 'badbadbadbad000111222333444555',),))
			options=(('grpc.ssl_target_name_override', commonName, ), ))

	return channel

'''
Get the Common Name (CN) from openssl command
'''
def getCommonName():
	command = 'openssl s_client -connect 172.31.234.158:60051'
	result = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
	raw = str(result.stdout.read())
	cn = raw.split('\\n')[3].split('/')
	for curr in cn:
		if 'CN' in curr:
			cn = curr[3:]
			break
	return cn

'''
Example: 165.26.79.1:60051
'''
def getServerAddress():
	count = 0
	serverAddress = ''
	while count < 3:
		serverIp      = getTargetIP()
		if(rmacfg.get('RMA_GRPC')):
			if(serverIp.strip() == "" or serverIp == svr.get('HOST_IP')):
				pass
			else:
				serverAddress = serverIp+':' + rmacfg.get('RMA_SVR_PORT')
				print("RMA: yes, RMA_SVR_PORT=" + rmacfg.get('RMA_SVR_PORT'))
				break
		else:
			if(serverIp.strip() == "" or serverIp == svr.get('HOST_IP')):
				pass
			else:
				serverAddress = serverIp+':' + svr.get('SERVER_PORT')
				print("RMA: no, SERVER_PORT=" + svr.get('SERVER_PORT'))
				break
			break
		time.sleep(1)
		count += 1

	return serverAddress

'''
Gets the IP address of the 1st instance of the board connected
to the PC
'''
def getTargetIP():
	if (svr.get('target_ip') != ""):
		targetIP = svr.get('target_ip')
		print("Target IP found in config file: " + targetIP)
		return targetIP

	absPath = os.path.dirname(os.path.realpath(__file__))
	cwd = os.getcwd()
	os.chdir(absPath)
	import importlib
	p = os.path.abspath('../lib/')
	if p not in sys.path:
		sys.path.append(p)
	import thd35
	importlib.reload(thd35)

	if(thd35.targetIP == "" or thd35.targetIP == svr.get('HOST_IP')):
		print("No target IP found")
		return ''
	else:
		#print("Target IP found: " + thd35.targetIP)
		return thd35.targetIP

	os.chdir(cwd)

class atsGrpcLocal():
	def __init__(self):
		pass

################################################################################################################
	def atsGrpcGetModeminfo(self, msgId):
		try:
			modemMgr_stub = RadioManager_pb2_grpc.ModemManagerStub(getClientChannel)
			request = RadioManager_pb2.ModemManagerFilterMessage(ModemManagerFilter = int(msgId))
			response = modemMgr_stub.GetInfo(request)
			print(str(response))
			return response
		except Exception as e:
			#print("Error getting modem info: " + str(e))
			return e

	def atsGrpcSendCommand(self, msgId, value1 = None, value2 = None, value3 = None):
		try:
			modemMgr_stub = RadioManager_pb2_grpc.ModemManagerStub(getClientChannel)
			request = RadioManager_pb2.ModemCommandRequest(Command = msgId, CommandValue = value1)
			response = modemMgr_stub.SendCommand(request)
			print(str(response))
			return 0
		except Exception as e:
			#print("Error: " + str(e))
			return e

	def atsGrpcGetModemDataStream_secure(self, msgId, secureChannel):
		try:
			f = open("report", "a")
			modemMgr_stub = RadioManager_pb2_grpc.ModemManagerStub(secureChannel)
			request = RadioManager_pb2.ModemManagerFilterMessage(ModemManagerFilter = int(msgId))
			responses = modemMgr_stub.GetModemDataStream(request)
			initial = True
			prev = ""

			for response in responses:
				print(response)
				f.write("Tag Id: " + str(hex(id(responses))) + "\n")
				f.write(str(response) + "\n")
				if initial == True:
					prev = id(responses)
					initial = False
				elif prev != id(responses):
					print("Stream " + msg.get(str(msgId)) + ": FAIL")
					return -1
				else:
					f.write("Verified ID: " + str(hex(id(responses))) + "\n")
					print("Stream " + msg.get(str(msgId)) + ": PASS")
					return 0
			f.close()
		except Exception as e:
			print("grpc stream info error")
			print(e)
			return str(e)
