#!/usr/bin/env python3
"""
timeLog.py

This is a class that abstracts a standard timestampped logging format

(c) 2013 Caterpillar, Inc.

"""

import time;
import datetime;

class timeLog:
	def __init__(self, outputPath = None, consolePrint = True):
		self.outputPath = outputPath;
		self.consolePrint = consolePrint;
	
	def humanTimeString(self):
		return str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'));
	
	def timelog(self, printString):
		if (self.consolePrint == True):
			print (self.humanTimeString() + ': ' + str(printString));
		if (self.outputPath != None):
			with open(self.outputPath, "a+") as outputFile:
				print(self.humanTimeString() + ': ' + str(printString), file=outputFile);
