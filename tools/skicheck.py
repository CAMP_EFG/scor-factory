#!/usr/bin/env python3

# Copyright Caterpillar Inc.
#
# Author:  Carson Dew (7 May 2014)

import http.client, configparser, os, sys

cfg = configparser.ConfigParser()
absPath = os.path.dirname(os.path.realpath(__file__))
cwd = os.getcwd()
os.chdir(absPath)

cfg.read('../config/atsGrpcconfig.cfg')
svr = cfg['SERVER_INFO']
rmacfg = cfg['RMA']
COMMON_NAME = 'GRPCRMA'

os.chdir(cwd)

def SKICheck(host_ip_addr):
    provisioned = False

    try:
        conn = http.client.HTTPConnection(host_ip_addr, 8080, timeout=4)
        conn.request("GET", "/cgi-bin/is_provisioned.cgi")

        r1 = conn.getresponse()

        if r1.status == 200:
            data = r1.read().decode('ascii')

        if data.count('Device is properly provisioned'):
            provisioned = True
    except:
        provisioned = None
    finally:
        conn.close()

    return provisioned

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description='Utility to query SKIServiceDashboard')
    parser.add_argument('-d','--device-ip', dest='deviceip', required=True,
                        help='IP address of the device host to query')
    args = parser.parse_args()
    ret = None
    if(rmacfg.get('RMA_GRPC')):
        p = os.path.abspath('../scripts/')
        if p not in sys.path:
            sys.path.append(p)
        import atsCalampSkiCheck
        res = atsCalampSkiCheck.getAtsCalampSkiCheck()
        if "SKI Provisioned" in str(res):
            ret = True
        elif "SKI not Provisioned" in str(res):
            ret = False
    else:
        ret = SKICheck(args.deviceip)
    if ret != None and ret == True:
        print(ret)
        exit(0)
    elif ret != None and ret == False:
        print(ret)
        exit(1)
    else:
        print('skiCheck failed')
        exit(-1)
