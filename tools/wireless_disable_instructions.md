# WIRELESS DISABLE CONFIRMATION INSTRUCTIONS
The T6RX/Scorpion radios have an external pin for wireless disable.  When grounded, this pin will shutdown the modems
in the unit.  This document discusses how to confirm that the modems are shutdown for manufacturing testing purposes.

## Wireless disable status interface
The ServiceDashboard application provides the http interface to query the device for wireless disable confirmation. It
provides this information in an XML file that may be downloaded from the following URL:

http://172.31.234.158/ServiceDashboard/ServiceDashboard.xml

The IP address should be static for T6RX devices.  Alternatively, the Caterpillar IP address in the 165.26.78.0/23 range
may be used.  This document does not discuss how to discover that address.

__Note:  The ServiceDashboard application gathers its data from the grpc server inside the CalAmp CASCOR application.  The
grpc interface is also a viable method to confirm wireless disable status.__

## Identifying modem state from ServiceDashboard.xml
Depending on the device being queried, there are one or more modem states to parse.

* For PL243 and PL083, the cell modem is identified by ```<Cell><Radio><ModemState>```
* For PL083 the additional satellite modem is identified by ```<Cell><Radio><Modemstate2>```

### PL083 Normal state
```
<ModemState>On</ModemState>
<Modemstate2>Off</Modemstate2>
```

### PL083 Wireless disabled state
```
<ModemState>Wireless disabled</ModemState>
<Modemstate2>Wireless disabled</Modemstate2>
```

### PL243 Normal state
```
<ModemState>On</ModemState>
<Modemstate2>Not Available</Modemstate2>
```

### PL243 Wireless disabled state
```
<ModemState>Wireless disabled</ModemState>
<Modemstate2>Not Available</Modemstate2>
```