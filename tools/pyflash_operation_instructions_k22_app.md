# How to verify Kinetis application flashed correctly

The answer to this is that the FL2 flashing mechanism through RPA (FTP interface)
is the relied upon mechansim for confirming flash operation.  This includes all 
content contained within an FL2 including kinetis k22 application files.  With that 
in mind, there is a deeper level of confirmation available.  

__After a complete, successful FL2 flash operation__, the device should be running the
ServiceDashboard application.  This application provides a cgi web interface to 
read the reprogramming log from the device.  Inside the reprogramming log will be 
references to the kinetis application file and it's SHA1 hash value.  It will also
include messages related to success or failure in flashing that application file.

## Web CGI interface
When the devices is powered and the ServiceDashboard is executing, a URL available 
at 

```

http://<IP_ADDRESS>/cgi-bin/display_reprogramming_log.cgi

where IP_ADDRESS is in 165.26.78.0/24 range
```

### Requesting the URL above yields results similar to below

```
--- snip ---

Jan 01 2013 01:26:01 | /usr/sbin/rpad[227] | 		sha1sum fa521eef0c45ab211f4b95f9c0b674e7c1e7cfbe
Jan 01 2013 01:26:06 | /usr/sbin/rpad[227] | 	Flash id:6789 rebooting
Jan 01 2013 00:28:26 | /usr/sbin/rpad[226] | Flash ID:1541605411 started by PyFTPPus
Jan 01 2013 00:28:33 | /usr/sbin/rpad[226] | 	Prepping Kinetis firmware (app_t6rxk22_KINETIS_2018.8.0.kinetis)
Jan 01 2013 00:28:33 | /usr/sbin/rpad[226] | 		sha1sum 5e90d606f1ebb6f907d1b3731d36df25c9c053c5
Jan 01 2013 00:28:33 | /usr/sbin/rpad[226] | 	Flashing Kinetis
Jan 01 2013 00:28:42 | /usr/sbin/rpad[226] | 		done
Jan 01 2013 00:28:42 | /usr/sbin/rpad[226] | 	Flashing kernel (uImage.imx6ull-scorpion-5663408-11-dv2-prodcerts.bin.signed_new.signed_old.bin)
Jan 01 2013 00:29:08 | /usr/sbin/rpad[226] | 		sha1sum b6498a45631db8c

--- snip ---
```

Note the 2 lines containing the text below show the Kinetis file name and the SHA1 value that 
the on board RPA program calculates.  
```
Jan 01 2013 00:28:33 | /usr/sbin/rpad[226] | 	Prepping Kinetis firmware (app_t6rxk22_KINETIS_2018.8.0.kinetis)
Jan 01 2013 00:28:33 | /usr/sbin/rpad[226] | 		sha1sum 5e90d606f1ebb6f907d1b3731d36df25c9c053c5
```
Success can be assumed by looking at the two lines immediately after:
```
Jan 01 2013 00:28:33 | /usr/sbin/rpad[226] | 	Flashing Kinetis
Jan 01 2013 00:28:42 | /usr/sbin/rpad[226] | 		done
```
The Kinetis version present on the device can then be compared to the one inside the FL2 file of the same filename.  The FL2 file is 
simply a 7-zip archive with a different extension.  The kinetis application file can be extracted from that archive and used to calculate
the expected sha1sum that is reported by the reprogramming log data.

A failure message is unlikely, but will be formatted similarly to:
```
Nov 07 2018 16:29:01 | /usr/sbin/rpad[317] | 	Prepping Kinetis firmware (bad_k22_app.kinetis)
Nov 07 2018 16:29:01 | /usr/sbin/rpad[317] | 		sha1sum 0193e965e61bed8728053ef295b293394a6e49d5
Nov 07 2018 16:29:01 | /usr/sbin/rpad[317] | 	Flashing Kinetis
Nov 07 2018 16:29:01 | /usr/sbin/rpad[317] | 		status: 512, errno: 2
```
Note that the filename and SHA1 portion of the message should be the same as a success, however, there will be a 'status' instead of 'done' message.
The filename and sha1sum in the failed example are different than the success example because a different file was just to invoke an error.