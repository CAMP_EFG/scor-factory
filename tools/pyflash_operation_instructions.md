# Pyflash operations
Pyflash is a collection of python scripts that faciliate some common device manufacturing operations.  These include IP address discovery,
SKI process confirmation, and FL2 flashing.


## Host Discovery
The Caterpillar devices typically run a protocol called Trivial Host Discovery to facililate discovery of active hosts
on a Caterpillar publicly reserved IP address range.  The range is 165.26.78.0/23.  A given host's IP address is 
dynamic and subject to change due to a link-local addressing procotol to assign IP addresses to devices on the network.
The Trivial Host Discovery is a basic UDP request/response protocol where devices respond with their MAC addresses.  MAC addresses
remain static for the device. 

The __thd.py__ file facilitates this operation.  The utility provides a help shown below.  Note that the host should 
be configured to an address in 165.26.78.0/23 range and is highly recommended to be 165.26.79.1

### thd.py help
```
$ ./thd.py --help
usage: thd.py [-h] -t SEARCHTIME [--host-mac HOSTMAC] [--host-ip HOSTIP]

Utility to discover THD hosts

optional arguments:
  -h, --help            show this help message and exit
  -t SEARCHTIME, --search-time SEARCHTIME
                        Time in seconds to discover hosts, 0 for infinite
  --host-mac HOSTMAC    MAC address of this host
  --host-ip HOSTIP      IP address of this host
```

### thd.py Execution example
An example run of the utility yields output similar to below where the 165.26.78.159 device is discovered.

```
$ ./thd.py -t 1
MAC: AAAAAAAAAAAA   IP: 165.26.79.1     Last Active: -0.5004284381866455s
MAC: 000A75108A77   IP: 165.26.78.159   Last Active: -0.44944190979003906s
MAC: AAAAAAAAAAAA   IP: 165.26.79.1     Last Active: -0.5004482269287109s
MAC: 000A75108A77   IP: 165.26.78.159   Last Active: -0.44451332092285156s
```


## SKI verification
A device's SKI state can be verified by querying a web cgi url on the device.  Their must be either
SKIServiceDashboard, ServiceDashboard, or WebFlash verity application present and executing on the device
in order for the cgi URL to be available.  These applications provide the __is_provisioned.cgi__ interface at 
a URL http://<IP_ADRESS>:8080/cgi-bin/is_provisioned.cgi

### Preferred approach using the command line utility
The __skicheck.py__ file facilitates this operation.  The utility provides help shown below.  Note that the device's IP
address is required as an input parameter so it must be known or disovered before using this tool. 

Using skicheck.py will query the address above.  Running skicheck.py will return a non-zero return code if the query is not successful.
Non-zero return codes typically happen by code exception such as a failed HTTP response or the if the cgi responds correctly 
but the unit does not have the SKI credentials present on the system.

#### skicheck.py help
```
./skicheck.py --help
usage: skicheck.py [-h] -d DEVICEIP

Utility to query SKIServiceDashboard

optional arguments:
  -h, --help            show this help message and exit
  -d DEVICEIP, --device-ip DEVICEIP
                        IP address of the device host to query
```

#### skicheck.py Execution: Success example
```
$ ./skicheck.py -d 165.26.78.159
True
$ echo $?
0
```

#### skicheck.py Execution: Failure example, valid http response
```
$ ./skicheck.py -d 165.26.78.159
False
$ echo $?
1
```

#### skicheck.py Execution: Failure example, http timeout or other exception case
```
>>> ./skicheck.py -d 165.26.78.159
Unit query failed
$ echo $?
1
```

### Browser based approach
If the a browser or similar is used to query the device, you will see two pieces of information reported from the device
assuming a successful http response:
1. Whether the SKI provisioning operation is in progress:
  * "SKI: running" text will be in the response body if the SKI provisioning process is still executing.
  * "SKI: not running" text will be in the response body if the SKI provisioning process is not executing.
2. A device's SKI status
  * "Device is properly provisioned" will be in the response body when the device has provisioned credentials.
  * "Device is not provisioned" will be in the response body when the device does not have SKI credentials.

If the http response body is parsed used during the SKI operation, SKI is complete when the device resports both "SKI: not running" and "Device is properly provisioned"


## FL2 flashing via FTP
FL2 files can be flashed to a target device through an FTP interface.  The ftppush.py utility is used to faciliate this operation.  There are several other python files used as dependencies for ftppush.py so they must be available in the appropriate directory. 
An FL2 file generally contains two 'Images.'  These images must be flashed in sequence and thus the __ftppush.py__ utility must be run twice in order to flash the complete FL2 file.  The OS image is flashed first and then the Application image is flashed.   

### ftppush.py help
```
$ ./ftppush.py --help
usage: ftppush.py [-h] -z TOOL7Z -d DEVICEMAC --fl2 FL2 --flash_select
                  {os,app,verify_only} [--skip_pn_verify]

Utility to flash a host with FTPPush

optional arguments:
  -h, --help            show this help message and exit
  -z TOOL7Z, --7z-bin TOOL7Z
                        7z command line utility to use
  -d DEVICEMAC, --device-mac DEVICEMAC
                        MAC address of the device host to flash
  --fl2 FL2             FL2 file to flash to device
  --flash_select {os,app,verify_only}
                        FL2 file to flash to device
  --skip_pn_verify      Flash will skip the Software Part Number verification
                        step after flashing the app
```

#### Note the -d DEVICEMAC input parameter
This parameter requires the device's MAC address which is available either from the unit's printed label or through output from the __thd.py__ utility.

### ftppush.py Instructions
ftppush.py handles the success case for flashing well.  In its current state, failure modes are not handled as completely.  Errors result in pyflash continuing to poll the device and will never exit.  
For practical use, ftppush may either return success or a governing process timeout failure should be applied to capture a failure case.  The length of time for the timeout will be dependent upon
Caterpillar platform as well as typical FL2 content size.  If a device experience's a flash failure, it is necessary to issue a StatusTrigger.xml to the onboard FTP reprogramming interface
in order for the error code to be reset.  Otherwise, using pyflash to attempt another flash on the device may result in a continued failure.  See more details below.

#### Flashing the OS: Success example
The first 'Image' to flash is the OS image.  It can be flashed with a command such as the following example:
```
$ ./ftppush.py -z /usr/bin/7z -d 000a75108a77 --fl2 5669805-14.fl2 --flash_select os
2018-11-08 15:10:24: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "PackageDescription.xml"
2018-11-08 15:10:24: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 15:10:24: DEBUG: part numbers before flash
2018-11-08 15:10:24: DEBUG FL2 OS: 566340811
2018-11-08 15:10:24: DEBUG Device OS: 566340811
2018-11-08 15:10:24: DEBUG FL2 App: 566980514
2018-11-08 15:10:24: DEBUG Device App: None
2018-11-08 15:10:24: INFO: Flashing OS
2018-11-08 15:10:25: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 15:10:25: DEBUG ReadyToFlash: TargetState 1 ApplicationReadyCode 0
2018-11-08 15:10:26: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 15:10:26: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/OS/app_t6rxk22_KINETIS_2018.8.0.kinetis"
2018-11-08 15:10:26: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/OS/uImage.imx6ull-scorpion-5663408-11-dv2-prodcerts.bin.signed_new.signed_old.bin"
2018-11-08 15:10:35: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/OS/ImageManifest.xml"
2018-11-08 15:10:35: DEBUG FlashSuccess: self.TargetState 1
2018-11-08 15:10:35: DEBUG FlashSuccess: self.ApplicationReadyCode 0
2018-11-08 15:10:35: DEBUG FlashSuccess: self.ToolSerialNumber FTP12345
2018-11-08 15:10:35: DEBUG FlashSuccess: self.ToolTriggerStartDateTime 6789
2018-11-08 15:10:35: DEBUG FlashSuccess: initialTriggerTime 1541711435
2018-11-08 15:10:35: DEBUG FlashSuccess: self.FlashBytesTotal 268253
2018-11-08 15:10:35: DEBUG FlashSuccess: self.FlashBytesComplete 268253
2018-11-08 15:10:35: DEBUG FlashSuccess: self.FlashErrorCode 0
2018-11-08 15:10:35: DEBUG FlashSuccess: CompletePendingReboot False
2018-11-08 15:10:35: DEBUG FlashOS: Flash not yet complete, waiting 5 seconds

--- snip (repeating waiting messages while flashing takes place) ---

2018-11-08 15:11:33: DEBUG: Searching for devices
2018-11-08 15:11:33: DEBUG: Searching for devices
2018-11-08 15:11:34: DEBUG: Searching for devices

--- snip (repeating waiting messages while rebooting takes place and FTP is coming back) ---

2018-11-08 15:12:30: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 15:12:30: DEBUG FlashOS: caught FTP exception, unit likely rebooting or initializing, sleeping 10 seconds
2018-11-08 15:12:40: DEBUG FlashSuccess: self.TargetState 3
2018-11-08 15:12:40: DEBUG FlashSuccess: self.ApplicationReadyCode 0
2018-11-08 15:12:40: DEBUG FlashSuccess: self.ToolSerialNumber PyFTPPus
2018-11-08 15:12:40: DEBUG FlashSuccess: self.ToolTriggerStartDateTime 1541711435
2018-11-08 15:12:40: DEBUG FlashSuccess: initialTriggerTime 1541711435
2018-11-08 15:12:40: DEBUG FlashSuccess: self.FlashBytesTotal 10323248
2018-11-08 15:12:40: DEBUG FlashSuccess: self.FlashBytesComplete 10323248
2018-11-08 15:12:40: DEBUG FlashSuccess: self.FlashErrorCode 0
2018-11-08 15:12:40: DEBUG FlashSuccess: CompletePendingReboot True
2018-11-08 15:12:40: DEBUG FlashOS: Flash not yet complete, waiting 5 seconds
2018-11-08 15:12:45: DEBUG FlashOS: Requesting Status

--- snip (repeating waiting messages while rebooting takes place and FTP is coming back) ---

2018-11-08 15:12:45: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 15:12:45: DEBUG FlashSuccess: self.TargetState 1
2018-11-08 15:12:45: DEBUG FlashSuccess: self.ApplicationReadyCode 0
2018-11-08 15:12:45: DEBUG FlashSuccess: self.ToolSerialNumber PyFTPPus
2018-11-08 15:12:45: DEBUG FlashSuccess: self.ToolTriggerStartDateTime 1541711435
2018-11-08 15:12:45: DEBUG FlashSuccess: initialTriggerTime 1541711435
2018-11-08 15:12:45: DEBUG FlashSuccess: self.FlashBytesTotal 10323248
2018-11-08 15:12:45: DEBUG FlashSuccess: self.FlashBytesComplete 10323248
2018-11-08 15:12:45: DEBUG FlashSuccess: self.FlashErrorCode 0
2018-11-08 15:12:45: DEBUG FlashSuccess: CompletePendingReboot True
2018-11-08 15:12:46: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 15:12:46: INFO: Flash OS successful
2018-11-08 15:12:46: DEBUG: part numbers after flash
2018-11-08 15:12:46: DEBUG FL2 OS: 566340811
2018-11-08 15:12:46: DEBUG Device OS: 566340811
2018-11-08 15:12:46: DEBUG FL2 App: 566980514
2018-11-08 15:12:46: DEBUG Device App: None

$ echo $?
0
```
Note the __2018-11-08 15:12:46: INFO: Flash OS successful__ message output as well as the 0 return code shows success.

#### Flashing the App: Success example

```
$ ./ftppush.py -z /usr/bin/7z -d 000a75108a77 --fl2 5669805-14.fl2 --flash_select app 
2018-11-08 16:44:02: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "PackageDescription.xml"
2018-11-08 16:44:03: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 16:44:03: DEBUG: part numbers before flash
2018-11-08 16:44:03: DEBUG FL2 OS: 566340811
2018-11-08 16:44:03: DEBUG Device OS: 566340811
2018-11-08 16:44:03: DEBUG FL2 App: 566980514
2018-11-08 16:44:03: DEBUG Device App: None
2018-11-08 16:44:03: INFO: Flashing App
2018-11-08 16:44:04: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 16:44:04: DEBUG ReadyToFlash: TargetState 1 ApplicationReadyCode 0
2018-11-08 16:44:04: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 16:44:04: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/calamp_apps_02018092701.verity"
2018-11-08 16:44:11: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/calamp_apps_02018092701.verity_verify"
2018-11-08 16:44:11: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/CDA_T6RX_02018071801.verity"
2018-11-08 16:44:16: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/CDA_T6RX_02018071801.verity_verify"
2018-11-08 16:44:16: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/Cleanup_T6R3_Release_2018.7.10.verity"
2018-11-08 16:44:16: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/Cleanup_T6R3_Release_2018.7.10.verity_verify"
2018-11-08 16:44:16: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/CoreTelematics_T6RX_02018092101.verity"
2018-11-08 16:44:23: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/CoreTelematics_T6RX_02018092101.verity_verify"
2018-11-08 16:44:23: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/rrpa_T6RX_02018092101.verity"
2018-11-08 16:44:24: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/rrpa_T6RX_02018092101.verity_verify"
2018-11-08 16:44:24: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/ServiceDashboard_T6RX_02018092101.verity"
2018-11-08 16:44:26: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/ServiceDashboard_T6RX_02018092101.verity_verify"
2018-11-08 16:44:26: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/SoftwareRelease_T6R3_OnboardTelematics_2018.7.0.dev5.verity"
2018-11-08 16:44:27: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/SoftwareRelease_T6R3_OnboardTelematics_2018.7.0.dev5.verity_verify"
2018-11-08 16:44:27: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/VIMS_T6R3_MultiApp_Release_2018.7.10.verity"
2018-11-08 16:44:59: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/VIMS_T6R3_MultiApp_Release_2018.7.10.verity_verify"
2018-11-08 16:44:59: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "DataImages/TARGET_01/App/ImageManifest.xml"
2018-11-08 16:44:59: DEBUG FlashSuccess: self.TargetState 1
2018-11-08 16:44:59: DEBUG FlashSuccess: self.ApplicationReadyCode 0
2018-11-08 16:44:59: DEBUG FlashSuccess: self.ToolSerialNumber PyFTPPus
2018-11-08 16:44:59: DEBUG FlashSuccess: self.ToolTriggerStartDateTime 1541716555
2018-11-08 16:44:59: DEBUG FlashSuccess: initialTriggerTime 1541717099
2018-11-08 16:44:59: DEBUG FlashSuccess: self.FlashBytesTotal 10323248
2018-11-08 16:44:59: DEBUG FlashSuccess: self.FlashBytesComplete 10323248
2018-11-08 16:44:59: DEBUG FlashSuccess: self.FlashErrorCode 0
2018-11-08 16:44:59: DEBUG FlashSuccess: CompletePendingReboot False
2018-11-08 16:44:59: DEBUG FlashApp: Flash not yet complete, waiting 5 seconds
2018-11-08 16:45:04: DEBUG FlashApp: Requesting Status

--- snip (repeating waiting messages while flashing takes place) ---

2018-11-08 16:46:07: DEBUG: Searching for devices
2018-11-08 16:46:07: DEBUG: Searching for devices

--- snip (repeating waiting messages while rebooting takes place and FTP is coming back) ---

2018-11-08 16:46:35: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 16:46:35: DEBUG FlashApp: caught FTP exception, unit likely rebooting or initializing, sleeping 10 seconds
2018-11-08 16:46:45: DEBUG FlashSuccess: self.TargetState 3
2018-11-08 16:46:45: DEBUG FlashSuccess: self.ApplicationReadyCode 0
2018-11-08 16:46:45: DEBUG FlashSuccess: self.ToolSerialNumber PyFTPPus
2018-11-08 16:46:45: DEBUG FlashSuccess: self.ToolTriggerStartDateTime 1541717099
2018-11-08 16:46:45: DEBUG FlashSuccess: initialTriggerTime 1541717099
2018-11-08 16:46:45: DEBUG FlashSuccess: self.FlashBytesTotal 55270972
2018-11-08 16:46:45: DEBUG FlashSuccess: self.FlashBytesComplete 55270972
2018-11-08 16:46:45: DEBUG FlashSuccess: self.FlashErrorCode 0
2018-11-08 16:46:45: DEBUG FlashSuccess: CompletePendingReboot True
2018-11-08 16:46:45: DEBUG FlashApp: Flash not yet complete, waiting 5 seconds
2018-11-08 16:46:50: DEBUG FlashApp: Requesting Status
2018-11-08 16:46:51: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 16:46:51: DEBUG FlashApp: caught FTP exception, unit likely rebooting or initializing, sleeping 10 seconds
2018-11-08 16:47:01: DEBUG FlashSuccess: self.TargetState 3
2018-11-08 16:47:01: DEBUG FlashSuccess: self.ApplicationReadyCode 0
2018-11-08 16:47:01: DEBUG FlashSuccess: self.ToolSerialNumber PyFTPPus
2018-11-08 16:47:01: DEBUG FlashSuccess: self.ToolTriggerStartDateTime 1541717099
2018-11-08 16:47:01: DEBUG FlashSuccess: initialTriggerTime 1541717099
2018-11-08 16:47:01: DEBUG FlashSuccess: self.FlashBytesTotal 55270972
2018-11-08 16:47:01: DEBUG FlashSuccess: self.FlashBytesComplete 55270972
2018-11-08 16:47:01: DEBUG FlashSuccess: self.FlashErrorCode 0
2018-11-08 16:47:01: DEBUG FlashSuccess: CompletePendingReboot True
2018-11-08 16:47:01: DEBUG FlashApp: Flash not yet complete, waiting 5 seconds
2018-11-08 16:47:06: DEBUG FlashApp: Requesting Status
2018-11-08 16:47:06: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 16:47:07: DEBUG FlashSuccess: self.TargetState 1
2018-11-08 16:47:07: DEBUG FlashSuccess: self.ApplicationReadyCode 0
2018-11-08 16:47:07: DEBUG FlashSuccess: self.ToolSerialNumber PyFTPPus
2018-11-08 16:47:07: DEBUG FlashSuccess: self.ToolTriggerStartDateTime 1541717099
2018-11-08 16:47:07: DEBUG FlashSuccess: initialTriggerTime 1541717099
2018-11-08 16:47:07: DEBUG FlashSuccess: self.FlashBytesTotal 55270972
2018-11-08 16:47:07: DEBUG FlashSuccess: self.FlashBytesComplete 55270972
2018-11-08 16:47:07: DEBUG FlashSuccess: self.FlashErrorCode 0
2018-11-08 16:47:07: DEBUG FlashSuccess: CompletePendingReboot True
2018-11-08 16:47:07: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 16:47:08: INFO: OS comparison successful again
2018-11-08 16:47:08: INFO: Flash App successful
2018-11-08 16:47:08: DEBUG: part numbers after flash
2018-11-08 16:47:08: DEBUG FL2 OS: 566340811
2018-11-08 16:47:08: DEBUG Device OS: 566340811
2018-11-08 16:47:08: DEBUG FL2 App: 566980514
2018-11-08 16:47:08: DEBUG Device App: 566980514

$ echo $?
0
```
Note the __2018-11-08 16:47:08: INFO: Flash App successful__ message output as well as the 0 return code shows success.

#### Verifying a unit against an FL2
This command can be used to compare a unit's software versions against an FL2 file's software versions

```
./ftppush.py -z /usr/bin/7z -d 000a75108a77 --fl2 5669805-14.fl2 --flash_select verify_only
2018-11-08 16:55:22: DEBUG: ExtractFile, "/usr/bin/7z" e -so "5669805-14.fl2" "PackageDescription.xml"
2018-11-08 16:55:23: DEBUG: Device with MAC 000a75108a77 found with IP 165.26.78.159
2018-11-08 16:55:23: DEBUG: part numbers before flash
2018-11-08 16:55:23: DEBUG FL2 OS: 566340811
2018-11-08 16:55:23: DEBUG Device OS: 566340811
2018-11-08 16:55:23: DEBUG FL2 App: 566980514
2018-11-08 16:55:23: DEBUG Device App: 566980514
2018-11-08 16:55:23: INFO: CURRENT PART NUMBER STATE
2018-11-08 16:55:23: INFO FL2 OS: 566340811
2018-11-08 16:55:23: INFO Device OS: 566340811
2018-11-08 16:55:23: INFO FL2 App: 566980514
2018-11-08 16:55:23: INFO Device App: 566980514
2018-11-08 16:55:23: DEBUG: part numbers after flash
2018-11-08 16:55:23: DEBUG FL2 OS: 566340811
2018-11-08 16:55:23: DEBUG Device OS: 566340811
2018-11-08 16:55:23: DEBUG FL2 App: 566980514
2018-11-08 16:55:23: DEBUG Device App: 566980514
```

#### Attempting to flash a unit that experienced a previous flashing failure
This process should be performed before subsequent flash attempts after a unit encounters and FL2 flash failure.

1. Power cycle the device
2. Upload a StatusTrigger.xml to the device.
  1. Determine the device's IP address
  2. Connect with an FTP client to the device's FTP server using user:pass = anonymous:anonymous
  3. Navigate to the 'ipflash' directory on the FTP server
  4. upload the StatusTrigger.xml file (contents shown below)
3. Attempt pyflash operation again

##### StatusTrigger.xml
```
<?xml version="1.0" encoding="utf-8" ?>
<StatusTrigger>
    <ECURecvFTP version="1">
        <ToolSerialNumber>12345</ToolSerialNumber>
        <ToolTriggerStartDateTime>12345</ToolTriggerStartDateTime>
    </ECURecvFTP>
</StatusTrigger>
```