# SCORPION RADIO BATTERY HIBERNATE INSTRUCTIONS
The Scorpion radios have an internal battery that must be put into hibernation during manufacturing stages and prior
to shipping the device from the supplier.  This is required to ensure proper battery lifetime.

## Interface for setting the battery hibernate mode
The mechanism to put the internal battery in a hibernate state is provided by a web cgi on the device.  This web
cgi is only available when the SKIServiceDashboard or the ServiceDashboard are present and executing on the device.
The device IP address to use for accessing the cgi is different depending on the application used.

### ServiceDashboard
For the regular ServiceDashboard, http://172.31.234.158/cgi-bin/set_hibernate.cgi may be used.


### SKIServiceDashboard
For the SKIServiceDashboard, the device IP address is different and dynamic.  It must be discovered in order
to access the webserver.  The discovery method is covered in other documentation.

The IP address will be in the range 165.26.78.0/23 and the url ```http://<IP_ADDRESS>:8080/cgi-bin/set_hibernate.cgi``` may be used.

## Query behavior
When querying the device, it should always return a __200 http response code__ if the cgi is up and is 
available.  A governing timeout may be necessary during manufacturing tests to account for variation in
ServiceDashboard application startup time.

Examples of responses from the ServiceDashboard are given below.

### Success example:
```
Content-Type: text/html

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hibernation</title>
</head>
<body>
<h1>
Device has been set to Hibernate.
</h1>
</body>
</html>
```

### Failure examples:
```
Status: 500 Internal Error
```

```
Status: 404 File Not Found
```