#! /usr/bin/env python3

import subprocess as sp
import shlex
import os
import sys
import xml.etree.ElementTree as ET
import time
import timeLog

# TODO:  Update this to use standard library (requires python 3.3+)
class FL2Extractor():
    
    def __init__(self, lzma_tool, myTimeLog = None):
        self.lzma_tool = lzma_tool
        if (myTimeLog != None):
            self.myTimeLog = myTimeLog;
        else:
            self.myTimeLog = timeLog.timeLog()

    def ExtractFile(self, fl2Object, desiredFile):
        '''Extracts a file from the FL2 given its relative archive path'''
        extract_cmd = '"{0}" e -so "{1}" "{2}"'.format(os.path.normpath(self.lzma_tool), 
                                                       os.path.normpath(fl2Object.fl2filepath), 
                                                       os.path.normpath(desiredFile))
        self.myTimeLog.timelog('DEBUG: ExtractFile, ' + extract_cmd)
        (filedata, stderr) = sp.Popen(shlex.split(extract_cmd), stdout=sp.PIPE, stderr=sp.PIPE).communicate()
        return filedata
        
    def SetPackageDescription(self, fl2Object):
        condition = True
        while condition:
            try:
                pd_str = self.ExtractFile(fl2Object, 'PackageDescription.xml')
                fl2Object.PackageDescription = PackageDescription(pd_str)
                condition = False
            except(IndexError):
                self.myTimeLog.timelog('DEBUG: Failed parsing package description')
                time.sleep(5)
    
    def FileListing(self, fl2Object):
        '''Returns list of files with relative archive paths'''
        list_cmd = '"{0}" l "{1}"'.format(os.path.normpath(self.lzma_tool), 
                                          os.path.normpath(fl2Object.fl2filepath))
        listing_str = sp.check_output(shlex.split(list_cmd))
        listing_str = listing_str.decode()
        listing_lines = listing_str.split(os.linesep)
        name_col_heading_offset = listing_str.find('Name')
        heading_row = listing_str.count(os.linesep,0,name_col_heading_offset)
        file_listing_startrow = heading_row + 2
        file_listing_endrow = -3
        file_listing_startcol = listing_lines[heading_row].find('Name')
        fl2Object.SetFileList([x[file_listing_startcol:] for x in listing_lines[file_listing_startrow:file_listing_endrow]])
        
        
class PackageDescription:
    def __init__(self,PackageDescriptionXMLString):
        self.initXMLString = PackageDescriptionXMLString
        self._processXML()
        
    def _processXML(self):
        '''Process initial XML string into necessary attributes'''

        tree = ET.ElementTree(ET.XML(self.initXMLString))
        root = tree.getroot()
        
        # There has got to be better code for this below, I feel dirty...
        xmlns_key = [x for x in root.attrib.keys() if x != 'version'][0]
        xmlns = '{' + root.attrib[xmlns_key].split()[0] + '}'
        
        self.OperatingSystemPartNumber = ""
        self.OperatingSystemChangeNumber = ""
        self.OperatingSystemImageReferences = []
        self.OperatingSystemFiles = []
        self.ApplicationImageReferences = []
        self.ApplicationFiles = []
        for elem in root.iter(tag=xmlns+'OperatingSystemPartition'):
            for subelem in elem.iter(tag=xmlns+'PartNumber'):
                self.OperatingSystemPartNumber = subelem.text
            for subelem in elem.iter(tag=xmlns+'ChangeNumber'):
                self.OperatingSystemChangeNumber = subelem.text
            
        # Get list of Operating System files based on ImageReferences
        for elem in root.iter(tag=xmlns+'OperatingSystemPartition'):
            for subelem in elem.iter(tag=xmlns+'ImageReferences'):
                for subsubelem in subelem.iter(tag=xmlns+'ImageReference'):
                    self.OperatingSystemImageReferences.append(subsubelem.get('reference'))
        for ref in self.OperatingSystemImageReferences:
            image = root.find(".//*[@id='%s']" % ref)
            for filename in image.iter(tag=xmlns+'Filename'):
                self.OperatingSystemFiles.append(filename.text)
        
        sw_elem = list(root.iter(tag=xmlns+'SoftwarePartNumber'))[0]
        for subelem in sw_elem.iter(tag=xmlns+'PartNumber'):
            self.ApplicationPartNumber = subelem.text
        for subelem in sw_elem.iter(tag=xmlns+'ChangeNumber'):
            self.ApplicationChangeNumber = subelem.text
            
        # Get list of Application files based on ImageReferences
        for elem in root.iter(tag=xmlns+'ApplicationPartition'):
            for subelem in elem.iter(tag=xmlns+'ImageReferences'):
                for subsubelem in subelem.iter(tag=xmlns+'ImageReference'):
                    self.ApplicationImageReferences.append(subsubelem.get('reference'))
        for ref in self.ApplicationImageReferences:
            image = root.find(".//*[@id='%s']" % ref)
            for filename in image.iter(tag=xmlns+'Filename'):
                self.ApplicationFiles.append(filename.text)
                
                
# Needs better definition
class FL2():
    '''Defines a FL2 flash file'''
    def __init__(self,fl2filepath):
        self.fl2filepath = fl2filepath
        
    def GetOperatingSystemPartNumber(self):
        return self.PackageDescription.OperatingSystemPartNumber + self.PackageDescription.OperatingSystemChangeNumber
        
    def GetApplicationPartNumber(self):
        return self.PackageDescription.ApplicationPartNumber + self.PackageDescription.ApplicationChangeNumber
    
    def SetFileList(self,filelist):
        self.files = filelist
    
    def GetFileList(self):
        return self.files
        
    def _getApplicationFiles(self):
        return self.PackageDescription.ApplicationFiles
        
    def _getOperatingSystemFiles(self):
        return self.PackageDescription.OperatingSystemFiles
