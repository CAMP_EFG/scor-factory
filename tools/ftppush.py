#! /usr/bin/env python3

# Copyright Caterpillar Inc.
# 
# Author:  Carson Dew (7 May 2014)

# This file currently uses the subprocess module and 7-zip to handle fl2 files
# LZMA processing is not available in python3.2's standard library which is the 
# standard python3 install in Ubuntu 12.04

from ftplib import FTP, all_errors as FTPERRORS
import xml.etree.ElementTree as ET
import subprocess as sp, shlex
import pprint
import os, io, time, sys
import thd
import socket
import timeLog
import errno

import fl2

def retrbinaryAsString(FTPsession,filename):
    read_list = []
    def read_ftp(more_data):
        read_list.append(more_data.decode('utf-8'))
    FTPsession.retrbinary('RETR ' + filename, read_ftp)
    return ''.join(read_list)

class FlashSystemInformation:
    def __init__(self,FlashSystemInformationXMLString):
        self.initXMLString = FlashSystemInformationXMLString
        self._processXML()
        
    def _processXML(self):
        '''Process initial XML string into necessary attributes'''
        tree = ET.ElementTree(ET.XML(self.initXMLString))
        root = tree.getroot()
        
        for elem in root.iter(tag='FlashSystemInformation'):
            self.version = elem.attrib['version']
            self.rootpath = elem.attrib['rootpath']
            
        for elem in tree.iter(tag='InformationFilePathName'):
            self.informationfilepathname = elem.text

        for elem in tree.iter(tag='StatusFilePathName'):
            self.statusfilepathname = elem.text

        for elem in tree.iter(tag='FlashTriggerFilePathName' ):
            self.flashtriggerfilepathname = elem.text

        for elem in tree.iter(tag='StatusTriggerFilePathName' ):
            self.statustriggerfilepathname = elem.text

class FlashTargetInformation:
    def __init__(self, FlashTargetInformationXMLString):
        # Keep string used to initialize object
        self.initXMLString = FlashTargetInformationXMLString
        self._processXML()
        
    def _processXML(self):
        '''Process initial XML string into necessary attributes'''
        tree = ET.ElementTree(ET.XML(self.initXMLString))
        root = tree.getroot()
        
        for elem in root.iter(tag='FlashTargetInformation'):
            self.version = elem.attrib['version']
            
        for elem in tree.iter(tag='LastServiceToolToFlash'):
            self.LastServiceToolToFlash = elem.text
            
        for elem in tree.iter(tag='ECMSerialNumber'):
            self.ECMSerialNumber = elem.text
            
        for elem in tree.iter(tag='OperatingSystem'):
            for subelem in elem.iter(tag='PartNumber'):
                self.OperatingSystemPartNumber = subelem.text
            for subelem in elem.iter(tag='ChangeNumber'):
                self.OperatingSystemChangeNumber  = subelem.text
            
        for elem in tree.iter(tag='Application'):
            for subelem in elem.iter(tag='PartNumber'):
                self.ApplicationPartNumber = subelem.text
            for subelem in elem.iter(tag='ChangeNumber'):
                self.ApplicationChangeNumber  = subelem.text
                
    def GetOperatingSystemPartNumber(self):
        try:
            os_pn = self.OperatingSystemPartNumber + self.OperatingSystemChangeNumber
        except:
            os_pn = None
        return os_pn
        
    def GetApplicationPartNumber(self):
        try:
           app_pn = self.ApplicationPartNumber + self.ApplicationChangeNumber
        except:
            app_pn = None
        return app_pn
        
    def GetECMSerialNumber(self):
        try:
            ecm_sn = self.ECMSerialNumber
        except:
            ecm_sn = None
        return ecm_sn
        
    def GetECMHardwareType(self):
        try:
            hardware_type = self.ECMSerialNumber[-2:]
        except:
            hardware_type = None
        return hardware_type

class FlashStatus():
    '''Holds a Flash Status XML Message'''
    def __init__(self, FlashStatusXMLString, myTimeLog = None):
        if (myTimeLog != None):
            self.myTimeLog = myTimeLog;
        else:
            self.myTimeLog = timeLog.timeLog()
        
        # Keep string used to initialize object
        self.initXMLString = FlashStatusXMLString
        self._processXML()
        
    def _processXML(self):
        '''Process initial XML string into necessary attributes'''
        tree = ET.ElementTree(ET.XML(self.initXMLString))
        root = tree.getroot()
        
        for elem in root.iter(tag='TargetState'):
            self.TargetState = elem.text
            
        for elem in root.iter(tag='ApplicationReadyCode'):
            self.ApplicationReadyCode = elem.text
        
        for elem in root.iter(tag='FlashErrorCode'):
            self.FlashErrorCode = elem.text
        for elem in root.iter(tag='ToolTriggerStartDateTime'):
            self.ToolTriggerStartDateTime = elem.text
            
        for elem in root.iter(tag='FlashBytesTotal'):
            self.FlashBytesTotal = elem.text
        for elem in root.iter(tag='FlashBytesComplete'):
            self.FlashBytesComplete = elem.text
        for elem in root.iter(tag='ToolSerialNumber'):
            self.ToolSerialNumber = elem.text
            
    
    def ReadyToFlash(self):
        self.myTimeLog.timelog('DEBUG ReadyToFlash: TargetState ' + self.TargetState + ' ApplicationReadyCode ' + self.ApplicationReadyCode)
        
        if self.TargetState == '1' and self.ApplicationReadyCode == '0':
            return True
        else:
            return False
        
    def FlashSuccess(self, initialTriggerTime, CompletePendingReboot):
        # Test if flash is complete (pending reboot)
        TestCompletePendingReboot = self.FlashBytesTotal == self.FlashBytesComplete and \
                                    self.FlashBytesTotal != '0' and \
                                    self.ToolTriggerStartDateTime == initialTriggerTime and \
                                    self.FlashErrorCode == '0' and \
                                    self.ApplicationReadyCode == '0' and \
                                    self.TargetState == '3'
        if TestCompletePendingReboot:
            CompletePendingReboot = True
                
        self.myTimeLog.timelog('DEBUG FlashSuccess: self.TargetState ' + self.TargetState)
        self.myTimeLog.timelog('DEBUG FlashSuccess: self.ApplicationReadyCode ' + self.ApplicationReadyCode)
        self.myTimeLog.timelog('DEBUG FlashSuccess: self.ToolSerialNumber {}' .format(self.ToolSerialNumber))
        self.myTimeLog.timelog('DEBUG FlashSuccess: self.ToolTriggerStartDateTime ' + self.ToolTriggerStartDateTime)
        self.myTimeLog.timelog('DEBUG FlashSuccess: initialTriggerTime ' + initialTriggerTime)
        self.myTimeLog.timelog('DEBUG FlashSuccess: self.FlashBytesTotal ' + self.FlashBytesTotal)
        self.myTimeLog.timelog('DEBUG FlashSuccess: self.FlashBytesComplete ' + self.FlashBytesComplete)
        self.myTimeLog.timelog('DEBUG FlashSuccess: self.FlashErrorCode ' + self.FlashErrorCode)
        self.myTimeLog.timelog('DEBUG FlashSuccess: CompletePendingReboot {}' .format(CompletePendingReboot))
        
        # Test for flash complete (including reboot)
        TEST = self.ToolTriggerStartDateTime == initialTriggerTime and \
               self.FlashErrorCode == '0' and \
               self.ApplicationReadyCode == '0' and \
               self.TargetState == '1'
        if TEST:
            CompletePendingReboot = False
            return (True, CompletePendingReboot)
        else:
            # Test for possible NAND Wipe after flash was complete but pending reboot
            TestForNANDWipe = self.ToolTriggerStartDateTime == "0" and \
                               self.FlashErrorCode == '0' and \
                               self.ApplicationReadyCode == '0' and \
                               self.TargetState == '1' and \
                               self.FlashBytesTotal == '0' and \
                               self.FlashBytesComplete == '0' and \
                               self.ToolSerialNumber == None and \
                               CompletePendingReboot == True
            if TestForNANDWipe:
                # Since RPA data is all default values, assume NAND was wiped, flash state was lost, but flash was successful
                self.myTimeLog.timelog('DEBUG FlashSuccess: Assuming NAND was wiped, considering flash successful')
                CompletePendingReboot = False
                return (True, CompletePendingReboot)
            else:
                return (False, CompletePendingReboot)

class FTPPushHost():
    '''Defines a network host that uses FTPPush strategy'''
    
    def __init__(self, mac_addr, myTimeLog = None):
        if (myTimeLog != None):
            self.myTimeLog = myTimeLog;
        else:
            self.myTimeLog = timeLog.timeLog()
        
        self.myIP = '165.26.79.1'
        self.myMac = 'AAAAAAAAAAAA'
        self.mac_addr = mac_addr
        
        # Need an initial IP address
        self.DiscoverDeviceIP()
        with FTP(self.ip_addr) as ftp_session:
            ftp_session.login()
            self.FlashSystemInformation = FlashSystemInformation(retrbinaryAsString(ftp_session,'FlashSystemInformation.xml'))
            self.FlashTargetInformation = FlashTargetInformation(retrbinaryAsString(ftp_session, 
                                                                 self.FlashSystemInformation.rootpath + 'FlashTargetInformation.xml'))
            self.FlashStatus = FlashStatus(retrbinaryAsString(ftp_session, self.FlashSystemInformation.rootpath + 'FlashStatus.xml'), self.myTimeLog)
            ftp_session.close()
            
    # TODO:  This function should really be part of a FlashController class
    def _genFlashTrigger(self, timestamp, toolSN='PyFTPPush'):
        '''Generate a Flash Trigger XML as BytesIO object'''
        FlashTriggerXML = (b'<?xml version="1.0" encoding="utf-8" ?>\n'
                           b'<FlashTrigger>\n'
                           b'    <ECURecvFTP version="1">\n'
                           b'        <ToolSerialNumber>' + bytes(toolSN,'ascii') + b'</ToolSerialNumber>\n'
                           b'        <ToolTriggerStartDateTime>' + bytes(timestamp,'ascii') + b'</ToolTriggerStartDateTime>\n'
                           b'    </ECURecvFTP>\n'
                           b'</FlashTrigger>\n')
        return io.BytesIO(FlashTriggerXML)
        
    # TODO:  This function should really be part of a FlashController class
    def _genStatusTrigger(self, timestamp, toolSN='PyFTPPush'):
        '''Generates a Status Trigger XML as BytesIO object'''
        StatusTriggerXML = (b'<?xml version="1.0" encoding="utf-8" ?>\n'
                            b'<StatusTrigger>\n'
                            b'    <ECURecvFTP version="1">\n'
                            b'        <ToolSerialNumber>' + bytes(toolSN,'ascii') + b'</ToolSerialNumber>\n'
                            b'        <ToolTriggerStartDateTime>' + bytes(timestamp,'ascii') + b'</ToolTriggerStartDateTime>\n'
                            b'    </ECURecvFTP>\n'
                            b'</StatusTrigger>\n')
        return io.BytesIO(StatusTriggerXML)
    
    # TODO:  This function should really be part of a FlashController class                    
    def FlashOS(self, fl2Object, fl2Extractor):
        self.DiscoverDeviceIP()
        # Stop OS flash if no OS Files in FL2
        if not fl2Object._getOperatingSystemFiles():
            self.myTimeLog.timelog('DEBUG FlashOS: Nothing to flash')
            return
        with FTP(self.ip_addr) as ftp_session:
            ftp_session.login()
            
            # Upload all files in FL2 OS Image
            for osfile in fl2Object._getOperatingSystemFiles():
                fileBytesStream = io.BytesIO(fl2Extractor.ExtractFile(fl2Object, osfile))
                ftp_session.storbinary('STOR ' + self.FlashSystemInformation.rootpath + os.path.basename(osfile), fileBytesStream)
                
            # Upload the FlashTrigger and maintain timestamp
            trigger_time = str(int(time.time()))
            ftp_session.storbinary('STOR ' + self.FlashSystemInformation.rootpath + 'FlashTrigger.xml', self._genFlashTrigger(trigger_time))
            ftp_session.close()

            # Poll for success, multiple factors influence success
            completePendingReboot = False
            flashSuccessful, completePendingReboot = self.FlashStatus.FlashSuccess(trigger_time, completePendingReboot)
            while not flashSuccessful:
                self.myTimeLog.timelog('DEBUG FlashOS: Flash not yet complete, waiting 5 seconds')
                time.sleep(5)
                try:
                    self.myTimeLog.timelog('DEBUG FlashOS: Requesting Status')
                    self.UpdateFlashStatus()
                except(FTPERRORS):
                    self.myTimeLog.timelog('DEBUG FlashOS: caught FTP exception, unit likely rebooting or initializing, sleeping 10 seconds')
                    time.sleep(10)
                flashSuccessful, completePendingReboot = self.FlashStatus.FlashSuccess(trigger_time, completePendingReboot)    
    
    # TODO:  This function should really be part of a FlashController class
    def FlashApp(self, fl2Object, fl2Extractor):
        self.DiscoverDeviceIP()
        with FTP(self.ip_addr) as ftp_session:
            ftp_session.login()
            
            # Upload all files in FL2 Application Image
            for appfile in fl2Object._getApplicationFiles():
                fileBytesStream = io.BytesIO(fl2Extractor.ExtractFile(fl2Object, appfile))
                ftp_session.storbinary('STOR ' + self.FlashSystemInformation.rootpath + os.path.basename(appfile), fileBytesStream)
                
            # Upload the FlashTrigger and maintain timestamp
            trigger_time = str(int(time.time()))
            ftp_session.storbinary('STOR ' + self.FlashSystemInformation.rootpath + 'FlashTrigger.xml', self._genFlashTrigger(trigger_time))
            ftp_session.close()

            # Poll for success, multiple factors influence success
            completePendingReboot = False
            flashSuccessful, completePendingReboot = self.FlashStatus.FlashSuccess(trigger_time, completePendingReboot)
            while not flashSuccessful:
                self.myTimeLog.timelog('DEBUG FlashApp: Flash not yet complete, waiting 5 seconds')
                time.sleep(5)
                try:
                    self.myTimeLog.timelog('DEBUG FlashApp: Requesting Status')
                    self.UpdateFlashStatus()
                except(FTPERRORS):
                    self.myTimeLog.timelog('DEBUG FlashApp: caught FTP exception, unit likely rebooting or initializing, sleeping 10 seconds')
                    time.sleep(10)
                flashSuccessful, completePendingReboot = self.FlashStatus.FlashSuccess(trigger_time, completePendingReboot)    
            
    def UpdateFlashStatus(self):
        self.DiscoverDeviceIP()
        with FTP(self.ip_addr) as ftp_session:
            ftp_session.login()
            self.FlashStatus = FlashStatus(retrbinaryAsString(ftp_session, self.FlashSystemInformation.rootpath + 'FlashStatus.xml'), self.myTimeLog)
            ftp_session.close()

    def UpdateFlashTargetInformation(self):
        self.DiscoverDeviceIP()
        with FTP(self.ip_addr) as ftp_session:
            ftp_session.login()
            self.FlashTargetInformation = FlashTargetInformation(retrbinaryAsString(ftp_session, 
                                                                 self.FlashSystemInformation.rootpath + 'FlashTargetInformation.xml'))
            ftp_session.close()

    def THDCallback(self, active_hosts):
        network_copy = active_hosts.copy()
        for host in network_copy.values():
            if ((host.mac).upper() == (self.mac_addr).upper()):
                # Device found
                self.ip_addr = host.ip_addr
                return 1
        # No devices found
        self.myTimeLog.timelog('DEBUG: Searching for devices')
        return None

    def DiscoverDeviceIP(self):
        self.ip_addr = None
        thdAttempts = 30
        while (thdAttempts > 0):
            myThd = None
            try:
                myThd = thd.THDLocalhost(self.myMac, self.myIP)
                myThd.THDD(.5,600,callback=self.THDCallback)
                thdAttempts = 0
            except (OSError, socket.error) as instance:
                if (instance.errno == errno.EADDRINUSE):
                    thdAttempts -= 1
                    time.sleep(10)
                else:
                    raise instance
            finally:
                if (myThd != None):
                    myThd.close()

        if (self.ip_addr != None):
            self.myTimeLog.timelog('DEBUG: Device with MAC ' + self.mac_addr + ' found with IP ' + self.ip_addr)
        else:
            raise Exception('No device found with specified MAC')

def simple_flash(fl2_path, tool7z, devicemac, flash_select, skip_pn_verify = False, timeLogArg = None):
    if (timeLogArg != None):
        myTimeLog = timeLogArg;
    else:
        myTimeLog = timeLog.timeLog()
        
    # FL2 File Parsing
    inputFL2 = fl2.FL2(fl2_path)
    fl2processor = fl2.FL2Extractor(tool7z, myTimeLog)
    fl2processor.FileListing(inputFL2)
    fl2processor.SetPackageDescription(inputFL2)
    
    # Setup Device Host
    flashHost = FTPPushHost(devicemac, myTimeLog)
    myTimeLog.timelog('DEBUG: part numbers before flash')
    myTimeLog.timelog('DEBUG FL2 OS: ' + str(inputFL2.GetOperatingSystemPartNumber()))
    myTimeLog.timelog('DEBUG Device OS: ' + str(flashHost.FlashTargetInformation.GetOperatingSystemPartNumber()))
    myTimeLog.timelog('DEBUG FL2 App: ' + str(inputFL2.GetApplicationPartNumber()))
    myTimeLog.timelog('DEBUG Device App: ' + str(flashHost.FlashTargetInformation.GetApplicationPartNumber()))
    
    if flash_select == 'os':
        # Flash OS to device
        myTimeLog.timelog('INFO: Flashing OS')
        flashHost.UpdateFlashStatus()
        if flashHost.FlashStatus.ReadyToFlash():
            flashHost.FlashOS(inputFL2, fl2processor)
        else:
            myTimeLog.timelog('ERROR: OS cannot be flashed')
            return (1)
        
        # Compare OS part number on device to FL2
        flashHost.UpdateFlashTargetInformation()
        if not inputFL2.GetOperatingSystemPartNumber():
            myTimeLog.timelog('INFO: No OS Part Number in FL2, skipping OS Part Number check as we probably did not flash anything')
        elif flashHost.FlashTargetInformation.GetOperatingSystemPartNumber() == inputFL2.GetOperatingSystemPartNumber():
            myTimeLog.timelog('INFO: Flash OS successful')
        else:
            myTimeLog.timelog('ERROR: Flash OS failed')
            return (1)
    elif flash_select == 'app':
        # Flash App to device
        myTimeLog.timelog('INFO: Flashing App')
        flashHost.UpdateFlashStatus()
        if flashHost.FlashStatus.ReadyToFlash():   
            flashHost.FlashApp(inputFL2, fl2processor)
        else:
            myTimeLog.timelog('ERROR: App cannot be flashed')
            return (1)

        if (skip_pn_verify == True):
            myTimeLog.timelog('INFO: Flash App successful (did not verify part number)')
            flashHost.UpdateFlashTargetInformation()
        else:
            # Compare OS part number AND App part on host numbers to FL2
            flashHost.UpdateFlashTargetInformation()
            while flashHost.FlashTargetInformation.GetApplicationPartNumber() == None:
                myTimeLog.timelog('DEBUG App part number polling...waiting 10 seconds')
                time.sleep(10)
                flashHost.UpdateFlashTargetInformation()
                
            if not inputFL2.GetOperatingSystemPartNumber():
                myTimeLog.timelog('INFO: No OS Part Number in FL2, skipping OS Part Number check as we probably did not flash anything')
                if flashHost.FlashTargetInformation.GetApplicationPartNumber() == inputFL2.GetApplicationPartNumber():
                    myTimeLog.timelog('INFO: Flash App successful')
                else:
                    myTimeLog.timelog('ERROR: Flash App failed')
                    return (1)
            elif flashHost.FlashTargetInformation.GetOperatingSystemPartNumber() == inputFL2.GetOperatingSystemPartNumber():
                myTimeLog.timelog('INFO: OS comparison successful again')
                if flashHost.FlashTargetInformation.GetApplicationPartNumber() == inputFL2.GetApplicationPartNumber():
                    myTimeLog.timelog('INFO: Flash App successful')
                else:
                    myTimeLog.timelog('ERROR: Flash App failed')
                    return (1)
            else:
                myTimeLog.timelog('ERROR: OS Part number comparison failed after app flash')
                return (1)
    elif flash_select == 'verify_only':
        myTimeLog.timelog('INFO: CURRENT PART NUMBER STATE')
        myTimeLog.timelog('INFO FL2 OS: ' + str(inputFL2.GetOperatingSystemPartNumber()))
        myTimeLog.timelog('INFO Device OS: ' + str(flashHost.FlashTargetInformation.GetOperatingSystemPartNumber()))
        myTimeLog.timelog('INFO FL2 App: ' + str(inputFL2.GetApplicationPartNumber()))
        myTimeLog.timelog('INFO Device App: ' + str(flashHost.FlashTargetInformation.GetApplicationPartNumber()))
    else:
        myTimeLog.timelog('DEBUG: os or app not flashed')
        
    myTimeLog.timelog('DEBUG: part numbers after flash')
    myTimeLog.timelog('DEBUG FL2 OS: ' + str(inputFL2.GetOperatingSystemPartNumber()))
    myTimeLog.timelog('DEBUG Device OS: ' + str(flashHost.FlashTargetInformation.GetOperatingSystemPartNumber()))
    myTimeLog.timelog('DEBUG FL2 App: ' + str(inputFL2.GetApplicationPartNumber()))
    myTimeLog.timelog('DEBUG Device App: ' + str(flashHost.FlashTargetInformation.GetApplicationPartNumber()))
    return 0
    
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Utility to flash a host with FTPPush')
    # TODO:  Update this to use standard library (requires python 3.3+)
    parser.add_argument('-z', '--7z-bin', dest='tool7z', required=True,
                        help='7z command line utility to use')
    parser.add_argument('-d','--device-mac', dest='devicemac', required=True,
                        help='MAC address of the device host to flash')
    parser.add_argument('--fl2', dest='fl2', required=True,
                        help='FL2 file to flash to device')
    parser.add_argument('--flash_select', dest='flash_select', required=True, choices=['os', 'app', 'verify_only'],
                        help='FL2 file to flash to device')
    parser.add_argument('--skip_pn_verify', dest='skip_pn_verify', action='store_true',
                        help='Flash will skip the Software Part Number verification step after flashing the app')
    args = parser.parse_args()
    
    exit_code = simple_flash(args.fl2, args.tool7z, args.devicemac, args.flash_select, args.skip_pn_verify)
    
    exit(exit_code)
